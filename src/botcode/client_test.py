from flexmock import flexmock
from unittest.mock import patch, mock_open
from . import client


def test_check_requirements():
    (flexmock(client.subprocess)
     .should_receive('check_output')
     .with_args(['pip', 'list', '--outdated', '--format=json'])
     .and_return(b'[{"name": "telepot", "version": "12.1", '
                 b'"latest_version": "12.2", "latest_filetype": "wheel"},'
                 b'{"name": "setuptools", "version": "3.6", '
                 b'"latest_version": "36.2.7", "latest_filetype": "wheel"}]\n'))

    # not available during testing as src/ is mapped into /cyclesbot but
    # requirements.txt is COPYed in from . by Dockerfile...
    data = "telepot==12.1\npyparsing==2.2.0\n"
    with patch("builtins.open", mock_open(read_data=data)) as mock_file:

        assert client.do_check_requirements() \
            == 'telepot (12.1) - Latest: 12.2 \\[wheel]'

        mock_file.assert_called_with('requirements.txt')
