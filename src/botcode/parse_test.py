import pytest
from . import parse_msg

@pytest.mark.parametrize('msg,cmd,kwargs', [
    ('/check requirements', 'check_requirements', {}),
])
def test_code(msg, cmd, kwargs):
    assert list(parse_msg(msg)) == [(cmd, kwargs)]
