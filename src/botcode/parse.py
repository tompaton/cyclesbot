from pyparsing import Group, oneOf, Optional, Suppress, Word, alphas
from common.parse import keyword

CheckCommand = Optional(Suppress('/')) + keyword('check', 'requirements',
                                                 action='check_requirements')
Message = Group(CheckCommand('command'))

def parse_msg(msg):
    for res in Message.parseString(msg):
        yield res['command'][0], {}

HELP = """
*check requirements*
"""
