import json
import subprocess

def do_check_requirements():
    with open('requirements.txt') as f:
        requirements = [r.split('==')[0] for r in f.readlines()]

    cmd = ['pip', 'list', '--outdated', '--format=json']
    outdated = json.loads(subprocess.check_output(cmd).decode('utf-8'))

    return '\n'.join('{name} ({version}) - '
                     'Latest: {latest_version} \\[{latest_filetype}]'
                     .format(**o)
                     for o in outdated
                     if any(o['name'] == r for r in requirements))
