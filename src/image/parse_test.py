import pytest
from . import parse_msg


@pytest.mark.parametrize('msg,cmd,kwargs', [
    ('crop 100x200+300+400 radar', 'crop',
     {'command': 'radar', 'mode': 'replace',
      'width': 100, 'height': 200, 'left': 300, 'top': 400}),
    ('crop both 100x200+300+400 "radar zzz"', 'crop',
     {'command': 'radar zzz', 'mode': 'both',
      'width': 100, 'height': 200, 'left': 300, 'top': 400}),
    ('crop replace 100x200+300+400 radar two three', 'crop',
     {'command': 'radar two three', 'mode': 'replace',
      'width': 100, 'height': 200, 'left': 300, 'top': 400}),
    ('crop replace +100+200-300-400 radar two three', 'crop',
     {'command': 'radar two three', 'mode': 'replace',
      'left': 100, 'top': 200, 'right': 300, 'bottom': 400}),
    ('animate "radar two three" 0.25 100x200'
     ' 9,65 306,65 9,300 306,300 9,535 306,535 9,771 306,771',
     'animate',
     {'command': 'radar two three', 'duration': 0.25,
      'width': 100, 'height': 200,
      'frames': '9,65 306,65 9,300 306,300 9,535 306,535 9,771 306,771'})
])
def test_crop(msg, cmd, kwargs):
    assert list(parse_msg(msg)) == [(cmd, kwargs)]
