from pyparsing import Optional, Group, oneOf, delimitedList
from common.parse import keyword, Command, number

from pyparsing import pyparsing_common


ModeOption = oneOf('replace both', caseless=True)('mode')

Integer = pyparsing_common.integer
Geometry1 = (Integer('width') + 'x' + Integer('height')
             + '+' + Integer('left') + '+' + Integer('top'))
Geometry2 = ('+' + Integer('left') + '+' + Integer('top')
             + '-' + Integer('right') + '-' + Integer('bottom'))
Geometry3 = Integer('width') + 'x' + Integer('height')

CropCommand = (keyword('crop', action='crop')('action')
               + Optional(ModeOption, default='replace')('mode')
               + (Geometry1() | Geometry2())
                + Command('command'))

Duration = number

Frame = Integer + ',' + Integer
Frames = delimitedList(Frame, delim=' ', combine=True)

AnimateCommand = (keyword('animate', action='animate')('action')
                  + Command('command')
                  + Duration('duration')
                  + Geometry3()
                  + Frames('frames'))

Message = Group(CropCommand | AnimateCommand)


def parse_msg(msg):
    for res in Message.parseString(msg, parseAll=True):
        kwargs = {key: res[key]
                  for key in ['command', 'mode',
                              'width', 'height',
                              'left', 'top',
                              'right', 'bottom',
                              'duration', 'frames']
                  if key in res}

        yield res['action'], kwargs


HELP = """
*crop* [*replace*|both] WIDTHxHEIGHT+LEFT+TOP _command_
*crop* [*replace*|both] +LEFT+TOP-RIGHT-BOTTOM _command_
*animate* _command_ _duration_ WIDTHxHEIGHT frame1_x,frame2_y frame2_x,frame2_y ...
"""
