import io

from chat.modules import run_command
from chat.msg_types import Photo, ReplyList

from PIL import Image


def do_crop(command, mode='replace',
            width=None, height=None,
            left=None, top=None,
            right=None, bottom=None):
    result = ReplyList()
    for reply in run_command(command)[1]:
        if is_image(reply):
            cropped, original \
                = crop_image(reply.photos[0].read(),
                             width, height, left, top, right, bottom)
            result.then(cropped.module(name=reply.get_module()))
            if mode == 'both':
                result.then(original.module(name=reply.get_module()))
        else:
            result.then(reply)
    return result


def is_image(reply):
    return hasattr(reply, 'photos') and len(reply.photos) == 1


def crop_image(data, width, height, left, top,
               right=None, bottom=None):
    original = io.BytesIO(data)
    img = Image.open(original)
    if width is None and height is None:
        width, height = img.size
        img2 = img.crop((left, top, width - right, height - bottom))
    else:
        img2 = img.crop((left, top, left + width, top + height))
    cropped = io.BytesIO()
    img2.save(cropped, format=img.format)
    original.seek(0)
    cropped.seek(0)
    return Photo([cropped]), Photo([original])


def do_animate(command, duration, width, height, frames):
    frames = [tuple(int(d) for d in frame.split(','))
              for frame in frames.split()]
    result = ReplyList()
    for reply in run_command(command)[1]:
        if is_image(reply):
            animated = animate_image(reply.photos[0].read(),
                                     duration, width, height, frames)
            result.then(animated.module(name=reply.get_module()))
        else:
            result.then(reply)
    return result


def animate_image(data, duration, width, height, frames):
    img = Image.open(io.BytesIO(data))
    img2 = [img.crop((left, top, left + width, top + height))
            for left, top in frames]
    # TODO: progress bar
    animated = io.BytesIO()

    # long duration for first frame, faster for later frames
    # TODO: parameterise this
    total = int(duration * 1000 * len(img2))
    delays = [total // 4, total // 4,
              total // 8, total // 8] + [total // 16] * (len(img2) - 4)
    img2[0].save(animated,
                 format='GIF',
                 save_all=True,
                 append_images=img2[1:],
                 duration=delays,
                 loop=0)
    animated.seek(0)
    return Photo([animated], animated=True)
