import pytest
from . import parse_msg


@pytest.mark.parametrize('msg,cmd,kwargs', [
    ('/config bank', 'list', {'module': 'bank'}),
    ('/config list bank', 'list', {'module': 'bank'}),
    ('/config set bank uid 1234',
     'set', {'module': 'bank', 'key': 'uid', 'value': '1234'}),
    ('/config set bank key_name "quoted value"',
     'set', {'module': 'bank', 'key': 'key_name', 'value': 'quoted value'}),
    ('Config set weightlog weight_max 76.0',
     'set', {'module': 'weightlog', 'key': 'weight_max', 'value': '76.0'}),
    ('/config add bank uid 1234',
     'add', {'module': 'bank', 'key': 'uid', 'value': '1234'}),
    ('/config delete bank uid', 'delete', {'module': 'bank', 'key': 'uid'}),
])
def test_config(msg, cmd, kwargs):
    assert list(parse_msg(msg)) == [(cmd, kwargs)]
