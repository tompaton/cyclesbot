import pytest

from flexmock import flexmock
from unittest.mock import patch, mock_open
from . import var_types


def test_input_var():
    (flexmock(var_types.getpass)
     .should_receive('getpass')
     .with_args('Bank password: ')
     .and_return('secret'))

    var = var_types.InputVar('Bank password')
    var.input_value()
    assert var.value == 'secret'


def test_env_var():
    flexmock(var_types.os, environ={'VAR': 'value'})
    var = var_types.EnvVar('VAR')
    assert var.value == 'value'


def test_env_var_undef():
    flexmock(var_types.os, environ={})
    var = var_types.EnvVar('VAR')
    assert var.value is None


def test_env_var_default():
    flexmock(var_types.os, environ={})
    var = var_types.EnvVar('VAR', default='default')
    assert var.value == 'default'


def test_yaml_file_var():
    var = var_types.YamlFileVar('path/to/open')
    data = "- a\n- [b, c]\n"

    with patch("builtins.open", mock_open(read_data=data)) as mock_file:
        assert var.value == ['a', ['b', 'c']]

        mock_file.assert_called_with('path/to/open', 'r')


def test_yaml_file_var_default():
    var = var_types.YamlFileVar('path/to/open', default=[])

    with patch("builtins.open", mock_open()) as mock_file:
        mock_file.side_effect = FileNotFoundError()

        assert var.value == []

        mock_file.assert_called_with('path/to/open', 'r')


@pytest.mark.parametrize('default,value,result', [
    (None, None, None),
    ('', None, ''),
    ('234', None, '234'),
    (None, '', ''),
    ('', '', ''),
    ('234', '', ''),
    (None, '123', '123'),
    ('', '123', '123'),
    ('234', '123', '123'),
])
def test_configparam(default, value, result):
    var = var_types.ConfigParam('test', default=default)
    with patch.object(var_types, 'client') as client:
        client.get_value.return_value = [('module', 'name', value)]
        assert var.value == result
