from pyparsing import Optional, Suppress, Group, Word, alphanums
from common.parse import keyword, QuotedString

Module = Word(alphanums + '_').setName('module')
Key = Word(alphanums + '_').setName('key')
Value = QuotedString().setName('value')

ListCommand = ((keyword('config', 'list', action='list')
                | keyword('config', action='list'))('action')
               + Module('module'))
SetCommand = (keyword('config', 'set', action='set')('action')
              + Module('module') + Key('key') + Value('value'))
AddCommand = (keyword('config', 'add', action='add')('action')
              + Module('module') + Key('key') + Value('value'))
DeleteCommand = (keyword('config', 'delete', action='delete')('action')
                 + Module('module') + Key('key'))

Message = Optional(Suppress('/')) + Group(SetCommand
                                          | AddCommand
                                          | DeleteCommand
                                          | ListCommand)


def parse_msg(msg):
    for res in Message.parseString(msg, parseAll=True):
        kwargs = {key: res[key] for key in ('module', 'key', 'value')
                  if key in res}
        yield res['action'], kwargs


HELP = """
*/config [list]* _module_
*/config set* _module_ _key_ _value_
*/config add* _module_ _key_ _value_
*/config delete* _module_ _key_
"""
