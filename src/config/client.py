import importlib
import sqlite3
from collections import namedtuple
from common import util


def do_list(module):
    # TODO: show var name
    rows = join_vars(get_vars(module), get_config(module=module))
    return ('\n'.join(format_row(row) for row in rows)
            or 'No config for {}.'.format(module))

def format_row(row):
    return '{} *{}* "{}"{}'.format(row.module, row.key,
                                   'None' if row.value is None
                                   else util.escape_markdown(row.value),
                                   ' (default)' if row.rowid is None else '')

def do_set(module, key, value):
    if not valid_key(module, key):
        return 'Unknown config key.'

    _undo = do_delete(module, key)[1]

    with get_conn() as conn:
        conn.execute("""INSERT INTO config (module, key, value, created)
                        VALUES (?, ?, ?, datetime('now'))""",
                     (module, key, value))

    return 'Set ' + format_row(ConfigRow(module, key, value, 0, 0, 0)), _undo

def do_add(module, key, value):
    values = get_value(module, key)
    new_value = ','.join([row.value for row in values if row.value] + [value])

    return do_set(module, key, new_value)

def do_delete(module, key):
    values = get_value(module, key)

    with get_conn() as conn:
        for row in values:
            conn.execute("UPDATE config SET deleted=datetime('now') "
                         "WHERE rowid = ?",
                         (row.rowid,))

    def _undo():
        with get_conn() as conn:
            for row in values:
                conn.execute("UPDATE config SET deleted=NULL WHERE rowid = ?",
                             (row.rowid,))
        return '\n'.join('Restored ' + format_row(row) for row in values)

    return 'Deleted {} *{}*'.format(module, key), _undo

def get_conn():
    conn = sqlite3.connect('/data/config.db')
    conn.execute("""CREATE TABLE IF NOT EXISTS
                    config (module text, key text, value text,
                            created integer, deleted integer)""")
    return conn

def migrate_db_1():  # pragma: no cover
    get_conn().execute("""ALTER TABLE config ADD COLUMN "created" integer""")
    get_conn().execute("""ALTER TABLE config ADD COLUMN "deleted" integer""")
    with get_conn() as conn:
        conn.execute("""UPDATE config SET "created"=datetime('now')""")

ConfigRow = namedtuple('ConfigRow', 'module key value created deleted rowid')

def get_config(**kwargs):
    # TODO: remove in python 3.6 with sorted dicts
    kwargs = sorted(kwargs.items())
    cur = get_conn().cursor()
    cur.execute("SELECT module, key, value, created, deleted, rowid "
                "FROM config WHERE {} AND deleted IS NULL"
                .format(' AND '.join(key + ' = ?' for key, value in kwargs)),
                tuple(value for key, value in kwargs))
    return [ConfigRow(*row) for row in cur.fetchall()]

def get_value(module, key):
    return get_config(module=module, key=key)

def valid_key(module, key):
    # TODO: check module.config.key exists
    return True

def get_vars(module_name):
    from config.var_types import ConfigParam

    module = importlib.import_module(module_name)
    config = importlib.import_module('.config', module_name)

    defaults = []
    for key in dir(config):
        var = getattr(config, key)
        if isinstance(var, ConfigParam):
            # during tests, won't have called chat.config.initialize, so provide
            # fallback for .module and .key
            defaults.append(ConfigRow(var.module or module.__name__,
                                      var.key or key,
                                      var.default,
                                      None, None, None))

    return defaults

def join_vars(defaults, values):
    result = {(row.module, row.key): row for row in defaults}
    for row in values:
        result[(row.module, row.key)] = row
    return [result[key] for key in sorted(result.keys())]
