import functools
import getpass
import os
import yaml

from . import client


class BaseParam():

    def __init__(self, default=None):
        self.default = default

    def get_value(self):  # pragma: no cover
        return None

    @property
    def value(self):
        value = self.get_value()
        if self.default is None:
            return value
        elif value is None:
            return self.default
        else:
            return value

    @property
    def values(self):
        return (self.value or '').split(',')

    def required(self, result="Function not enabled."):
        '''decorator which will return "Function not enabled" if config param
        is not defined'''

        def inner(func):
            @functools.wraps(func)
            def wrapped(*args, **kwargs):
                if self.value is None:
                    return result
                return func(*args, **kwargs)
            return wrapped

        return inner


class EnvVar(BaseParam):
    def __init__(self, name, default=None):
        super().__init__(default=default)
        self.name = name

    def get_value(self):
        value = os.environ.get(self.name)

        # TODO: description - print at console if not set and module disabled
        if value is None:
            if self.default is None:
                print('{} functions disabled: export {}="{}"'
                      .format('module', self.name, 'desc'))
            else:
                print('{} using default value: export {}="{}"'
                      .format('module', self.name, 'desc'))

        return value


class YamlFileVar(BaseParam):
    def __init__(self, filename, default=None):
        super().__init__(default=default)
        self.filename = filename

    def get_value(self):
        try:
            with open(self.filename, 'r') as yaml_file:
                return yaml.safe_load(yaml_file)
        except yaml.YAMLError as exc:
            print(exc)
        except FileNotFoundError:
            pass


class InputVar(BaseParam):
    def __init__(self, name):
        super().__init__()
        self.name = name
        self._value = None

    def input_value(self):
        self._value = getpass.getpass('{}: '.format(self.name)) or None

    def get_value(self):
        return self._value


class ConfigParam(BaseParam):
    def __init__(self, name, default=None):
        super().__init__(default=default)
        self.name = name
        self.module = None
        self.key = None

    def get_value(self):
        for row in client.get_value(self.module, self.key):
            return row[2]
