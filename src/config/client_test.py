from flexmock import flexmock
from common.test import mock_contextmanager
from . import client


def test_get_conn():
    conn = flexmock()

    (flexmock(client.sqlite3)
     .should_receive('connect')
     .with_args('/data/config.db')
     .and_return(conn))

    (flexmock(conn)
     .should_receive('execute')
     .with_args("""CREATE TABLE IF NOT EXISTS
                    config (module text, key text, value text,
                            created integer, deleted integer)""")
     .once())

    assert client.get_conn() == conn


def test_get_config():
    conn = flexmock()

    (flexmock(client)
     .should_receive('get_conn')
     .and_return(conn))

    cur = flexmock()

    (flexmock(conn)
     .should_receive('cursor')
     .and_return(cur))

    (flexmock(cur)
     .should_receive('execute')
     .with_args("SELECT module, key, value, created, deleted, rowid "
                "FROM config WHERE module = ? AND deleted IS NULL",
                ('bank',))
     .once())

    (flexmock(cur)
     .should_receive('fetchall')
     .and_return([('bank', 'uid', '1234', 1234567, None, 1)]))

    assert client.get_config(module='bank') \
        == [client.ConfigRow('bank', 'uid', '1234', 1234567, None, 1)]


def test_get_value():
    conn = flexmock()

    (flexmock(client)
     .should_receive('get_conn')
     .and_return(conn))

    cur = flexmock()

    (flexmock(conn)
     .should_receive('cursor')
     .and_return(cur))

    (flexmock(cur)
     .should_receive('execute')
     .with_args('SELECT module, key, value, created, deleted, rowid '
                'FROM config WHERE key = ? AND module = ? '
                'AND deleted IS NULL',
                ('uid', 'bank'))
     .once())

    (flexmock(cur)
     .should_receive('fetchall')
     .and_return([('bank', 'uid', '1234', 1234567, None, 1)]))

    assert client.get_value('bank', 'uid') \
        == [client.ConfigRow('bank', 'uid', '1234', 1234567, None, 1)]


def test_list_empty():
    (flexmock(client)
     .should_receive('get_config')
     .with_args(module='bank')
     .and_return([]))

    (flexmock(client)
     .should_receive('get_vars')
     .with_args('bank')
     .and_return([]))

    assert client.do_list('bank') == 'No config for bank.'


def test_list():
    (flexmock(client)
     .should_receive('get_config')
     .with_args(module='bank')
     .and_return([client.ConfigRow('bank', 'uid', '1234_567',
                                   1234567, None, 1)]))

    (flexmock(client)
     .should_receive('get_vars')
     .with_args('bank')
     .and_return([client.ConfigRow('bank', 'acc', None, None, None, None)]))

    assert client.do_list('bank') == ('bank *acc* "None" (default)\n'
                                      r'bank *uid* "1234\_567"')


def test_set():
    (flexmock(client)
     .should_receive('valid_key')
     .with_args('bank', 'uid')
     .and_return(True))

    (flexmock(client)
     .should_receive('do_delete')
     .with_args('bank', 'uid')
     .and_return(('ignored', 'undo callback')))

    conn = flexmock()

    (flexmock(client)
     .should_receive('get_conn')
     .and_return(conn)
     .once())

    (flexmock(conn)
     .should_receive('execute')
     .with_args("""INSERT INTO config (module, key, value, created)
                        VALUES (?, ?, ?, datetime('now'))""",
                ('bank', 'uid', '1234_567'))
     .once())

    reply = client.do_set('bank', 'uid', '1234_567')

    assert reply[0] == r'Set bank *uid* "1234\_567"'
    assert reply[1] == 'undo callback'


def test_set_invalid():
    (flexmock(client)
     .should_receive('valid_key')
     .with_args('bank', 'uid')
     .and_return(False))

    assert client.do_set('bank', 'uid', '1234') == 'Unknown config key.'


def test_add():
    (flexmock(client)
     .should_receive('get_value')
     .with_args('bank', 'uid')
     .and_return([client.ConfigRow('bank', 'uid', '1234',
                                   1234567, None, 1)]))

    (flexmock(client)
     .should_receive('do_set')
     .with_args('bank', 'uid', '1234,5678')
     .and_return('added'))

    assert client.do_add('bank', 'uid', '5678') == 'added'


def test_add_to_empty():
    (flexmock(client)
     .should_receive('get_value')
     .with_args('bank', 'uid')
     .and_return([]))

    (flexmock(client)
     .should_receive('do_set')
     .with_args('bank', 'uid', '5678')
     .and_return('added'))

    assert client.do_add('bank', 'uid', '5678') == 'added'


def test_delete():
    (flexmock(client)
     .should_receive('get_value')
     .with_args('bank', 'uid')
     .and_return([client.ConfigRow('bank', 'uid', '1234',
                                   1234567, None, 1)]))

    conn = flexmock()

    (flexmock(client)
     .should_receive('get_conn')
     .replace_with(mock_contextmanager(conn)))

    (flexmock(conn)
     .should_receive('execute')
     .with_args("UPDATE config SET deleted=datetime('now') "
                "WHERE rowid = ?",
                (1,))
     .once())

    reply = client.do_delete('bank', 'uid')

    assert reply[0] == 'Deleted bank *uid*'

    # undo
    (flexmock(conn)
     .should_receive('execute')
     .with_args("UPDATE config SET deleted=NULL WHERE rowid = ?",
                (1,))
     .once())

    assert reply[1]() == 'Restored bank *uid* "1234"'


def test_valid_key():
    assert client.valid_key('bank', 'uid')


def test_join_vars():
    d1 = client.ConfigRow('module', 'var1', 'default1', None, None, None)
    d2 = client.ConfigRow('module', 'var2', 'default2', None, None, None)
    v1 = client.ConfigRow('module', 'var1', 'value1', 1234567, None, 1)
    assert client.join_vars([d1, d2], [v1]) == [v1, d2]


def test_get_vars():
    assert [row[:3] for row in client.get_vars('bank')] \
        == [('bank', 'recent_days', '7'),
            ('bank', 'uid', None)]
