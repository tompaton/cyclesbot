from datetime import datetime
from logging.handlers import RotatingFileHandler
from chat.msg_types import Reply, ReplyList, Question
from common.util import escape_markdown


last_msg = 'No messages received yet.'

def do_huh():
    return last_msg

def do_unparsed():
    with _open_log('r') as l:
        return l.read()

def do_add():
    with _open_log('a') as l:
        l.write('{0} {1}\n'.format(_now(), last_msg))

def _now():  # pragma: no cover
    return datetime.now().strftime('%Y-%m-%d %H:%M:%S')

def _open_log(mode):  # pragma: no cover
    return open('/data/unparsed.log', mode)

def last_message(msg, log=False):
    global last_msg

    last_msg = msg

    if log:
        do_add()


def do_clear():
    RotatingFileHandler('/data/unparsed.log',
                        maxBytes=1,
                        backupCount=999).doRollover()
    return Reply('Unparsed messages archived.')


def do_reminders():
    replies = []
    with _open_log('r') as l:
        for line in l.readlines():
            if 'Last message not parsed' in line:
                dt = datetime.strptime(line[:19], '%Y-%m-%d %H:%M:%S')
                msg = line[45:].strip()
                if msg.lower() in ('n', 'no', 'y', 'yes'):
                    continue
                replies.append(dt.strftime('%a %H:%M')
                               + ' ' + escape_markdown(msg))

    def _clear(text):
        if text.lower() in ('y', 'yes'):
            return do_clear()

    if replies:
        return ReplyList(*replies).then(Question('Clear?', _clear))
