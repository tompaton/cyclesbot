import pytest
from . import parse_msg


@pytest.mark.parametrize('msg,cmd,kwargs', [
    ('/huh?', 'huh', {}),
    ('huh?', 'huh', {}),
    ('huh', 'huh', {}),
    ('unparsed add', 'add', {}),
    ('unparsed clear', 'clear', {}),
    ('unparsed log', 'unparsed', {}),
    ('unparsed', 'reminders', {}),
])
def test_trace(msg, cmd, kwargs):
    assert list(parse_msg(msg)) == [(cmd, kwargs)]
