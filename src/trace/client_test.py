from flexmock import flexmock
from chat.msg_types import Reply
from . import client


def test_huh():
    assert client.do_huh() == 'No messages received yet.'


def test_last_message():
    client.last_message('do something')
    assert client.do_huh() == 'do something'


def test_unparsed():
    log = flexmock(read=lambda: 'timestamp message\n')

    (flexmock(client)
     .should_receive('_open_log')
     .with_args('r')
     .and_return(log)
     .once())

    assert client.do_unparsed() == 'timestamp message\n'


def test_last_message_log():
    (flexmock(client)
     .should_receive('_now')
     .and_return('timestamp')
     .once())

    log = flexmock()

    (flexmock(log)
     .should_receive('write')
     .with_args('timestamp do something\n')
     .once())

    (flexmock(client)
     .should_receive('_open_log')
     .with_args('a')
     .and_return(log)
     .once())

    client.last_message('do something', log=True)


def test_do_clear():
    handler = flexmock()

    (flexmock(client)
     .should_receive('RotatingFileHandler')
     .with_args('/data/unparsed.log', maxBytes=1, backupCount=999)
     .and_return(handler))

    (flexmock(handler)
     .should_receive('doRollover')
     .with_args()
     .once())

    assert client.do_clear().message == 'Unparsed messages archived.'


def test_do_reminders_empty():
    lines = ['2020-02-28 13:30:23 Last message not parsed: n\n',
             '2020-02-28 13:30:23 Last message not parsed: Y\n',
             '2020-02-28 13:30:23 Last message was Ok: ignored\n']
    log = flexmock(readlines=lambda: lines)

    (flexmock(client)
     .should_receive('_open_log')
     .with_args('r')
     .and_return(log)
     .once())

    replies = client.do_reminders()

    assert replies is None


def test_do_reminders():
    lines = ['2020-02-05 12:17:41 Last message not parsed: Lorem_ipsum\n',
             '2020-02-28 13:30:23 Last message not parsed: dolor\n',
             '2020-02-28 13:30:23 Last message was Ok: ignored\n',
             '2020-03-11 13:11:06 Last message not parsed: sit amet\n']
    log = flexmock(readlines=lambda: lines)

    (flexmock(client)
     .should_receive('_open_log')
     .with_args('r')
     .and_return(log)
     .once())

    replies = client.do_reminders()

    assert replies._messages == ['Wed 12:17 Lorem\_ipsum',
                                 'Fri 13:30 dolor',
                                 'Wed 13:11 sit amet',
                                 'Clear?']


def test_do_reminders_clear():
    lines = ['2020-02-05 12:17:41 Last message not parsed: Lorem ipsum\n']
    log = flexmock(readlines=lambda: lines)

    (flexmock(client)
     .should_receive('_open_log')
     .with_args('r')
     .and_return(log)
     .once())

    replies = client.do_reminders()

    assert replies._messages == ['Wed 12:17 Lorem ipsum',
                                 'Clear?']

    assert replies.replies[-1].callback('no') is None

    (flexmock(client)
     .should_receive('do_clear')
     .with_args()
     .and_return(Reply('Unparsed messages archived.')))

    assert replies.replies[-1].callback('y').message \
        == 'Unparsed messages archived.'
