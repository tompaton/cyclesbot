from pyparsing import Group, oneOf, Optional, Suppress
from common.parse import keyword

LastCommand = (Optional(Suppress('/'))
               + oneOf('huh', caseless=True)('command')
               + Optional(Suppress('?')))
UnparsedAddCommand = keyword('unparsed', 'add', action='add')
UnparsedLogCommand = keyword('unparsed', 'log', action='unparsed')
UnparsedClearCommand = keyword('unparsed', 'clear', action='clear')
RemindersCommand = keyword('unparsed', action='reminders')
Message = Group(LastCommand
                | UnparsedAddCommand('command')
                | UnparsedClearCommand('command')
                | UnparsedLogCommand('command')
                | RemindersCommand('command'))

def parse_msg(msg):
    for res in Message.parseString(msg):
        yield res['command'], {}

HELP = """
*huh*[?]
*unparsed* [log|add|clear]
"""
