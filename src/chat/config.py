from chat.msg_types import Reply
from config.var_types import ConfigParam, EnvVar, InputVar
from config import client

telegram_token = EnvVar('TOKEN', default='NO_TOKEN')
telegram_id = EnvVar('ID_ME', default=0)
telegram_id2 = EnvVar('ID_ME2', default=0)

def initialize():
    from .msg_types import Questions
    from . import modules as chat_modules

    qns = Questions()

    for module in chat_modules._modules:
        if hasattr(module, 'config'):
            for key in dir(getattr(module, 'config')):
                var = getattr(getattr(module, 'config'), key)

                if isinstance(var, InputVar):
                    var.input_value()

                if isinstance(var, ConfigParam):
                    var.module = module.__name__
                    var.key = key

                    if var.value is None:
                        qns.add('{}__{}'.format(var.module, var.key),
                                '{}?'.format(var.name))

    if qns.done(_save, push=True):
        chat_modules.set_callback_ex(qns, 'config')


def _save(**responses):
    for key, value in responses.items():
        name, var = key.split('__')

        print('Save {}.{} = "{}"'.format(name, var, value))
        client.do_set(name, var, value)

    return Reply('Config saved.')
