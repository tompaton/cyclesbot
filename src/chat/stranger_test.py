import pytest
from flexmock import flexmock


@pytest.fixture
def tally():
    return {}


@pytest.fixture
def stranger(tally):
    from . import stranger
    stranger.tally.clear()
    for uid, count in tally.items():
        stranger.tally[uid] = count
    return stranger


def test_cant_talk_me(stranger):
    msg = {'from': {'id': stranger.ID_US[0]}}

    (flexmock(stranger.telepot)
     .should_receive('glance')
     .with_args(msg)
     .and_return(('text', 'text', stranger.ID_US[0])))

    assert stranger.cant_talk(msg) is None

    assert stranger.tally == {0: 1}


@pytest.mark.parametrize('tally,i,reply', [
    ({}, 0, "Hi Bob, sorry, can't talk now."),
    ({1234: 1}, 1, "Hi Bob, sorry, can't talk now."),
    ({1234: 2}, 2, "No, really."),
    ({1234: 3}, 3, "No, really."),
    ({1234: 4}, 4, "No, really."),
])
def test_cant_talk(stranger, i, reply):
    msg = {'from': {'id': 1234, 'first_name': 'Bob'}, 'text': 'Hello?'}

    (flexmock(stranger.telepot)
     .should_receive('glance')
     .with_args(msg)
     .and_return(('text', 'text', 1234)))

    (flexmock(stranger.bot)
     .should_receive('sendMessage')
     .with_args(1234, reply)
     .once())

    (flexmock(stranger.bot)
     .should_receive('sendMessage')
     .with_args(stranger.ID_US[0],
                "Got text message from {}:\nHello?"
                .format(repr(msg['from'])),
                parse_mode=None,
                disable_notification=False,
                disable_web_page_preview=False,
                tag=None, delete_tag=None)
     .once())

    assert stranger.cant_talk(msg)

    assert stranger.tally == {1234: i + 1}


@pytest.mark.parametrize('tally', [{1234: 5}])
def test_cant_talk_quiet(stranger):
    msg = {'from': {'id': 1234, 'first_name': 'Bob'},
           'text': 'Hello?'}

    (flexmock(stranger.telepot)
     .should_receive('glance')
     .with_args(msg)
     .and_return(('text', 'text', 1234)))

    (flexmock(stranger.bot)
     .should_receive('sendMessage')
     .with_args(stranger.ID_US[0],
                "Got text message from {}:\nHello?"
                .format(repr(msg['from'])),
                parse_mode=None,
                disable_notification=False,
                disable_web_page_preview=False,
                tag=None, delete_tag=None)
     .once())

    assert stranger.cant_talk(msg)

    assert stranger.tally == {1234: 6}


@pytest.mark.parametrize('tally', [{1234: 5}])
def test_cant_talk_not_text(stranger):
    msg = {'from': {'id': 1234, 'first_name': 'Bob'}}

    (flexmock(stranger.telepot)
     .should_receive('glance')
     .with_args(msg)
     .and_return(('other', 'chat_type', 1234)))

    (flexmock(stranger.bot)
     .should_receive('sendMessage')
     .with_args(stranger.ID_US[0],
                "Got other message from {}:\nunknown"
                .format(repr(msg['from'])),
                parse_mode=None,
                disable_notification=False,
                disable_web_page_preview=False,
                tag=None, delete_tag=None)
     .once())

    assert stranger.cant_talk(msg)

    assert stranger.tally == {1234: 6}
