import telepot

from . import bot_wrapper
from . import config
from .parse import parse_msg


ID_ME = int(config.telegram_id.value)
ID_ME2 = int(config.telegram_id2.value)
ID_US = [ID_ME, ID_ME2]

bot = bot_wrapper.BotWrapper(telepot.Bot(config.telegram_token.value), ID_ME)
