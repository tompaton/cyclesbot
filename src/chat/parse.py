from pyparsing import Group, Optional, Word, alphanums
from common.parse import keyword, number, Command2


ChatID = number('chat_id').setName('chat_id')

SendCommand = (keyword('cc send')('action')
               + ChatID
               + Command2)

LastCommand = (keyword('last')('action')
               + Optional(number, default=1)('num')
               + Command2)

LastCount = keyword('last', 'count', action='last_count')('action')

Module = Word(alphanums).setName('module')
EchoImageCommand = (keyword('echo', 'image', action='echo_image')('action')
                    + Module('module')
                    + Command2)

EchoCommand = keyword('echo')('action') + Module('module') + Command2

MuteCommand = keyword('mute')('action') + Command2


ClearFlag = keyword('clear')

Tag = Word(alphanums + '.-').setName('tag')

DeDupeCommand = (keyword('dedupe')('action')
                 + Optional(ClearFlag, default='')('clear')
                 + Tag('tag')
                 + Command2)

DashboardCommand = (keyword('dashboard')('action')
                    + keyword('clear export')('command'))

Message = Group(SendCommand | LastCount | LastCommand
                | EchoImageCommand | EchoCommand | MuteCommand
                | DeDupeCommand | DashboardCommand)

def parse_msg(msg):
    for res in Message.parseString(msg):
        yield (res['action'],
               {key: res[key] for key in ('chat_id', 'command', 'num', 'tag',
                                          'clear', 'module')
                if key in res})

HELP = """
*send* _chat-id_ _command_
*cc* _chat-id_ _command_
*last* [num] _command_
*last count*
*mute* _command_
*echo* _module_ _message_
*echo image* _module_ _url_
*dedupe* [clear] _tag_ _command_
*dashboard* [clear|export]
"""
