import io
import sys

from collections import namedtuple
from pathlib import Path

from telepot.exception import TelegramError
from urllib3.exceptions import ReadTimeoutError
from better_exceptions import format_exception


class ReplyBase():
    def __init__(self, undo=None, callback=None, history_key=None, **kwargs):
        self.undo = undo
        self.callback = callback
        self.history_key = history_key
        self.send_args = kwargs

    def get_send_args(self, force_notification=False):
        send_args = {
            # telegram api
            'disable_notification': True,

            # for BotWrapper
            'tag': None,
            'delete_tag': None,
        }

        if force_notification:
            send_args['disable_notification'] = False

        send_args.update(self.send_args)

        return send_args

    def send(self, bot, chat_id, **kwargs):
        raise NotImplementedError()

    def push(self, force_notification=True):
        from . import ID_ME, bot
        return self.send(bot, ID_ME, force_notification=force_notification)

    def send_to(self, chat_id):
        from . import bot
        return self.send(bot, chat_id)

    def cc(self, chat_id):
        from . import bot
        return self.send(bot, chat_id, force_notification=True)

    def then(self, reply):
        return ReplyList(self, reply)

    def module(self, module=None, name=None):
        if name is None:
            if module is None:
                return self

            name = module.__name__

        current = self.get_module()

        minor = ['chat', 'cron', 'graph', 'image', 'trace', 'undo', 'words']
        if current is None or (current in minor and name not in minor):
            self.send_args['module'] = name

        return self

    def get_module(self):
        return self.send_args.get('module')


class Reply(ReplyBase):
    # need to specify undo for from_response tuple unpacking to work
    def __init__(self, message, undo=None, **kwargs):
        self.message = message
        kwargs.setdefault('parse_mode', 'Markdown')
        kwargs.setdefault('disable_web_page_preview', False)
        super().__init__(undo=undo, **kwargs)

    def send(self, bot, chat_id, **kwargs):
        print(self.__class__.__name__, self.message or '{empty}')
        if not self.message:
            return False

        for retry in range(3):
            try:
                bot.sendMessage(chat_id, self.message,
                                **self.get_send_args(**kwargs))
                return True

            except (TelegramError, ReadTimeoutError) as ex:
                print('Retry {}: {}'.format(retry + 1, type(ex).__name__))
                print(format_exception(*sys.exc_info()))

        return False

    def replace(self, old, new):
        self.message = self.message.replace(old, new)
        return self

    @classmethod
    def from_cmd(cls, module, cmd, kwargs):
        result = getattr(module.client, 'do_{}'.format(cmd))(**kwargs)

        return cls.from_response(result).module(module)

    @classmethod
    def from_response(cls, result, **kwargs):
        if isinstance(result, ReplyBase):
            return result
        elif isinstance(result, tuple):
            # old-style tuple of (message, undo_callback)
            return cls(*result, **kwargs)
        else:
            # simple message returned
            return cls(result, **kwargs)


class Question(Reply):
    # no undo_callback
    def __init__(self, message, callback, **kwargs):
        super().__init__(message, callback=callback, **kwargs)
        self.send_args['skip_dashboard'] = True


class Photo(ReplyBase):
    def __init__(self, photos, **kwargs):
        self.photos = photos
        self.animated = kwargs.pop('animated', False)
        kwargs.setdefault('disable_notification', False)
        super().__init__(**kwargs)

    def send(self, bot, chat_id, **kwargs):
        error = None

        for photo in self.photos:
            bot.sendChatAction(chat_id, 'upload_photo')
            try:
                if self.animated:
                    bot.sendVideo(chat_id, video=photo,
                                  **self.get_send_args(**kwargs))
                else:
                    bot.sendPhoto(chat_id, photo=photo,
                                  **self.get_send_args(**kwargs))
            except (TelegramError, ReadTimeoutError) as ex:
                error = '{} {}'.format(ex, photo)
            else:
                return True

        if error:
            bot.sendMessage(chat_id, error)

        return True


class Document(ReplyBase):
    def __init__(self, filename, document, **kwargs):
        self.filename = filename
        self.document = document
        kwargs.setdefault('disable_notification', False)
        super().__init__(**kwargs)

    def send(self, bot, chat_id, **kwargs):
        bot.sendChatAction(chat_id, 'upload_document')

        bot.sendDocument(chat_id,
                         document=(self.filename, io.BytesIO(self.document)),
                         **self.get_send_args(**kwargs))
        return True


class Audio(ReplyBase):
    def __init__(self, url, artist=None, title=None, album=None, duration=None,
                 **kwargs):
        self.url = url
        self.artist = artist
        self.title = title
        self.album = album
        self.duration = duration
        super().__init__(**kwargs)

    def send(self, bot, chat_id, **kwargs):
        bot.sendChatAction(chat_id, 'upload_audio')
        bot.sendAudio(chat_id,
                      audio=self.open_audio(self.url),
                      caption=self.album,
                      duration=self.duration,
                      performer=self.artist,
                      title=self.title,
                      **self.get_send_args(**kwargs))
        return True

    @staticmethod
    def open_audio(url):
        audio = Path(url)
        return (audio.name, audio.open('rb'))


class ReplyList(ReplyBase):
    def __init__(self, *replies, **kwargs):
        super().__init__()

        self.replies = []
        for reply in replies:
            if isinstance(reply, ReplyList):
                for sub_reply in reply.replies:
                    self.then(sub_reply)
            else:
                self.then(Reply.from_response(reply, **kwargs))

    def then(self, reply):
        self.replies.append(reply)

        # undo from first reply
        if not self.undo:
            self.undo = self.replies[0].undo

        return self

    def send(self, bot, chat_id, **kwargs):
        results = [reply.send(bot, chat_id, **kwargs) for reply in self.replies]

        # callback from last sent reply
        for reply, result in zip(self.replies, results):
            if result:
                self.callback = reply.callback

        return any(results)

    @property
    def _messages(self):
        """helper for unit tests"""
        return [reply.message for reply in self.replies]

    def tag(self, tag=None, delete_tag=None):
        for reply in self.replies:
            reply.send_args['tag'] = tag

        if delete_tag:
            self.replies[0].send_args['delete_tag'] = delete_tag

        return self

    def module(self, module=None, name=None):
        for reply in self.replies:
            reply.module(module, name)
        return self


class Questions():
    Prompt = namedtuple('Prompt', 'message key required default parse_mode')

    def __init__(self):
        self.prompts = []
        self.responses = {}
        self.callback = None

        self.prompt = None

        self.default_responses = ('Y', 'y')
        self.abort_responses = ('N', 'n', '-')

    def add(self, key, message, required=True, default=None,
            parse_mode='Markdown'):
        self.prompts.append(self.Prompt(message, key, required, default,
                                        parse_mode))
        return self

    def done(self, callback, push=False):
        self.callback = callback
        if push:
            if self.prompts:
                self(None).push()
                return True
            else:
                return None
        else:
            return self(None)

    def __call__(self, response):
        if self.prompt:
            if response in self.abort_responses:
                if self.prompt.required:
                    return Reply('Aborted.')
                else:
                    response = None

            if self.prompt.default is not None:
                if response in self.default_responses:
                    response = self.prompt.default

            self.responses[self.prompt.key] = response

        if self.prompts:
            self.prompt = self.prompts.pop(0)
            return Question(self._format_message(self.prompt),
                            callback=self,
                            parse_mode=self.prompt.parse_mode,
                            disable_web_page_preview=True)
        else:
            return self.callback(**self.responses)

    def _format_message(self, prompt):
        if prompt.default is not None:
            return '{} {}[{}] "{}"'.format(
                prompt.message,
                '\\' if prompt.parse_mode == 'Markdown' else '',
                self.default_responses[0],
                prompt.default)
        else:
            return prompt.message
