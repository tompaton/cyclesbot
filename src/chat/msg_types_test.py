import pytest

from flexmock import flexmock
from . import msg_types


@pytest.mark.parametrize('init_args,kwargs,result', [
    ({}, {}, {'disable_notification': True, 'tag': None, 'delete_tag': None}),
    ({'disable_notification': False}, {},
     {'disable_notification': False, 'tag': None, 'delete_tag': None}),
    ({'tag': 'abc'}, {},
     {'disable_notification': True, 'tag': 'abc', 'delete_tag': None}),
    ({'delete_tag': 'abc'}, {},
     {'disable_notification': True, 'tag': None, 'delete_tag': 'abc'}),
    ({}, {'force_notification': True},
     {'disable_notification': False, 'tag': None, 'delete_tag': None}),
    ({'disable_notification': True},
     {'force_notification': True},
     {'disable_notification': True, 'tag': None, 'delete_tag': None}),
])
def test_get_send_arg(init_args, kwargs, result):
    assert msg_types.ReplyBase(**init_args).get_send_args(**kwargs) == result


def test_from_cmd():
    client = flexmock()
    module = flexmock(client=client, __name__='mod1')
    (flexmock(client)
     .should_receive('do_cmd')
     .with_args(arg=1)
     .once()
     .and_return('message'))

    reply = msg_types.Reply.from_cmd(module, 'cmd', {'arg': 1})
    assert reply.message == 'message'
    assert reply.send_args == {'disable_web_page_preview': False,
                               'module': 'mod1',
                               'parse_mode': 'Markdown'}


def test_send_empty():
    bot = flexmock()

    reply = msg_types.Reply(None)
    assert not reply.send(bot, 'chat_id')


def test_send():
    bot = flexmock()
    (flexmock(bot)
     .should_receive('sendMessage')
     .with_args('chat_id', 'message',
                parse_mode='Markdown',
                disable_notification=True,
                disable_web_page_preview=False,
                tag=None, delete_tag=None)
     .once())

    reply = msg_types.Reply('message')
    assert reply.send(bot, 'chat_id')


def test_send_notify():
    bot = flexmock()
    (flexmock(bot)
     .should_receive('sendMessage')
     .with_args('chat_id', 'message',
                parse_mode='Markdown',
                disable_notification=False,
                disable_web_page_preview=False,
                tag=None, delete_tag=None)
     .once())

    reply = msg_types.Reply('message')
    assert reply.send(bot, 'chat_id', force_notification=True)


def test_send_error():
    bot = flexmock()

    (flexmock(bot)
     .should_receive('sendMessage')
     .with_args('chat_id', 'message',
                parse_mode='Markdown',
                disable_notification=True,
                disable_web_page_preview=False,
                tag=None, delete_tag=None)
     .and_raise(msg_types.ReadTimeoutError, 'pool',
                url='url', message='Too slow!')
     .times(3))

    reply = msg_types.Reply('message')
    assert not reply.send(bot, 'chat_id')


def test_send_error_recover():
    bot = flexmock()

    (flexmock(bot)
     .should_receive('sendMessage')
     .with_args('chat_id', 'message',
                parse_mode='Markdown',
                disable_notification=True,
                disable_web_page_preview=False,
                tag=None, delete_tag=None)
     .and_raise(msg_types.ReadTimeoutError, 'pool',
                url='url', message='Too slow!')
     .and_raise(msg_types.ReadTimeoutError, 'pool',
                url='url', message='Too slow!')
     .and_return(True))

    reply = msg_types.Reply('message')
    assert reply.send(bot, 'chat_id')


def test_push():
    from . import bot
    (flexmock(bot)
     .should_receive('sendMessage')
     .with_args(0, 'message',
                parse_mode='Markdown',
                disable_notification=False,
                disable_web_page_preview=False,
                tag=None, delete_tag=None)
     .once())

    reply = msg_types.Reply('message')
    assert reply.push()


def test_push_dnd():
    from . import bot
    (flexmock(bot)
     .should_receive('sendMessage')
     .with_args(0, 'message',
                parse_mode='Markdown',
                disable_notification=True,
                disable_web_page_preview=False,
                tag=None, delete_tag=None)
     .once())

    reply = msg_types.Reply('message', disable_notification=True)
    assert reply.push()


def test_push_quiet():
    from . import bot
    (flexmock(bot)
     .should_receive('sendMessage')
     .with_args(0, 'message',
                parse_mode='Markdown',
                disable_notification=True,
                disable_web_page_preview=False,
                tag=None, delete_tag=None)
     .once())

    reply = msg_types.Reply('message')
    assert reply.push(force_notification=False)


def test_cc():
    from . import bot
    (flexmock(bot)
     .should_receive('sendMessage')
     .with_args(1234, 'message',
                parse_mode='Markdown',
                disable_notification=False,
                disable_web_page_preview=False,
                tag=None, delete_tag=None)
     .once())

    reply = msg_types.Reply('message')
    assert reply.cc(1234)


def test_send_to():
    from . import bot
    (flexmock(bot)
     .should_receive('sendMessage')
     .with_args(1234, 'message',
                parse_mode='Markdown',
                disable_notification=True,
                disable_web_page_preview=False,
                tag=None, delete_tag=None)
     .once())

    reply = msg_types.Reply('message')
    assert reply.send_to(1234)


def test_then():
    reply = msg_types.Reply('one', undo='undo')
    then = msg_types.Reply('two', callback='callback')
    result = reply.then(then)

    (flexmock(reply)
     .should_receive('send')
     .with_args('bot', 'chat_id')
     .and_return(True)
     .once())
    (flexmock(then)
     .should_receive('send')
     .with_args('bot', 'chat_id')
     .and_return(True)
     .once())

    assert result.replies == [reply, then]
    assert result.undo == 'undo'
    assert result.send('bot', 'chat_id')
    assert result.callback == 'callback'


def test_reply_list():
    one = msg_types.Reply('one', undo='undo')
    three = msg_types.Reply('three', callback='callback')
    result = msg_types.ReplyList(one, 'two', three,
                                 disable_notification=True)

    assert result.replies[0] == one
    assert result.replies[1].message == 'two'
    assert result.replies[2] == three

    assert result._messages == ['one', 'two', 'three']
    assert result.replies[0].send_args == {'disable_web_page_preview': False,
                                           'parse_mode': 'Markdown'}
    assert result.replies[1].send_args == {'disable_web_page_preview': False,
                                           'disable_notification': True,
                                           'parse_mode': 'Markdown'}
    assert result.replies[2].send_args == {'disable_web_page_preview': False,
                                           'parse_mode': 'Markdown'}


def test_reply_list_none():
    result = msg_types.ReplyList('', '')

    assert result.send('bot', 'chat_id') == False
    assert result.callback == None


def test_reply_list_callback():
    reply = msg_types.Reply('one', callback='callback')
    empty = msg_types.Reply('')
    result = msg_types.ReplyList(reply, empty)

    (flexmock(reply)
     .should_receive('send')
     .with_args('bot', 'chat_id')
     .and_return(True)
     .once())

    (flexmock(empty)
     .should_receive('send')
     .with_args('bot', 'chat_id')
     .and_return(False)
     .once())

    assert result._messages == ['one', '']
    assert result.send('bot', 'chat_id')
    assert result.callback == 'callback'


def test_reply_list_nested():
    one = msg_types.Reply('one', undo='undo')
    two = msg_types.Reply('two', callback='callback')
    one_two = one.then(two)
    result = msg_types.ReplyList(one_two, 'three')

    assert result.replies[0] == one
    assert result.replies[1] == two
    assert result.replies[2].message == 'three'

    assert result._messages == ['one', 'two', 'three']


def test_from_response_reply():
    reply = msg_types.Reply('message')
    assert msg_types.Reply.from_response(reply) == reply


def test_from_response_tuple():
    reply = msg_types.Reply.from_response(('message', 'undo'))
    assert reply.message == 'message'
    assert reply.undo == 'undo'


def test_from_response_str():
    reply = msg_types.Reply.from_response('message')
    assert reply.message == 'message'


def test_replace():
    reply = msg_types.Reply.from_response('old message')
    assert reply.replace('old', 'new') == reply
    assert reply.message == 'new message'


def test_history_key():
    reply = msg_types.Reply('message', history_key='key')
    assert reply.history_key == 'key'


def test_send_photo():
    bot = flexmock()
    (flexmock(bot)
     .should_receive('sendChatAction')
     .with_args('chat_id', 'upload_photo'))

    (flexmock(bot)
     .should_receive('sendPhoto')
     .with_args('chat_id',
                photo='photo/url',
                disable_notification=False,
                tag=None, delete_tag=None)
     .once())

    reply = msg_types.Photo(['photo/url'])
    assert reply.send(bot, 'chat_id')


def test_send_photo_fallback():
    bot = flexmock()
    (flexmock(bot)
     .should_receive('sendChatAction')
     .with_args('chat_id', 'upload_photo'))

    (flexmock(bot)
     .should_receive('sendPhoto')
     .with_args('chat_id',
                photo='photo1',
                disable_notification=False,
                tag=None, delete_tag=None)
     .and_raise(msg_types.TelegramError, 'Oh noes, rain!',
                error_code=None, json=None))
    (flexmock(bot)
     .should_receive('sendPhoto')
     .with_args('chat_id',
                photo='photo2',
                disable_notification=False,
                tag=None, delete_tag=None)
     .once())

    reply = msg_types.Photo(photos=['photo1', 'photo2'])
    assert reply.send(bot, 'chat_id')


def test_send_photos_empty():
    bot = flexmock()
    reply = msg_types.Photo(photos=[])
    assert reply.send(bot, 'chat_id')


def test_send_photo_error():
    bot = flexmock()
    (flexmock(bot)
     .should_receive('sendChatAction')
     .with_args('chat_id', 'upload_photo'))

    (flexmock(bot)
     .should_receive('sendPhoto')
     .with_args('chat_id',
                photo='photo/url',
                disable_notification=False,
                tag=None, delete_tag=None)
     .and_raise(msg_types.TelegramError, 'Oh noes, rain!',
                error_code=None, json=None))

    (flexmock(bot)
     .should_receive('sendMessage')
     .with_args('chat_id', "('Oh noes, rain!', None, None) photo/url")
     .once())

    reply = msg_types.Photo(['photo/url'])
    assert reply.send(bot, 'chat_id')


def test_send_document():
    bot = flexmock()
    (flexmock(bot)
     .should_receive('sendChatAction')
     .with_args('chat_id', 'upload_document'))

    (flexmock(bot)
     .should_receive('sendDocument')
     .with_args('chat_id',
                document=tuple,
                disable_notification=False,
                tag=None, delete_tag=None)
     .once())

    reply = msg_types.Document('filename', b'content')
    assert reply.send(bot, 'chat_id')


def test_send_audio():
    bot = flexmock()
    (flexmock(bot)
     .should_receive('sendChatAction')
     .with_args('chat_id', 'upload_audio'))

    (flexmock(bot)
     .should_receive('sendAudio')
     .with_args('chat_id',
                audio=('filename', 'bytes'),
                caption=None, duration=None,
                performer=None, title=None,
                disable_notification=True,
                tag=None, delete_tag=None)
     .once())

    reply = msg_types.Audio('url')

    (flexmock(reply)
     .should_receive('open_audio')
     .with_args('url')
     .and_return(('filename', 'bytes')))

    assert reply.send(bot, 'chat_id')

def test_send_audio_tagged():
    bot = flexmock()
    (flexmock(bot)
     .should_receive('sendChatAction')
     .with_args('chat_id', 'upload_audio'))

    (flexmock(bot)
     .should_receive('sendAudio')
     .with_args('chat_id',
                audio=('filename', 'bytes'),
                caption='album', duration=1234,
                performer='artist', title='title',
                disable_notification=True,
                tag=None, delete_tag=None)
     .once())

    reply = msg_types.Audio('url', 'artist', 'title', 'album', 1234)

    (flexmock(reply)
     .should_receive('open_audio')
     .with_args('url')
     .and_return(('filename', 'bytes')))

    assert reply.send(bot, 'chat_id')


def test_questions():
    qns = (msg_types.Questions()
           .add('one', 'prompt 1?')
           .add('two', 'prompt 2?'))

    def done(**kwargs):
        return ','.join('{}={}'.format(key, value)
                        for key, value in sorted(kwargs.items()))

    reply1 = qns.done(done)
    assert reply1.message == 'prompt 1?'
    reply2 = reply1.callback('ONE')
    assert reply2.message == 'prompt 2?'
    reply3 = reply2.callback('TWO')
    assert reply3 == 'one=ONE,two=TWO'


def test_questions_defaults_y():
    qns = (msg_types.Questions()
           .add('one', 'prompt 1?', default='One')
           .add('two', 'prompt 2?', default='Two'))

    def done(**kwargs):
        return ','.join('{}={}'.format(key, value)
                        for key, value in sorted(kwargs.items()))

    reply1 = qns.done(done)
    assert reply1.message == 'prompt 1? \\[Y] "One"'
    reply2 = reply1.callback('y')
    assert reply2.message == 'prompt 2? \\[Y] "Two"'
    reply3 = reply2.callback('Y')
    assert reply3 == 'one=One,two=Two'


def test_questions_defaults_n():
    qns = (msg_types.Questions()
           .add('one', 'prompt 1?', default='One', required=False)
           .add('two', 'prompt 2?', default='Two', required=False))

    def done(**kwargs):
        return ','.join('{}={}'.format(key, value)
                        for key, value in sorted(kwargs.items()))

    reply1 = qns.done(done)
    assert reply1.message == 'prompt 1? \\[Y] "One"'
    reply2 = reply1.callback('n')
    assert reply2.message == 'prompt 2? \\[Y] "Two"'
    reply3 = reply2.callback('n')
    assert reply3 == 'one=None,two=None'


def test_questions_abort():
    qns = (msg_types.Questions()
           .add('one', 'prompt 1?', default='One', required=True))

    def done(**kwargs):
        raise "shouldn't be called"  # pragma: no cover

    reply1 = qns.done(done)
    assert reply1.message == 'prompt 1? \\[Y] "One"'
    reply2 = reply1.callback('n')
    assert reply2.message == 'Aborted.'


def test_questions_push():
    qns = (msg_types.Questions()
           .add('one', 'prompt 1?')
           .add('two', 'prompt 2?'))

    qn = flexmock()

    (flexmock(msg_types)
     .should_receive('Question')
     .with_args('prompt 1?', parse_mode='Markdown', callback=qns,
                disable_web_page_preview=True)
     .and_return(qn))

    (flexmock(qn).should_receive('push'))

    assert qns.done('callback', push=True) == True

    assert qns.callback == 'callback'


def test_questions_push_empty():
    qns = msg_types.Questions()
    assert qns.done('callback', push=True) is None
    assert qns.callback == 'callback'


def test_questions_parse_mode():
    qns = (msg_types.Questions()
           .add('one', 'prompt 1?', default='One', parse_mode=None)
           .add('two', 'prompt 2?', default='Two'))

    def done(**kwargs):
        return ','.join('{}={}'.format(key, value)
                        for key, value in sorted(kwargs.items()))

    reply1 = qns.done(done)
    assert reply1.message == 'prompt 1? [Y] "One"'
    assert reply1.send_args['parse_mode'] == None
    reply2 = reply1.callback('y')
    assert reply2.message == 'prompt 2? \\[Y] "Two"'
    assert reply2.send_args['parse_mode'] == 'Markdown'
    reply3 = reply2.callback('Y')
    assert reply3 == 'one=One,two=Two'
