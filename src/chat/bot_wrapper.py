import io
import json
from collections import defaultdict
from pathlib import Path
from telepot.exception import TelegramError
from urllib3.exceptions import ReadTimeoutError, ProtocolError
from PIL import Image


class BotWrapper:

    def __init__(self, bot, ID_ME):
        self.bot = bot
        self.ID_ME = ID_ME

        self._message_ids = []

        # module --> message_id/tag --> message
        self.dashboard = defaultdict(dict)

    def _tagged_send(self, name, *args, tag=None, delete_tag=None, module=None,
                     skip_dashboard=False, **kwargs):
        # tag: tag this message.
        # delete_tag: delete any immediately preceeding messages before sending.

        self._delete_tagged_messages(delete_tag)

        message = getattr(self.bot, name)(*args, **kwargs)

        self._message_ids.append((message['chat']['id'],
                                  message['message_id'],
                                  tag))

        return message

    def _delete_tagged_messages(self, tag):
        if tag is not None:
            while self._message_ids:
                chat_id, message_id, message_tag = self._message_ids.pop()
                if message_tag == tag:
                    try:
                        self.bot.deleteMessage((chat_id, message_id))
                    except:
                        pass
                else:
                    break

            self._message_ids.clear()

    def received_message(self):
        self._message_ids.clear()

    def sendMessage(self, *args, **kwargs):
        return self._add_dashboard(
            self._tagged_send('sendMessage', *args, **kwargs),
            args[0], {'text': args[1]},
            **kwargs)

    def sendPhoto(self, *args, **kwargs):
        photo = kwargs['photo']
        if hasattr(photo, 'seekable'):
            if not photo.seekable():
                # need to read from this multiple times, so copy into a buffer
                kwargs['photo'] = photo = io.BytesIO(photo.read())

        # check dashboard for last image sent for this module/tag and
        # add this image as the last frame in an animated gif

        item = self._get_dashboard(tag_suffix='_image', **kwargs)
        if item:
            image = Path("/data/dashboard") / Path(item['image']).name
            kwargs['video'] = append_video_frame(image, kwargs.pop('photo'))
            # delete the previous image, even if it wasn't the last
            # message - tagged send should take care of this for the
            # most part, but animating is supposed to minimize the
            # number of images in the chat history (especially if
            # they're animated)
            try:
                self.bot.deleteMessage((args[0], item['message_id']))
            except:
                pass
            return self.sendVideo(*args, **kwargs)

        message = self._tagged_send('sendPhoto', *args, **kwargs)

        if isinstance(photo, str):
            content = {'image': photo}
        else:
            filename = Path(f"/data/dashboard/{kwargs['module']}-{message['message_id']}")
            filename.parent.mkdir(parents=True, exist_ok=True)
            with filename.open('wb') as f:
                photo.seek(0)
                f.write(photo.read())
            content = {'image': f'data/{filename.name}'}

        return self._add_dashboard(message, args[0], content,
                                   tag_suffix='_image', **kwargs)

    def sendVideo(self, *args, **kwargs):
        photo = kwargs['video']
        if hasattr(photo, 'seekable'):
            if not photo.seekable():
                # need to read from this multiple times, so copy into a buffer
                kwargs['video'] = photo = io.BytesIO(photo.read())

        message = self._tagged_send('sendVideo', *args, **kwargs)

        if isinstance(photo, str):
            content = {'image': photo}
        else:
            # TODO: md5 hash?
            filename = Path(f"/data/dashboard/{kwargs['module']}-{message['message_id']}")
            filename.parent.mkdir(parents=True, exist_ok=True)
            with filename.open('wb') as f:
                photo.seek(0)
                f.write(photo.read())
            content = {'image': f'data/{filename.name}'}

        return self._add_dashboard(message, args[0], content,
                                   tag_suffix='_image', **kwargs)

    def sendDocument(self, *args, **kwargs):
        return self._tagged_send('sendDocument', *args, **kwargs)

    def sendAudio(self, *args, **kwargs):
        return self._tagged_send('sendAudio', *args, **kwargs)

    def sendChatAction(self, *args, **kwargs):
        try:
            return self.bot.sendChatAction(*args, **kwargs)

        # it doesn't matter if we time out here
        # TODO: really should queue these up and attempt resending later
        except (TelegramError, ReadTimeoutError, ProtocolError) as ex:
            # TODO: log exception
            return None

    def download_file(self, *args, **kwargs):
        return self.bot.download_file(*args, **kwargs)

    def _add_dashboard(self, message, chat_id, content,
                       tag=None, delete_tag=None, module=None,
                       parse_mode='Markdown', tag_suffix='',
                       skip_dashboard=False, **kwargs):
        if chat_id != self.ID_ME:
            return message

        if skip_dashboard:
            return message

        # TODO: filter out errors etc.

        item = {'message_id': message['message_id'],
                'tag': tag,
                'date': message['date'],
                'parse_mode': parse_mode}
        item.update(content)

        # NOTE: hoping the module + tag will keep only the last message for
        # things like radar, heater status etc.
        # NOTE: also relying on the dictionary items remaining in order inserted
        if tag:
            tag2 = tag + tag_suffix
        else:
            tag2 = message['message_id']
        self.dashboard[module][tag2] = item

        return message

    def _get_dashboard(self, module=None, tag=None, tag_suffix='', **kwargs):
        if module in self.dashboard and tag is not None:
            return self.dashboard[module].get(tag + tag_suffix)

    def clear_dashboard(self):
        self.dashboard.clear()

    def export_dashboard(self):
        obj = [{'topic': module,
                'messages': list(messages.values())}
               for module, messages in self.dashboard.items()]
        json.dump(obj, open('/data/dashboard/dashboard.json', 'w'), indent=2)


def append_video_frame(image, photo):
    result = io.BytesIO()
    img = Image.open(image)
    img.save(result,
             format='GIF',
             save_all=True,
             append_images=[Image.open(photo)],
             duration=250,
             loop=0)
    result.seek(0)
    return result
