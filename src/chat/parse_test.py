import pytest
from . import parse_msg


@pytest.mark.parametrize('msg,cmd,kwargs', [
    ('cc 1234 weather', 'cc', {'chat_id': 1234, 'command': 'weather'}),
    ('cc 1234 radar check', 'cc', {'chat_id': 1234, 'command': 'radar check'}),
    ('cc 1234 "radar check"', 'cc', {'chat_id': 1234, 'command': 'radar check'}),
    ('cc 1234 "taylor-st"', 'cc', {'chat_id': 1234, 'command': 'taylor-st'}),
    ('cc 1234 mqtt check topic/one > 10 "Huh?"',
     'cc', {'chat_id': 1234, 'command': 'mqtt check topic/one > 10 "Huh?"'}),
    ('send 1234 weather', 'send', {'chat_id': 1234, 'command': 'weather'}),
    ('send 1234 taylor-st', 'send', {'chat_id': 1234, 'command': 'taylor-st'}),
    ('last weather', 'last', {'num': 1, 'command': 'weather'}),
    ('last 2 weather', 'last', {'num': 2, 'command': 'weather'}),
    ('last count', 'last_count', {}),
    ('mute weather', 'mute', {'command': 'weather'}),
    ('mute radar check', 'mute', {'command': 'radar check'}),
    ('mute "radar check"', 'mute', {'command': 'radar check'}),
    ('echo iot "Some message"',
     'echo', {'command': 'Some message', 'module': 'iot'}),
    ('echo weather https://rjh.org/~rjh/melbourne/history.png',
     'echo', {'command': 'https://rjh.org/~rjh/melbourne/history.png',
              'module': 'weather'}),
    ('echo image mod1 https://example.com/q?abc+def',
     'echo_image', {'command': 'https://example.com/q?abc+def',
                    'module': 'mod1'}),
    ('dedupe rain radar check',
     'dedupe', {'command': 'radar check', 'tag': 'rain', 'clear': ''}),
    ('dedupe clear rain radar check',
     'dedupe', {'command': 'radar check', 'tag': 'rain', 'clear': 'clear'}),
    ('dashboard clear', 'dashboard', {'command': 'clear'}),
    ('dashboard export', 'dashboard', {'command': 'export'}),
])
def test_chat(msg, cmd, kwargs):
    assert list(parse_msg(msg)) == [(cmd, kwargs)]
