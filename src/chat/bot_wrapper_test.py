from unittest.mock import Mock, patch, call

import pytest

from . import bot_wrapper


@pytest.fixture
def bot():
    return bot_wrapper.BotWrapper(Mock(), 12345)


@pytest.fixture
def message():
    return {'chat': {'id': 12345}, 'message_id': 23456}


@pytest.mark.parametrize('kwargs', [
    ({}),
    ({'tag': 'abc'}),
    ({'delete_tag': 'def'}),
])
def test_send_message(bot, kwargs):
    with patch.object(bot, '_tagged_send') as tagged_send, \
         patch.object(bot, '_add_dashboard') as add_dashboard:
        tagged_send.return_value = 'message1'
        add_dashboard.return_value = 'message'
        assert bot.sendMessage(12345, 'arg', **kwargs) == 'message'
        tagged_send.assert_called_with('sendMessage', 12345, 'arg', **kwargs)
        add_dashboard.assert_called_with('message1', 12345, {'text': 'arg'}, **kwargs)


@pytest.mark.skip(reason="rewrite for dashboard")
@pytest.mark.parametrize('kwargs', [
    ({}),
    ({'tag': 'abc'}),
    ({'delete_tag': 'def'}),
])
def test_send_photo(bot, kwargs):
    # TODO: test copying photo to /data/dashboard
    # TODO: test add_dashboard call
    with patch.object(bot, '_tagged_send') as tagged_send:
        tagged_send.return_value = 'message'
        assert bot.sendPhoto(12345, photo='arg', **kwargs) == 'message'
        tagged_send.assert_called_with('sendPhoto', 12345, photo='arg', **kwargs)


@pytest.mark.parametrize('method', [
    'sendDocument', 'sendAudio'
])
@pytest.mark.parametrize('kwargs', [
    ({}),
    ({'tag': 'abc'}),
    ({'delete_tag': 'def'}),
])
def test_send_methods(bot, method, kwargs):
    with patch.object(bot, '_tagged_send') as tagged_send:
        tagged_send.return_value = 'message'
        assert getattr(bot, method)(12345, 'arg', **kwargs) == 'message'
        tagged_send.assert_called_with(method, 12345, 'arg', **kwargs)


def test_send_chat_action_error(bot):
    bot.bot.sendChatAction.side_effect \
        = bot_wrapper.ReadTimeoutError('pool', url='url', message='Too slow!')
    assert bot.sendChatAction('test') is None


@pytest.mark.parametrize('method', [
    'sendChatAction', 'download_file'
])
def test_other_methods(bot, method):
    getattr(bot.bot, method).return_value = 'result'
    assert getattr(bot, method)(12345, 'arg', kwarg='value') == 'result'
    getattr(bot.bot, method).assert_called_with(12345, 'arg', kwarg='value')


@pytest.mark.parametrize('tag,delete_tag', [
    (None, None),
    ('abc', None),
    (None, 'def'),
    ('abc', 'def'),
])
def test_tagged_send(bot, message, tag, delete_tag):
    bot.bot.sendMessage.return_value = message

    bot._message_ids.append('before')

    with patch.object(bot, '_delete_tagged_messages') as delete:

        assert bot._tagged_send('sendMessage', 12345, 'text',
                                parse_mode='Markdown',
                                tag=tag, delete_tag=delete_tag) == message

        delete.assert_called_with(delete_tag)

    bot.bot.sendMessage.assert_called_with(12345, 'text',
                                           parse_mode='Markdown')

    assert bot._message_ids == ['before', (12345, 23456, tag)]


@pytest.mark.parametrize('tag,before,after,delete', [
    # delete_tag = None doesn't change the history
    (None, [], [], []),
    (None, [(123, 11, None)], [(123, 11, None)], []),
    (None, [(123, 11, 'abc')], [(123, 11, 'abc')], []),
    # delete_tag != None always clears the history
    ('abc', [], [], []),
    ('abc', [(123, 11, None)], [], []),
    ('abc', [(123, 11, 'def')], [], []),
    ('abc', [(123, 11, 'abc'), (123, 22, None)], [], []),
    ('abc', [(123, 11, 'abc'), (123, 22, 'def')], [], []),
    # and will delete last calls that match
    ('abc', [(123, 11, 'abc')], [], [call((123, 11))]),
    ('abc', [(123, 11, 'abc'), (123, 22, 'abc')], [],
     # reverse order
     [call((123, 22)), call((123, 11))]),
    ('abc', [(123, 11, None), (123, 22, 'abc')], [],
     [call((123, 22))]),
])
def test_delete_tagged_messages(bot, tag, before, after, delete):
    bot._message_ids.extend(before)

    bot._delete_tagged_messages(tag)

    assert bot._message_ids == after

    assert bot.bot.deleteMessage.mock_calls == delete


def test_received_message(bot):
    bot._message_ids.append('before')

    bot.received_message()

    assert bot._message_ids == []
