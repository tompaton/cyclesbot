import importlib
import tempfile

from collections import defaultdict

from pyparsing import ParseException

from . import bot
from .msg_types import Reply

disabled = []
_modules = []

def load(module_name):
    if module_name in disabled:
        return

    module = importlib.import_module(module_name)

    client = importlib.import_module('.client', module_name)

    enabled = client.init() if hasattr(client, 'init') else True

    if enabled is not False:
        _modules.append(module)

    return module


_answer_callback = [None, None]
history = defaultdict(list)


def set_callback(reply, interactive=True):
    if not interactive and _answer_callback[0] is not None:
        return

    set_callback_ex(reply.callback, reply.get_module())


def set_callback_ex(callback, module_name):
    _answer_callback[0] = callback
    _answer_callback[1] = module_name


def do_callback(text):
    if callable(_answer_callback[0]):
        reply = _answer_callback[0](text)
        if reply:
            reply.module(name=_answer_callback[1])
        return reply


def process_message(chat_id, text, set_undo):

    reply = do_callback(text)
    if reply is not None:
        reply.send(bot, chat_id)
        set_callback(reply)
        return True

    def notify(action):
        bot.sendChatAction(chat_id, 'typing')

    # direct messages shouldn't have $words expanded
    module, replies = run_raw_command(text, notify, get_modules(chat_id))

    for reply in replies:
        set_undo(reply.undo)
        reply.send(bot, chat_id)
        set_callback(reply)
        if reply.history_key != '':
            history[reply.history_key or text].append(reply)

    return module


def get_modules(chat_id):
    from . import ID_ME, ID_ME2

    if chat_id == ID_ME:
        return _modules
    elif chat_id == ID_ME2:
        return [module for module in _modules
                if module.__name__ in ['iot']]
    else:
        return []


def run_command(text, expand_words=None):
    # called from cron and other "meta" commands

    if expand_words is None:
        for module in _modules:
            if module.__name__ == 'words':
                expand_words = module.expand_words

    module = None
    replies = []

    for expanded_text in expand_words(text):
        result = run_raw_command(expanded_text)
        module = module or result[0]
        replies.extend(result[1])

    return module, replies


def run_raw_command(text, notify=None, modules=None):
    issues = []

    if modules is None:
        modules = _modules

    for module in modules:
        try:
            replies = []
            for cmd, kwargs in module.parse_msg(text):
                if callable(notify):
                    notify('typing')
                replies.append(Reply.from_cmd(module, cmd, kwargs))

            if replies:
                return module, replies

        except ParseException as e:
            issues.append(e)

    for pe in issues:
        print(pe)
        print('column: {} "{}"'.format(pe.col, text))

    return None, []

def process_photo(chat_id, photo_id, set_undo):
    for module in _modules:
        if hasattr(module.client, 'can_process_photo'):
            if module.client.can_process_photo():
                with tempfile.NamedTemporaryFile() as tmp:
                    bot.download_file(photo_id, tmp.file)
                    tmp.seek(0)
                    reply = Reply.from_response(
                        module.client.process_photo(tmp))
                    set_undo(reply.undo)
                    reply.send(bot, chat_id)
                    set_callback(reply)

                return module
