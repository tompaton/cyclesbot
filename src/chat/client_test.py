import pytest
from flexmock import flexmock
from . import client
from chat.msg_types import Reply
from chat import modules


def test_cc():
    reply = Reply('test')
    (flexmock(reply)
     .should_receive('send')
     .with_args(object, 1234, force_notification=True)
     .and_return(True))

    (flexmock(modules)
     .should_receive('run_command')
     .with_args('radar check')
     .and_return(1, [reply]))

    assert client.do_cc(1234, 'radar check').replies == [reply]


def test_cc_many():
    reply1 = Reply('test1')
    (flexmock(reply1)
     .should_receive('send')
     .with_args(object, 1234, force_notification=True)
     .and_return(True))

    reply2 = Reply('test2')
    (flexmock(reply2)
     .should_receive('send')
     .with_args(object, 1234, force_notification=True)
     .and_return(True))

    (flexmock(modules)
     .should_receive('run_command')
     .with_args('radar check')
     .and_return(1, [reply1, reply2]))

    assert client.do_cc(1234, 'radar check').replies == [reply1, reply2]


def test_send():
    reply = Reply('test')
    (flexmock(reply)
     .should_receive('send')
     .with_args(object, 1234)
     .and_return(True))

    (flexmock(modules)
     .should_receive('run_command')
     .with_args('radar check')
     .and_return(1, [reply]))

    assert client.do_send(1234, 'radar check') is None


def test_mute():
    reply = Reply('test')

    (flexmock(modules)
     .should_receive('run_command')
     .with_args('radar check')
     .and_return(1, [reply]))

    assert client.do_mute('radar check') is None


def test_last_count_empty():
    modules.history.clear()
    assert client.do_last_count() == 'No reply history'


def test_last_count():
    modules.history.clear()

    modules.history['key1'].append('reply1')
    modules.history['key1'].append('reply2')
    modules.history['key2'].append('reply3')

    assert client.do_last_count() == 'key1: 2\nkey2: 1'


def test_last_none():
    modules.history.clear()
    assert client.do_last('command') == 'No reply history for "command"'


def test_last_one():
    modules.history.clear()
    modules.history['command'].append('reply1')
    modules.history['command'].append('reply2')

    assert client.do_last('command', 1)._messages == ['reply2']


def test_last_n():
    modules.history.clear()
    modules.history['command'].append('reply1')
    modules.history['command'].append('reply2')
    modules.history['command'].append('reply3')
    modules.history['command'].append('reply4')

    assert client.do_last('command', 3)._messages \
        == ['reply2', 'reply3', 'reply4']


def test_echo():
    reply = client.do_echo('message', 'mod1')
    assert reply.message == 'message'
    assert reply.get_module() == 'mod1'


def test_echo_url():
    reply = client.do_echo('https://tompaton.com', 'mod1')
    assert reply.message == 'https://tompaton.com'
    assert reply.get_module() == 'mod1'


def test_echo_image():
    url = 'https://tompaton.com/test.jpg'
    req = flexmock()
    photo = flexmock()

    (flexmock(client)
     .should_receive('Request')
     .with_args(url)
     .and_return(req))

    (flexmock(req)
     .should_receive('add_header')
     .with_args('User-Agent', 'tompaton.com/1.0')
     .once())

    (flexmock(client)
     .should_receive('urlopen')
     .with_args(req)
     .and_return(photo))

    reply = client.do_echo(url, 'mod1')
    assert reply.photos == [photo]
    assert reply.get_module() == 'mod1'


def test_echo_image_error():
    url = 'https://tompaton.com/test.jpg'
    req = flexmock()

    (flexmock(client)
     .should_receive('Request')
     .with_args(url)
     .and_return(req))

    (flexmock(req)
     .should_receive('add_header')
     .with_args('User-Agent', 'tompaton.com/1.0')
     .once())

    (flexmock(client)
     .should_receive('urlopen')
     .with_args(req)
     .and_raise(client.URLError, "Nope."))

    reply = client.do_echo(url, 'mod1')
    assert reply.message \
        == '<urlopen error Nope.> https://tompaton.com/test.jpg'
    assert reply.get_module() == 'mod1'


def test_echo_image2():
    url = 'https://tompaton.com/test?q=abc'
    req = flexmock()
    photo = flexmock()

    (flexmock(client)
     .should_receive('Request')
     .with_args(url)
     .and_return(req))

    (flexmock(req)
     .should_receive('add_header')
     .with_args('User-Agent', 'tompaton.com/1.0')
     .once())

    (flexmock(client)
     .should_receive('urlopen')
     .with_args(req)
     .and_return(photo))

    reply = client.do_echo_image(url, 'mod1')
    assert reply.photos == [photo]
    assert reply.get_module() == 'mod1'


def test_echo_image_ftp():
    url = 'ftp://tompaton.com/test.jpg'
    req = flexmock()
    photo = flexmock()

    (flexmock(client)
     .should_receive('Request')
     .with_args(url)
     .and_return(req))

    (flexmock(client)
     .should_receive('urlopen')
     .with_args(req)
     .and_return(photo))

    reply = client.do_echo(url, 'mod1')
    assert reply.photos == [photo]
    assert reply.get_module() == 'mod1'


def test_dedupe():
    reply1 = Reply('test1')
    reply2 = Reply('test2')

    (flexmock(modules)
     .should_receive('run_command')
     .with_args('radar check')
     .and_return(flexmock(__name__='mod1'), [reply1, reply2]))

    replies = client.do_dedupe('clear', 'radar', 'radar check').replies

    assert len(replies) == 2
    assert replies[0].message == 'test1'
    assert replies[1].message == 'test2'
    assert replies[0].send_args == {'tag': 'radar',
                                    'delete_tag': 'radar',
                                    'parse_mode': 'Markdown',
                                    'disable_web_page_preview': False,
                                    'module': 'mod1'}
    assert replies[1].send_args == {'tag': 'radar',
                                    'parse_mode': 'Markdown',
                                    'disable_web_page_preview': False,
                                    'module': 'mod1'}
