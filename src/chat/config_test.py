from flexmock import flexmock
from . import config, modules
from config import var_types
from chat import msg_types


def test_initialize():
    mod1 = flexmock(__name__='abc')
    mod2 = flexmock(__name__='def',
                    config=flexmock(key1=var_types.ConfigParam(name='defined'),
                                    key2=var_types.ConfigParam(name='manual',
                                                               default='zzz'),
                                    key3=var_types.ConfigParam(name='input')))
    flexmock(modules,
             _modules=[mod1, mod2],
             _answer_callback=['before', 'mod_before'])

    (flexmock(var_types.client)
     .should_receive('get_value')
     .with_args('def', 'key1')
     .and_return([('def', 'key1', 'defined')])
     .once())

    (flexmock(var_types.client)
     .should_receive('get_value')
     .with_args('def', 'key2')
     .and_return([])
     .twice())  # once by initialize, once by assertEqual

    (flexmock(var_types.client)
     .should_receive('get_value')
     .with_args('def', 'key3')
     .and_return([])
     .once())

    qns = flexmock()

    (flexmock(msg_types)
     .should_receive('Questions')
     .and_return(qns))

    (flexmock(qns)
     .should_receive('add')
     .with_args('def__key3', 'input?'))

    (flexmock(qns)
     .should_receive('done')
     .with_args(config._save, push=True)
     .and_return(True))

    config.initialize()

    assert modules._answer_callback == [qns, 'config']

    assert mod2.config.key2.module == 'def'
    assert mod2.config.key2.key == 'key2'
    assert mod2.config.key2.value == 'zzz'

    assert mod2.config.key3.module == 'def'
    assert mod2.config.key3.key == 'key3'


def test_initialize_none():
    flexmock(modules, _modules=[])

    qns = flexmock()

    (flexmock(msg_types)
     .should_receive('Questions')
     .and_return(qns))

    (flexmock(qns)
     .should_receive('done')
     .with_args(config._save, push=True)
     .and_return(False))

    config.initialize()


def test_initialize_input_value():
    var = flexmock(var_types.InputVar('input'))

    (flexmock(var)
     .should_receive('input_value')
     .once())

    mod = flexmock(__name__='def', config=flexmock(key1=var))
    flexmock(modules, _modules=[mod])

    qns = flexmock()

    (flexmock(msg_types)
     .should_receive('Questions')
     .and_return(qns))

    (flexmock(qns)
     .should_receive('done')
     .with_args(config._save, push=True)
     .and_return(False))

    config.initialize()


def test_save_none():
    assert config._save().message == 'Config saved.'


def test_save_key():
    (flexmock(config.client)
     .should_receive('do_set')
     .with_args('module', 'key', 'value')
     .once())
    assert config._save(module__key='value').message == 'Config saved.'
