import pytest
import tempfile
from flexmock import flexmock
from unittest.mock import Mock
from pyparsing import ParseException
from . import msg_types


@pytest.fixture
def modules():
    from . import modules
    modules.disabled.clear()
    modules._modules.clear()
    modules._answer_callback = [None, None]
    return modules


def test_load(modules):
    (flexmock(modules.importlib)
     .should_receive('import_module')
     .with_args('mod1')
     .and_return('MOD1'))

    (flexmock(modules.importlib)
     .should_receive('import_module')
     .with_args('.client', 'mod1')
     .and_return('MOD1.client'))

    assert modules.load('mod1') == 'MOD1'
    assert modules._modules == ['MOD1']


def test_load_init(modules):
    (flexmock(modules.importlib)
     .should_receive('import_module')
     .with_args('mod1')
     .and_return('MOD1'))

    client = flexmock()
    flexmock(client).should_receive('init').once()

    (flexmock(modules.importlib)
     .should_receive('import_module')
     .with_args('.client', 'mod1')
     .and_return(client))

    assert modules.load('mod1') == 'MOD1'
    assert modules._modules == ['MOD1']


def test_load_init_disabled(modules):
    (flexmock(modules.importlib)
     .should_receive('import_module')
     .with_args('mod1')
     .and_return('MOD1'))

    client = flexmock()
    flexmock(client).should_receive('init').and_return(False)

    (flexmock(modules.importlib)
     .should_receive('import_module')
     .with_args('.client', 'mod1')
     .and_return(client))

    assert modules.load('mod1') == 'MOD1'
    assert modules._modules == []


def test_load_disabled(modules):
    modules.disabled.append('mod1')
    assert modules.load('mod1') is None
    assert modules._modules == []


def test_run_raw_command_no_modules(modules):
    assert modules.run_raw_command('text') == (None, [])


def test_run_raw_command_unparsed(modules):
    mod1 = flexmock()
    (flexmock(mod1)
     .should_receive('parse_msg')
     .with_args('text')
     .and_return([]))

    modules._modules.append(mod1)

    assert modules.run_raw_command('text') == (None, [])


def test_run_raw_command_error(modules):
    mod1 = flexmock()
    (flexmock(mod1)
     .should_receive('parse_msg')
     .with_args('text')
     .and_raise(ParseException, 'error'))

    modules._modules.append(mod1)

    assert modules.run_raw_command('text') == (None, [])


def test_run_raw_command(modules):
    mod1 = flexmock(client=flexmock(), __name__='mod1')

    (flexmock(mod1)
     .should_receive('parse_msg')
     .with_args('text')
     .and_return([('cmd', {'arg': 1})]))

    (flexmock(mod1.client)
     .should_receive('do_cmd')
     .with_args(arg=1)
     .and_return('reply'))

    modules._modules.append(mod1)

    module, replies = modules.run_raw_command('text')

    assert module == mod1
    assert replies[0].message == 'reply'
    assert replies[0].send_args == {'disable_web_page_preview': False,
                                    'module': 'mod1',
                                    'parse_mode': 'Markdown'}


def test_run_raw_command_notify(modules):
    mod1 = flexmock(client=flexmock(), __name__='mod1')

    (flexmock(mod1)
     .should_receive('parse_msg')
     .with_args('text')
     .and_return([('cmd', {'arg': 1})]))

    (flexmock(mod1.client)
     .should_receive('do_cmd')
     .with_args(arg=1)
     .and_return('reply'))

    modules._modules.append(mod1)

    def notify(action):
        assert action == 'typing'

    module, replies = modules.run_raw_command('text', notify)

    assert module == mod1
    assert replies[0].message == 'reply'


def test_process_message(modules):
    mod1 = Mock(__name__='mod1')

    modules.history.clear()
    modules._modules.append(mod1)

    from . import ID_ME

    undo = []

    def set_undo(undo_callback):
        undo.append(undo_callback)

    reply = flexmock(undo='undo', callback='callback', history_key=None,
                     get_module=lambda: 'mod1')

    flexmock(reply).should_receive('send').with_args(modules.bot, ID_ME)

    (flexmock(modules)
     .should_receive('run_raw_command')
     .with_args('text', object, [mod1])
     .and_return((mod1, [reply])))

    assert modules.process_message(ID_ME, 'text', set_undo) == mod1

    assert modules._answer_callback == ['callback', 'mod1']
    assert undo == ['undo']
    assert modules.history == {'text': [reply]}


def test_process_message_history_key(modules):
    mod1 = Mock(__name__='mod1')
    modules.history.clear()
    modules._modules.append(mod1)

    from . import ID_ME

    undo = []

    def set_undo(undo_callback):
        undo.append(undo_callback)

    reply = flexmock(undo='undo', callback='callback', history_key='key',
                     get_module=lambda: 'mod1')

    flexmock(reply).should_receive('send').with_args(modules.bot, ID_ME)

    (flexmock(modules)
     .should_receive('run_raw_command')
     .with_args('text', object, [mod1])
     .and_return((mod1, [reply])))

    assert modules.process_message(ID_ME, 'text', set_undo) == mod1

    assert modules._answer_callback == ['callback', 'mod1']
    assert undo == ['undo']
    assert modules.history == {'key': [reply]}


def test_process_message_no_history_key(modules):
    mod1 = Mock(__name__='mod1')
    modules.history.clear()
    modules._modules.append(mod1)

    from . import ID_ME

    undo = []

    def set_undo(undo_callback):
        undo.append(undo_callback)

    reply = flexmock(undo='undo', callback='callback', history_key='',
                     get_module=lambda: 'mod1')

    flexmock(reply).should_receive('send').with_args(modules.bot, ID_ME)

    (flexmock(modules)
     .should_receive('run_raw_command')
     .with_args('text', object, [mod1])
     .and_return((mod1, [reply])))

    assert modules.process_message(ID_ME, 'text', set_undo) == mod1

    assert modules._answer_callback == ['callback', 'mod1']
    assert undo == ['undo']
    assert modules.history == {}


def test_process_message_callback(modules):
    reply = flexmock(callback='callback', get_module=lambda: 'mod1')

    def callback(text):
        assert text == 'text'
        return reply

    modules._answer_callback = [callback, 'mod1']

    (flexmock(reply)
     .should_receive('send')
     .with_args(modules.bot, 'chat_id')
     .and_return(True))

    (flexmock(reply)
     .should_receive('module')
     .with_args(name='mod1')
     .and_return(reply))

    assert modules.process_message('chat_id', 'text', None) == True

    assert modules._answer_callback == ['callback', 'mod1']


def test_process_message_callback_unhandled(modules):
    mod1 = Mock(__name__='mod1')
    modules._modules.append(mod1)

    from . import ID_ME

    def callback(text):
        assert text == 'text'
        return None

    modules._answer_callback = [callback, 'mod2']

    (flexmock(modules)
     .should_receive('run_raw_command')
     .with_args('text', object, [mod1])
     .and_return((None, [])))

    assert modules.process_message(ID_ME, 'text', None) is None

    # callback still hooked up
    assert modules._answer_callback == [callback, 'mod2']


def test_process_message_me2(modules):
    mod1 = flexmock(__name__='mod1')
    mod2 = flexmock(__name__='iot')

    modules.history.clear()
    modules._modules.append(mod1)
    modules._modules.append(mod2)

    import chat
    _ID_ME2 = chat.ID_ME2
    chat.ID_ME2 = 1235

    undo = []

    def set_undo(undo_callback):
        undo.append(undo_callback)

    reply = flexmock(undo='undo', callback='callback', history_key=None,
                     get_module=lambda: 'mod1')

    flexmock(reply).should_receive('send').with_args(modules.bot, chat.ID_ME2)

    (flexmock(modules)
     .should_receive('run_raw_command')
     .with_args('text', object, [mod2])
     .and_return((mod2, [reply])))

    assert modules.process_message(chat.ID_ME2, 'text', set_undo) == mod2

    assert modules._answer_callback == ['callback', 'mod1']
    assert undo == ['undo']
    assert modules.history == {'text': [reply]}

    chat.ID_ME2 = _ID_ME2


def test_process_message_other(modules):
    mod1 = flexmock(__name__='mod1')
    mod2 = flexmock(__name__='iot')

    modules.history.clear()
    modules._modules.append(mod1)
    modules._modules.append(mod2)

    undo = []

    def set_undo(undo_callback):
        undo.append(undo_callback)

    (flexmock(modules)
     .should_receive('run_raw_command')
     .with_args('text', object, [])
     .and_return((None, [])))

    assert modules.process_message(999, 'text', set_undo) is None

    assert modules._answer_callback == [None, None]
    assert undo == []
    assert modules.history == {}


def test_process_photos_unhandled(modules):
    modules._answer_callback = 'callback'

    assert modules.process_photo('chat_id', 'photo_id', None) is None

    # callback still hooked up
    assert modules._answer_callback == 'callback'


def test_process_photos_not_ready(modules):
    mod1 = flexmock(client=flexmock())
    (flexmock(mod1.client)
     .should_receive('can_process_photo')
     .and_return(False))

    modules._modules.append(mod1)

    assert modules.process_photo('chat_id', 'photo_id', None) is None


def test_process_photos(modules):
    undo = []

    def set_undo(undo_callback):
        undo.append(undo_callback)

    reply = flexmock(msg_types.Reply('message', undo='undo',
                                     callback='callback'))
    mod1 = flexmock(client=flexmock())
    tmp = flexmock(name='/tmp/1234', file='file')

    (flexmock(mod1.client)
     .should_receive('can_process_photo')
     .and_return(True))

    (flexmock(tempfile)
     .should_receive('NamedTemporaryFile')
     .and_return(tmp))

    (flexmock(modules.bot)
     .should_receive('download_file')
     .with_args('photo_id', 'file'))

    (flexmock(tmp)
     .should_receive('seek')
     .with_args(0)
     .once())

    (flexmock(mod1.client)
     .should_receive('process_photo')
     .with_args(tmp)
     .and_return(reply))

    (flexmock(reply)
     .should_receive('send')
     .with_args(modules.bot, 'chat_id'))

    modules._modules.append(mod1)

    assert modules.process_photo('chat_id', 'photo_id', set_undo) == mod1

    assert modules._answer_callback == ['callback', None]
    assert undo == ['undo']


def test_get_modules(modules):
    mod1 = flexmock()
    mod2 = flexmock(__name__='iot')

    modules._modules.append(mod1)
    modules._modules.append(mod2)

    from . import ID_ME

    assert modules.get_modules(ID_ME) == [mod1, mod2]
