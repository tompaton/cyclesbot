from collections import defaultdict

import telepot

from . import bot, ID_US
from chat.msg_types import Reply

tally = defaultdict(int)

def cant_talk(msg):
    content_type, chat_type, chat_id = telepot.glance(msg)

    print('From:', msg['from'])

    tally[msg['from']['id']] += 1

    if msg['from']['id'] not in ID_US:
        if tally[msg['from']['id']] <= 2:
            response = "Hi {}, sorry, can't talk now."
        elif tally[msg['from']['id']] <= 5:
            response = "No, really."
        else:
            response = None

        if response:
            bot.sendMessage(chat_id, response.format(msg['from']['first_name']))

        Reply("Got {} message from {}:\n{}"
              .format(content_type, repr(msg['from']),
                      msg['text'] if content_type == 'text'
                      else 'unknown'),
              parse_mode=None).push()

        return True
