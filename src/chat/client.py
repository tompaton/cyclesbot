from urllib.error import URLError
from urllib.request import urlopen, Request
from chat import modules
from chat.msg_types import ReplyList, Photo, Reply


def do_cc(chat_id, command):
    replies = ReplyList(*modules.run_command(command)[1])
    replies.cc(chat_id)
    return replies


def do_send(chat_id, command):
    replies = ReplyList(*modules.run_command(command)[1])
    replies.send_to(chat_id)
    return None


def do_mute(command):
    modules.run_command(command)
    return None


def do_last(command, num=1):
    if command in modules.history and modules.history[command]:
        return ReplyList(*modules.history[command][-num:])

    else:
        return 'No reply history for "{}"'.format(command)


def do_last_count():
    return ('\n'.join('{}: {}'.format(key, len(modules.history[key]))
                      for key in sorted(modules.history.keys()))
            or 'No reply history')


def do_echo_image(command, module):
    return do_echo(command, module, reply_type='image')


def do_echo(command, module, reply_type=None):
    if reply_type is None:
        if command.split('.')[-1].lower() in ('gif', 'jpg', 'jpeg', 'png'):
            reply_type = 'image'

    if command.lower().startswith('http') or command.lower().startswith('ftp'):
        if reply_type == 'image':
            req = Request(command)
            if command.lower().startswith('http'):
                req.add_header('User-Agent', 'tompaton.com/1.0')
            try:
                return Photo([urlopen(req)]).module(name=module)
            except URLError as ex:
                error = '{} {}'.format(ex, command)
                return Reply(error).module(name=module)

    return Reply(command).module(name=module)


def do_dedupe(clear, tag, command):
    module, replies = modules.run_command(command)
    reply = ReplyList(*replies).module(module)
    return reply.tag(tag=tag, delete_tag=tag if clear else None)


def do_dashboard(command):
    from . import bot
    if command == 'clear':
        bot.clear_dashboard()
        return 'Dashboard cleared.'
    elif command == 'export':
        bot.export_dashboard()
        # TODO: return filename/url?
        return 'Dashboard exported.'
