# coding: utf-8

import sqlite3

from collections import namedtuple

from common.parse import normalize_emoji


def get_conn():  # pragma: no cover
    return sqlite3.connect('/data/emoji.db')

def init_db():  # pragma: no cover
    get_conn().execute('''CREATE TABLE IF NOT EXISTS
                          macros (macro text, result text,
                                  created integer, modified integer)''')
    for macro, result in [('✅', '/tick'),
                          ('💤', '/next'),
                          ('🏃', '/tick run'),
                          ('🚿', '/tick cold shower'),
                          ('💪', '/tick exercises'),

                          ('🥪', '/lunch home m med -'),
                          ('🍰', '/lunch Muffin/Cake v sml -'),
                          ('🥖', '/lunch focaccia v med $')]:
        print(do_undef(macro))
        print(do_define(macro, result))

def migrate_db_1():  # pragma: no cover
    get_conn().execute('ALTER TABLE macros ADD COLUMN "created" integer')
    get_conn().execute('ALTER TABLE macros ADD COLUMN "deleted" integer')
    with get_conn() as conn:
        conn.execute("UPDATE macros SET created=datetime('now')")

MacroRow = namedtuple('MacroRow', 'macro result created deleted rowid')

def fetch_macros(**kwargs):
    # TODO: remove in python 3.6 with sorted dicts
    kwargs = sorted(kwargs.items())
    where = ['deleted IS NULL'] + [key + ' = ?' for key, value in kwargs]
    params = tuple(value for key, value in kwargs)

    cur = get_conn().cursor()
    cur.execute('SELECT macro, result, created, deleted, rowid '
                'FROM macros WHERE {}'.format(' AND '.join(where)),
                params)
    return [MacroRow(*row) for row in cur.fetchall()]

def expand_macros(msg):
    # don't expand emoji in define commands
    if msg.strip().startswith('#'):
        return msg

    msg = normalize_emoji(msg)

    for row in fetch_macros():
        msg = msg.replace(row.macro, ' {} '.format(row.result.strip()))
    return msg

def do_defines():
    return '\n'.join('{} {}'.format(row.macro, row.result)
                     for row in fetch_macros())

def do_define(macro, result):
    with get_conn() as conn:
        cur = conn.cursor()
        cur.execute('INSERT INTO macros (macro, result, created) '
                    "VALUES (?, ?, datetime('now'))",
                    (macro, result))
        rowid = cur.lastrowid

    def _undo():
        with get_conn() as conn:
            conn.execute("UPDATE macros SET deleted=datetime('now') "
                         "WHERE rowid = ?", (rowid,))
        return 'Deleted {} {}'.format(macro, result)

    return 'Added {} {}'.format(macro, result), _undo

def do_undef(macro):
    macros = fetch_macros(macro=macro)
    if macros:
        with get_conn() as conn:
            for row in macros:
                conn.execute("UPDATE macros SET deleted=datetime('now') "
                             "WHERE rowid = ?", (row.rowid,))

        def _undo():
            for row in macros:
                conn.execute("UPDATE macros SET deleted=NULL "
                             "WHERE rowid = ?", (row.rowid,))
            return '\n'.join('Restored {} {}'.format(row.macro, row.result)
                             for row in macros)

        return ('Deleted {} {}'.format(macro, ', '.join('{}'.format(row.result)
                                                        for row in macros)),
                _undo)

    else:
        return "Couldn't find macro {}.".format(macro)

if __name__ == '__main__':  # pragma: no cover
    init_db()
    print(do_defines())
