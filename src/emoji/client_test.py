# coding: utf-8
import pytest
from flexmock import flexmock
from common.test import mock_contextmanager

from . import client


def test_fetch_macros():
    cur = flexmock()

    (flexmock(cur)
     .should_receive('execute')
     .with_args('SELECT macro, result, created, deleted, rowid '
                'FROM macros WHERE deleted IS NULL AND macro = ?',
                ('✅',))
     .once())

    (flexmock(cur)
     .should_receive('fetchall')
     .and_return([('✅', '/tick', 1234567, None, 1),
                  ('✅', '/tock', 1234567, None, 2)]))

    conn = flexmock()

    (flexmock(conn)
     .should_receive('cursor')
     .and_return(cur))

    (flexmock(client)
     .should_receive('get_conn')
     .and_return(conn))

    assert (client.fetch_macros(macro='✅')
            == [client.MacroRow('✅', '/tick', 1234567, None, 1),
                client.MacroRow('✅', '/tock', 1234567, None, 2)])


def test_defines():
    (flexmock(client)
     .should_receive('fetch_macros')
     .and_return([client.MacroRow('✅', '/tick', 1234567, None, 1),
                  client.MacroRow('💤', '/next', 1234567, None, 2)]))
    assert client.do_defines() == '✅ /tick\n💤 /next'


def test_expand_macros_macro():
    assert client.expand_macros('#defines') == '#defines'


def test_expand_macros():
    (flexmock(client)
     .should_receive('fetch_macros')
     .and_return([client.MacroRow('✅', '/tick', 1234567, None, 1),
                  client.MacroRow('💤', '/next', 1234567, None, 2)]))
    assert client.expand_macros('✅') == ' /tick '


def test_expand_macros_multiple():
    (flexmock(client)
     .should_receive('fetch_macros')
     .and_return([client.MacroRow('✅', '/tick', 1234567, None, 1),
                  client.MacroRow('💤', '/next', 1234567, None, 2)]))
    assert client.expand_macros('💤 blah ✅') == ' /next  blah  /tick '


def test_expand_macros_skin_tones():
    (flexmock(client)
     .should_receive('fetch_macros')
     .and_return([client.MacroRow('🏃', '/tick', 1234567, None, 1)]))
    assert client.expand_macros('🏃🏻') == ' /tick '


def test_define():
    cur = flexmock(lastrowid=3)
    (flexmock(cur)
     .should_receive('execute')
     .with_args("INSERT INTO macros (macro, result, created) "
                "VALUES (?, ?, datetime('now'))",
                ('✅', '/tick'))
     .once())

    conn = flexmock()
    flexmock(conn).should_receive('cursor').and_return(cur)

    (flexmock(client)
     .should_receive('get_conn')
     .replace_with(mock_contextmanager(conn)))

    reply = client.do_define('✅', '/tick')
    assert reply[0] == 'Added ✅ /tick'

    # undo
    (flexmock(conn)
     .should_receive('execute')
     .with_args("UPDATE macros SET deleted=datetime('now') "
                "WHERE rowid = ?", (3,))
     .once())

    assert reply[1]() == 'Deleted ✅ /tick'


def test_undef_missing():
    (flexmock(client)
     .should_receive('fetch_macros')
     .with_args(macro='✅')
     .and_return([])
     .once())

    assert client.do_undef('✅') == "Couldn't find macro ✅."


def test_undef():
    (flexmock(client)
     .should_receive('fetch_macros')
     .with_args(macro='✅')
     .and_return([client.MacroRow('✅', '/tick', 1234567, None, 1),
                  client.MacroRow('✅', '/tock', 1234567, None, 2)])
     .once())

    conn = flexmock()
    (flexmock(conn)
     .should_receive('execute')
     .with_args("UPDATE macros SET deleted=datetime('now') "
                "WHERE rowid = ?", (1,))
     .once())
    (flexmock(conn)
     .should_receive('execute')
     .with_args("UPDATE macros SET deleted=datetime('now') "
                "WHERE rowid = ?", (2,))
     .once())

    (flexmock(client)
     .should_receive('get_conn')
     .replace_with(mock_contextmanager(conn)))

    reply = client.do_undef('✅')
    assert reply[0] == "Deleted ✅ /tick, /tock"

    # undo
    (flexmock(conn)
     .should_receive('execute')
     .with_args("UPDATE macros SET deleted=NULL WHERE rowid = ?", (1,))
     .once())
    (flexmock(conn)
     .should_receive('execute')
     .with_args("UPDATE macros SET deleted=NULL WHERE rowid = ?", (2,))
     .once())

    assert reply[1]() == 'Restored ✅ /tick\nRestored ✅ /tock'
