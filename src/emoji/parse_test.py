# coding: utf-8

import pytest
from . import parse_msg


@pytest.mark.parametrize('msg,cmd,kwargs', [
    ('#defines', 'defines', {}),
    ('#undef ✅', 'undef', {'macro': '✅'}),
    ('#define ✅ /tick', 'define', {'macro': '✅', 'result': '/tick'}),
    ('#define ✅ /tick run', 'define', {'macro': '✅', 'result': '/tick run'}),

    # ignore skin tones & male/female modifiers
    ('#define 🏃🏻 /tick run', 'define', {'macro': '🏃', 'result': '/tick run'}),
    ('#define 🏃\u200D\U0001F3FE /tick run', 'define',
     {'macro': '🏃', 'result': '/tick run'}),
    ('#define 🏃♂ /tick run', 'define', {'macro': '🏃', 'result': '/tick run'}),
])
def test_emoji(msg, cmd, kwargs):
    assert list(parse_msg(msg)) == [(cmd, kwargs)]
