import sys
from pyparsing import Group, OneOrMore, oneOf, Suppress, Regex
from common.parse import normalize_emoji

uWord = Regex(r'\S+')

Macro = uWord('macro').setName('macro')
Result = Group(OneOrMore(uWord))('result').setName('command')

ListKeyword = oneOf('defines', caseless=True)('command')
ListCommand = ListKeyword

UndefKeyword = oneOf('undef', caseless=True)('command')
UndefCommand = UndefKeyword + Macro

DefineKeyword = oneOf('define', caseless=True)('command')
DefineCommand = DefineKeyword + Macro + Result

Message = Group(Suppress('#') + (ListCommand | UndefCommand | DefineCommand))

def parse_msg(msg):
    for res in Message.parseString(msg, parseAll=True):
        kwargs = {}
        if 'macro' in res:
            kwargs['macro'] = normalize_emoji(res['macro'])
        if 'result' in res:
            kwargs['result'] = ' '.join(res['result'])
        yield res['command'], kwargs

HELP = """
*#defines*
*#define* _macro_ _command_
*#undef* _macro_
"""
