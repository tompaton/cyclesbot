#!/usr/bin/env python
# coding: utf-8

import argparse
import sys

import telepot
from telepot.loop import MessageLoop

from better_exceptions import format_exception

from chat import bot

import common.git
import chat.config
import chat.stranger
import chat.modules

parser = argparse.ArgumentParser()
parser.add_argument('--disable', action='append', metavar='module',
                    default=[], help='disable input vars for module')
args = parser.parse_args()
chat.modules.disabled = args.disable
common.git.enabled = 'git' not in args.disable

emoji = chat.modules.load('emoji')
chat.modules.load('cycles')
chat.modules.load('lunch')
chat.modules.load('rain')
chat.modules.load('weightlog')
chat.modules.load('blog')
chat.modules.load('weather')
bank = chat.modules.load('bank')
cron = chat.modules.load('cron')
undo = chat.modules.load('undo')
trace = chat.modules.load('trace')
chat.modules.load('botcode')
chat.modules.load('help')
chat.modules.load('config')
chat.modules.load('opentabs')
chat.modules.load('yearlyreps')
chat.modules.load('podcasts')
chat.modules.load('graph')
chat.modules.load('chat')
chat.modules.load('image')
iot = chat.modules.load('iot')
chat.modules.load('rss')
chat.modules.load('words')


def on_chat_message(msg):
    content_type, chat_type, chat_id = telepot.glance(msg)
    print('Chat:', content_type, chat_type, chat_id)

    if chat.stranger.cant_talk(msg):
        trace.last_message('Last message wasn\'t from me!\n')
        return

    bot.received_message()

    if content_type == 'text':
        _process_text_message(chat_id, msg['text'])

    elif content_type == 'photo':
        _process_photo_message(chat_id, msg['photo'][-1]['file_id'])

    else:
        trace.last_message('Last message wasn\'t text: {}'
                           .format(content_type))


def _process_text_message(chat_id, msg_text):
    try:
        if emoji:
            text = emoji.expand_macros(msg_text)
        else:
            text = msg_text

        parsed_by = chat.modules.process_message(chat_id, text, undo.set_undo)

        if parsed_by == trace:
            pass

        elif parsed_by:
            trace.last_message('Last message was Ok: {}'.format(text))

        else:
            trace.last_message('Last message not parsed: {}'.format(text),
                               log=True)

    except Exception as e:
        trace.last_message('Last message was {}: {}\n{}'
                           .format(type(e).__name__, text, e))
        print(format_exception(*sys.exc_info()))


def _process_photo_message(chat_id, photo_id):
    try:
        parsed_by = chat.modules.process_photo(chat_id, photo_id,
                                               undo.set_undo)

        if parsed_by:
            trace.last_message('Last message was Ok: {}'.format(photo_id))

        else:
            trace.last_message('Last message not parsed: {}'.format(photo_id),
                               log=True)

    except Exception as e:
        trace.last_message('Last message was {}: {}\n{}'
                           .format(type(e).__name__, photo_id, e))
        print(format_exception(*sys.exc_info()))


MessageLoop(bot.bot, {'chat': on_chat_message}).run_as_thread()

print('Checking config ...')

chat.config.initialize()

print('Listening ...')

iot.initialize()

cron.client.run_jobs()
