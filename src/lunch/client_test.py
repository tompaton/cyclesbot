from flexmock import flexmock
from . import client
from common import bash


def test_add():
    (flexmock(bash)
     .should_receive('run')
     .with_args('lunch/lunch_functions.sh', 'lunch', 'what', 'group', 'size',
                'cost')
     .and_return('added'))

    reply = client.do_add('what', 'group', 'size', 'cost', 'when', 'today')
    assert reply[0] == 'added'

    (flexmock(bash)
     .should_receive('run')
     .with_args('lunch/lunch_functions.sh', 'lunch_undo')
     .and_return('undo'))

    assert reply[1]() == 'undo'


def test_add_yesterday():
    reply = client.do_add('what', 'group', 'size', 'cost', 'when', 'yesterday')
    assert reply == 'Sorry, currently can only add for today.'


def test_recent():
    (flexmock(bash)
     .should_receive('run')
     .with_args('lunch/lunch_functions.sh', 'lunch_recent')
     .and_return('recent'))
    assert client.do_recent() == 'recent'


def test_stats():
    (flexmock(bash)
     .should_receive('run')
     .with_args('lunch/lunch_functions.sh', 'lunch_stats')
     .and_return('stats'))
    assert client.do_stats() == 'stats'


def test_options():
    (flexmock(bash)
     .should_receive('run')
     .with_args('lunch/lunch_functions.sh', 'lunch')
     .and_return('lunch "option" V sml \\$'))
    assert client.do_options() == 'lunch "option" V sml $'


def test_reminder():
    (flexmock(client)
     .should_receive('lunch_logged')
     .and_return(False))
    assert client.do_reminder().message == "Don't forget to log lunch for today"


def test_reminder_logged():
    (flexmock(client)
     .should_receive('lunch_logged')
     .and_return(True))
    assert client.do_reminder() is None
