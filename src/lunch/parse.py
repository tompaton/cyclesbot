from pyparsing import (Word, alphas, Group, OneOrMore, oneOf, Optional,
                       Suppress, WordEnd, Combine)

from common.parse import permutationOf
from common.when import past_dow_keywords, get_past_date

LunchKeyword = Optional(Suppress('/')) + oneOf('lunch', caseless=True)
LunchCommand = oneOf('stats recent options reminder', caseless=True)('command')
Type = Combine(oneOf('m v', caseless=True) + WordEnd())
Size = oneOf('sml med lrg', caseless=True)
Cost = oneOf('$ $$ $$$ -', caseless=True)
When = oneOf(past_dow_keywords(), caseless=True).setName('weekday')
What = Group(OneOrMore(Word(alphas + '/'), stopOn=(Type | Size | Cost))).setName('food')

Categories = permutationOf(Optional(Type, default='m')('group'),
                           Optional(Size, default='med')('size'),
                           Optional(Cost, default='-')('cost')).setName('type size cost')

LunchItem = (Optional(Suppress(oneOf('add ate had from', caseless=True)))
             + What('what')
             + Categories
             + Optional(When, default='today')('when'))
Message = OneOrMore(Group(Suppress(LunchKeyword)
                          + Optional(LunchCommand | LunchItem)))

def parse_msg(msg):
    for res in Message.parseString(msg):
        kwargs = {}
        if 'command' in res:
            cmd = res['command']
        else:
            if 'what' in res:
                cmd = 'add'
            else:
                cmd = 'options'

        if 'what' in res:
            kwargs['what'] = ' '.join([word.title()
                                       for word in res['what']])
        if 'group' in res:
            kwargs['group'] = res['group'].upper()
        for kw in ('size', 'cost'):
            if kw in res:
                kwargs[kw] = res[kw]
        if 'when' in res:
            kwargs['when_raw']= res['when']
            kwargs['when']= get_past_date(res['when'])

        yield cmd, kwargs

HELP = """
*lunch* [options|stats|recent|reminder]
*lunch* [add|ate|had|from] _description_ [m|v] [sml|med|lrg] [$|-] [today|weekday]
"""
