import pytest
from flexmock import flexmock
from . import parse_msg
from . import parse


@pytest.mark.parametrize('msg,cmd,kwargs', [
    ('/lunch', 'options', {}),
    ('/lunch stats', 'stats', {}),
    ('lunch recent', 'recent', {}),
    ('lunch reminder', 'reminder', {}),
    ('lunch options', 'options', {}),
    ('lunch Muffin/Cake sml v $',
     'add', {'cost': '$', 'group': 'V', 'size': 'sml', 'what': 'Muffin/Cake',
             'when': 'today', 'when_raw': 'today'}),
    ('lunch home',
     'add', {'cost': '-', 'group': 'M', 'size': 'med', 'what': 'Home',
             'when': 'today', 'when_raw': 'today'}),
    ('lunch home m',
     'add', {'cost': '-', 'group': 'M', 'size': 'med', 'what': 'Home',
             'when': 'today', 'when_raw': 'today'}),
    ('lunch home med $ v',
     'add', {'cost': '$', 'group': 'V', 'size': 'med', 'what': 'Home',
             'when': 'today', 'when_raw': 'today'}),
    ('lunch home m med',
     'add', {'cost': '-', 'group': 'M', 'size': 'med', 'what': 'Home',
             'when': 'today', 'when_raw': 'today'}),
    ('lunch add home m sml -',
     'add', {'cost': '-', 'group': 'M', 'size': 'sml', 'what': 'Home',
             'when': 'today', 'when_raw': 'today'}),
    ('lunch pizza/calzone m med -',
     'add', {'cost': '-', 'group': 'M', 'size': 'med', 'what': 'Pizza/Calzone',
             'when': 'today', 'when_raw': 'today'}),
    ('lunch baked potato v med $$',
     'add', {'cost': '$$', 'group': 'V', 'size': 'med', 'what': 'Baked Potato',
             'when': 'today', 'when_raw': 'today'}),
    ('lunch from home m lrg -',
     'add', {'cost': '-', 'group': 'M', 'size': 'lrg', 'what': 'Home',
             'when': 'today', 'when_raw': 'today'}),
    ('lunch home m med - yesterday',
     'add', {'cost': '-', 'group': 'M', 'size': 'med', 'what': 'Home',
             'when': 'yesterday',
             'when_raw': 'yesterday'}),
    ('lunch ate baguette v sml $',
     'add', {'cost': '$', 'group': 'V', 'size': 'sml', 'what': 'Baguette',
             'when': 'today', 'when_raw': 'today'}),
    ('lunch baguette v lrg $',
     'add', {'cost': '$', 'group': 'V', 'size': 'lrg', 'what': 'Baguette',
             'when': 'today', 'when_raw': 'today'}),
])
def test_lunch(msg, cmd, kwargs):
    (flexmock(parse)
     .should_receive('get_past_date')
     .replace_with(lambda when: when))

    assert list(parse_msg(msg)) == [(cmd, kwargs)]
