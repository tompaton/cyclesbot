from datetime import datetime
from common import bash
from chat.msg_types import Reply


def do_add(what, group, size, cost, when, when_raw):
    if when_raw != 'today':
        return 'Sorry, currently can only add for today.'

    return bash.run('lunch/lunch_functions.sh', 'lunch',
                    what, group, size, cost), _undo_add


def _undo_add():
    return bash.run('lunch/lunch_functions.sh', 'lunch_undo')


def do_recent():
    return bash.run('lunch/lunch_functions.sh', 'lunch_recent')


def do_stats():
    return bash.run('lunch/lunch_functions.sh', 'lunch_stats')


def do_options():
    return bash.run('lunch/lunch_functions.sh', 'lunch').replace('\\$', '$')


do_ate = do_had = do_from = do_add


def do_reminder():
    today = datetime.now().date()
    if lunch_logged(today):
        return

    return Reply("Don't forget to log lunch for today")


def lunch_logged(date):  # pragma: no cover
    return date.strftime('%Y-%m-%d') in do_recent()
