from flexmock import flexmock
from . import client


# TODO: support this again somehow - detect /podcasts/downloaded.rss?
# def test_disabled():
#     reply = client.do_command('list', []')
#     assert reply == 'Function not enabled.'


def test_list():
    (flexmock(client.manage)
     .should_receive('list_downloads')
     .with_args(grep=[])
     .and_return('full list'))

    reply = client.do_command('list', [])
    assert reply.message == 'full list'
    assert reply.send_args['parse_mode'] == 'Markdown'


def test_list_filter():
    (flexmock(client.manage)
     .should_receive('list_downloads')
     .with_args(grep=['filter'])
     .and_return('filtered list'))

    reply = client.do_command('list', ['filter'])
    assert reply.message == 'filtered list'


def test_rss():
    (flexmock(client.manage)
     .should_receive('generate_rss')
     .with_args()
     .once())

    reply = client.do_command('rss', [])
    assert reply.message == 'Generated downloaded.rss'


def test_remove():
    (flexmock(client.manage)
     .should_receive('remove_download')
     .with_args(mp3s=['episode'])
     .and_return('done'))

    (flexmock(client.manage)
     .should_receive('generate_rss')
     .with_args()
     .times(2))  # undo as well

    reply = client.do_command('remove', ['episode'])
    assert reply.message == 'done'
    assert reply.send_args['parse_mode'] == 'Markdown'

    (flexmock(client.manage)
     .should_receive('undo_remove')
     .with_args()
     .and_return('undo'))

    assert reply.undo().message == 'undo'


def test_remove_multiple():
    (flexmock(client.manage)
     .should_receive('remove_download')
     .with_args(mp3s=['episode1', 'episode2'])
     .and_return('done'))

    (flexmock(client.manage)
     .should_receive('generate_rss')
     .with_args()
     .once())

    reply = client.do_command('remove', ['episode1', 'episode2'])
    assert reply.message == 'done'


def test_remove_completed():
    (flexmock(client.manage)
     .should_receive('remove_download')
     .with_args(mp3s=[])
     .and_return('done'))

    (flexmock(client.manage)
     .should_receive('generate_rss')
     .with_args()
     .once())

    reply = client.do_command('remove', [])
    assert reply.message == 'done'


def test_save():
    (flexmock(client.manage)
     .should_receive('save_download')
     .with_args(mp3s=['episode'])
     .and_return('done'))

    (flexmock(client.manage)
     .should_receive('generate_rss')
     .with_args()
     .once())

    reply = client.do_command('save', ['episode'])
    assert reply.message == 'done'


def test_play():
    (flexmock(client.manage)
     .should_receive('play_download')
     .with_args(mp3s=['episode.mp3'])
     .and_return({'filename': '/podcasts/processed/episode.mp3',
                  'url': 'https://podcasts.tompaton.com/downloaded/episode.mp3',
                  'author': 'Tony C Smith',
                  'title': 'www.starshipsofa.com StarShipSofa No 519 Hannu Rajaniemi',
                  'duration_seconds': 2901,
                  'length': 8551653}))

    reply = client.do_command('play', ['episode.mp3'])
    assert reply.url == '/podcasts/processed/episode.mp3'
    assert type(reply).__name__ == 'Audio'
    assert reply.artist == 'Tony C Smith'
    assert reply.title == ('www.starshipsofa.com '
                           'StarShipSofa No 519 Hannu Rajaniemi')
    assert reply.duration == 2901


def test_play_untagged():
    (flexmock(client.manage)
     .should_receive('play_download')
     .with_args(mp3s=['episode.mp3'])
     .and_return({'filename': '/podcasts/processed/episode.mp3',
                  'url': 'https://podcasts.tompaton.com/downloaded/episode.mp3',
                  'author': 'Unknown author',
                  'title': 'Unknown title',
                  'duration_seconds': 0,
                  'length': 8551653}))

    reply = client.do_command('play', ['episode.mp3'])
    assert reply.url == '/podcasts/processed/episode.mp3'
    assert type(reply).__name__ == 'Audio'
    assert reply.artist is None
    assert reply.title is None
    assert reply.duration is None


def test_play_not_found():
    (flexmock(client.manage)
     .should_receive('play_download')
     .with_args(mp3s=['missing.mp3'])
     .and_return(None))

    reply = client.do_command('play', ['missing.mp3'])
    assert reply.message == 'Not found!'
    assert type(reply).__name__ == 'Reply'


def test_play_too_big():
    (flexmock(client.manage)
     .should_receive('play_download')
     .with_args(mp3s=['large.mp3'])
     .and_return({'filename': '/podcasts/processed/large.mp3',
                  'url': 'https://podcasts.tompaton.com/downloaded/large.mp3',
                  'author': 'Tony C Smith',
                  'title': 'www.starshipsofa.com StarShipSofa No 519 Hannu Rajaniemi',
                  'duration_seconds': 2901,
                  'length': 53215333}))

    reply = client.do_command('play', ['large.mp3'])
    assert reply.message == 'https://podcasts.tompaton.com/downloaded/large.mp3'
    assert type(reply).__name__ == 'Reply'


def test_download():
    (flexmock(client.manage)
     .should_receive('download_episode')
     .with_args(urls=['http://example.com/url.mp3'])
     .and_return('done'))

    (flexmock(client.manage)
     .should_receive('generate_rss')
     .with_args()
     .once())

    reply = client.do_command('download', ['http://example.com/url.mp3'])
    assert reply.message == 'done'
