from podcast_downloader import manage

from chat.msg_types import Audio, Reply


def do_command(action, name):
    if action == 'list':
        return Reply(manage.list_downloads(grep=name), parse_mode='Markdown')

    elif action == 'rss':
        manage.generate_rss()
        return Reply('Generated downloaded.rss')

    elif action == 'remove':
        def _undo_remove():
            result = manage.undo_remove()
            manage.generate_rss()
            return Reply(result, parse_mode='Markdown')

        result = manage.remove_download(mp3s=name)
        manage.generate_rss()
        return Reply(result, parse_mode='Markdown', undo=_undo_remove)

    elif action == 'save':
        result = manage.save_download(mp3s=name)
        manage.generate_rss()
        return Reply(result, parse_mode='Markdown')

    elif action == 'play':
        result = manage.play_download(mp3s=name)

        if result is None:
            return Reply('Not found!')

        else:
            if result['length'] < 50 * 1024 * 1024:
                artist = result['author']
                if artist == 'Unknown author':
                    artist = None

                title = result['title']
                if title == 'Unknown title':
                    title = None

                return Audio(result['filename'], artist=artist, title=title,
                             duration=result['duration_seconds'] or None)
            else:
                # fallback to url if file is too big
                return Reply(result['url'])

    elif action == 'download':
        result = manage.download_episode(urls=name)
        manage.generate_rss()
        return Reply(result, parse_mode='Markdown')
