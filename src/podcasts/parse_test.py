import pytest
from . import parse_msg


@pytest.mark.parametrize('msg,cmd,kwargs', [
    ('/podcasts', 'command', {'action': 'list', 'name': []}),
    ('/podcasts list', 'command', {'action': 'list', 'name': []}),
    ('podcasts list', 'command', {'action': 'list', 'name': []}),
    ('podcasts list bbg', 'command', {'action': 'list', 'name': ['bbg']}),
    ('/podcasts rss', 'command', {'action': 'rss', 'name': []}),
    ('podcasts rm 1', 'command', {'action': 'remove', 'name': ['1']}),
    ('podcasts delete 1', 'command', {'action': 'remove', 'name': ['1']}),
    ('podcasts remove bbg_171220.mp3',
     'command', {'action': 'remove', 'name': ['bbg_171220.mp3']}),
    ('podcasts remove bbg-171220.mp3',
     'command', {'action': 'remove', 'name': ['bbg-171220.mp3']}),
    ('podcasts save 1', 'command', {'action': 'save', 'name': ['1']}),
    ('podcasts archive bbg_171220.mp3',
     'command', {'action': 'save', 'name': ['bbg_171220.mp3']}),
    ('podcasts play 1', 'command', {'action': 'play', 'name': ['1']}),
    ('podcasts play bbg_171220.mp3', 
     'command', {'action': 'play', 'name': ['bbg_171220.mp3']}),
    ('podcasts rm file1.mp3 file2.mp3',
     'command', {'action': 'remove', 'name': ['file1.mp3', 'file2.mp3']}),
    ('podcasts rm completed', 'command', {'action': 'remove', 'name': []}),
    ('podcasts download http://example.com/url.mp3',
     'command', {'action': 'download', 'name': ['http://example.com/url.mp3']}),
])
def test_podcasts(msg, cmd, kwargs):
    assert list(parse_msg(msg)) == [(cmd, kwargs)]
