from pyparsing import Group, Optional, Suppress, OneOrMore, oneOf
from common.parse import keyword, Url, FileName

PodcastsKeyword = Optional(Suppress('/')) + keyword('podcasts')

ListKeyword = keyword('list')
RSSKeyword = keyword('rss')
PlayKeyword = keyword('play')
SaveKeyword = keyword('save archive', action='save')
RemoveKeyword = keyword('remove rm delete del', action='remove')
DownloadKeyword = keyword('download')

Action = (ListKeyword | RSSKeyword | PlayKeyword | SaveKeyword
          | RemoveKeyword | DownloadKeyword)('action')
Name = Group(OneOrMore(Url | FileName))('name')

Completed = oneOf('completed', caseless=True)
RemoveCompleted = Group(Suppress(PodcastsKeyword)
                        + RemoveKeyword('action')
                        + Completed('completed'))

Message = RemoveCompleted \
          | Group(Suppress(PodcastsKeyword)
                  + Optional(Action)
                  + Optional(Name))

def parse_msg(msg):
    for res in Message.parseString(msg):
        kwargs = {'action': 'list', 'name': []}

        if 'action' in res:
            kwargs['action'] = res['action'].lower()

        if 'name' in res:
            kwargs['name'] = list(res['name'])

        if 'completed' in res:
            kwargs['name'] = []

        yield 'command', kwargs

HELP = """
*podcasts* [list|rss|play|remove|archive|download] _episode(s)_
*podcasts* remove completed
"""
