import pytest
from flexmock import flexmock


@pytest.fixture
def client():
    from . import client
    flexmock(client.config.opentabs_uid, get_value='uid')
    flexmock(client.config.opentabs_pwd, get_value='pwd')

    return client


def test_list(client):
    (flexmock(client.Opentabs)
     .should_receive('__init__')
     .with_args('uid', 'pwd')
     .once())

    (flexmock(client.Opentabs)
     .should_receive('get_urls')
     .and_return([
         client.Opentabs.Page(123, 'http://example.com/page.html',
                              'Page One'),
         client.Opentabs.Page(234, 'http://example.com/page/two',
                              'Page two')]))

    reply = client.do_list()
    assert reply.message \
        == ('https://opentabs.tompaton.com/\n'
            '[Page One](http://example.com/page.html) (123)\n'
            '[Page two](http://example.com/page/two) (234)')
    assert reply.send_args == {'disable_web_page_preview': True,
                               'parse_mode': 'Markdown'}


def test_add_title(client):
    (flexmock(client.Opentabs)
     .should_receive('add_url')
     .with_args('http://example.com/page.html', 'Page One')
     .and_return(client.Opentabs.Page(345, 'http://example.com/page.html',
                                      'Page One')))

    assert client.do_add('http://example.com/page.html', 'Page One').message \
        == ('Added [Page One](http://example.com/page.html) to Opentabs (345)')


def test_add_error(client):
    (flexmock(client.Opentabs)
     .should_receive('add_url')
     .with_args('http://example.com/page.html', 'Page One')
     .and_raise(client.PostError('error')))

    assert client.do_add('http://example.com/page.html', 'Page One').message \
        == 'Error: error'


def test_add(client):
    (flexmock(client.webpage)
     .should_receive('summary')
     .with_args('http://example.com/page.html')
     .and_return(('Page One', 'Summary for page one')))

    (flexmock(client.Opentabs)
     .should_receive('add_url')
     .with_args('http://example.com/page.html', 'Page One')
     .and_return(client.Opentabs.Page(345, 'http://example.com/page.html',
                                      'Page One')))

    assert client.do_add('http://example.com/page.html').message \
        == ('Added [Page One](http://example.com/page.html) to Opentabs (345)')


def test_remove(client):
    (flexmock(client.Opentabs)
     .should_receive('delete_url')
     .with_args(234)
     .and_return(True))

    assert client.do_remove(234).message == 'Removed Opentabs url 234.'


def test_remove_error(client):
    (flexmock(client.Opentabs)
     .should_receive('delete_url')
     .with_args(234)
     .and_raise(client.PostError('error')))

    assert client.do_remove(234).message == 'Error: error'
