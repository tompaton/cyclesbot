from config.var_types import ConfigParam

opentabs_uid = ConfigParam('Opentabs username')
opentabs_pwd = ConfigParam('Opentabs password')
