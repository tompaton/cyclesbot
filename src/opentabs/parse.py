from pyparsing import Group, Optional

from pyparsing import pyparsing_common

from common.parse import keyword, Url, QuotedString

Title = QuotedString().setName('title')

UrlID = pyparsing_common.integer

ListUrls = Group(keyword('opentabs', 'list', action='list')('command')
                 | keyword('opentabs', action='list')('command'))

AddUrl = Group(keyword('opentabs', 'add', action='add')('command')
               + Url('url')
               + Optional(Title('title')))

RemoveUrl = Group(keyword('opentabs', 'remove rm', action='remove')('command')
                  + UrlID('url_id'))

Message = RemoveUrl | AddUrl | ListUrls


def parse_msg(msg):
    for res in Message.parseString(msg, parseAll=True):
        kwargs = {key: res[key] for key in ('url', 'title', 'url_id')
                  if key in res}
        yield res['command'], kwargs


HELP = """
*opentabs* [list]
*opentabs* add [url] {"title"}
*opentabs* rm|remove [url\\_id]
"""
