from opentabs_client import Opentabs, PostError

from common import webpage
from chat.msg_types import Reply
from . import config


def _tabs():
    return Opentabs(config.opentabs_uid.get_value(),
                    config.opentabs_pwd.get_value())


def do_list():
    return Reply(
        Opentabs.URL + '\n'
        + '\n'.join(f'[{page.title}]({page.url}) ({page.url_id})'
                    for page in _tabs().get_urls()),
        disable_web_page_preview=True)


def do_add(url=None, title=None):
    if title is None:
        title = webpage.summary(url)[0]

    try:
        new_page = _tabs().add_url(url, title or url)
    except PostError as e:
        return Reply(f'Error: {e}')
    else:
        return Reply(f'Added [{new_page.title}]({new_page.url}) '
                     f'to Opentabs ({new_page.url_id})')


def do_remove(url_id=None):
    try:
        # TODO: support undo
        _tabs().delete_url(url_id)
    except PostError as e:
        return Reply(f'Error: {e}')
    else:
        return Reply(f'Removed Opentabs url {url_id}.')
