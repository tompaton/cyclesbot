import pytest
from . import parse_msg


@pytest.mark.parametrize('msg,cmd,kwargs', [
    ('opentabs', 'list', {}),
    ('opentabs list', 'list', {}),
    ('opentabs add http://example.com/page.html',
     'add', {'url': 'http://example.com/page.html'}),
    ('opentabs add http://example.com/page.html "page title"',
     'add', {'url': 'http://example.com/page.html',
             'title': 'page title'}),
    ('opentabs remove 123', 'remove', {'url_id': 123}),
    ('opentabs rm 123', 'remove', {'url_id': 123}),
])
def test_opentabs(msg, cmd, kwargs):
    assert list(parse_msg(msg)) == [(cmd, kwargs)]
