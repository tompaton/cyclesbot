last_command = None

def do_undo():
    if callable(last_command):
        return last_command()
    else:
        return 'Nothing to undo!'

def set_undo(undo_command):
    global last_command

    if callable(undo_command):
        last_command = undo_command
    else:
        last_command = None
