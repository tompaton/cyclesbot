import pytest
from pyparsing import ParseException
from . import parse_msg


@pytest.mark.parametrize('msg,cmd,kwargs', [
    ('/undo', 'undo', {}),
    ('undo', 'undo', {}),
])
def test_undo(msg, cmd, kwargs):
    assert list(parse_msg(msg)) == [(cmd, kwargs)]


@pytest.mark.parametrize('msg', [
    'don\'t undo this',
])
def test_dont_undo(msg):
    with pytest.raises(ParseException):
        list(parse_msg(msg))
