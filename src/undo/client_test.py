from . import client


def test_no_undo():
    assert client.do_undo() == 'Nothing to undo!'


def test_set_undo_none():
    client.set_undo(None)
    assert client.do_undo() == 'Nothing to undo!'


def test_set_undo():
    client.set_undo(lambda: 'Undone!')
    assert client.do_undo() == 'Undone!'
