from pyparsing import Group, oneOf, Optional, Suppress

UndoCommand = Optional(Suppress('/')) + oneOf('undo', caseless=True)
Message = Group(UndoCommand('command'))

def parse_msg(msg):
    for res in Message.parseString(msg):
        yield res['command'][0], {}

HELP = """
*undo*
"""
