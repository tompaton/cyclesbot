# coding: utf-8

import pytest
from datetime import date
from flexmock import flexmock
from common.test import mock_contextmanager


@pytest.fixture
def config():
    return {'pwd': 'secret'}


@pytest.fixture
def client(config):
    from . import client

    for param, value in config.items():
        flexmock(getattr(client.config, param), get_value=value)

    return client


@pytest.mark.parametrize('config', [{'pwd': None}])
def test_enabled_false(client):
    def func(arg):  # pragma: no cover
        return 'Oh no {}'.format(arg)

    assert client.enabled(func)('arg') == 'Bank functions disabled.'


def test_enabled_true(client):
    def func(arg):
        return 'Ready to go {}'.format(arg)

    assert client.enabled(func)('arg') == 'Ready to go arg'


@pytest.mark.parametrize('config', [{'pwd': 'pwd',
                                     'recent_days': '7'}])
def test_do_recent(client):
    (flexmock(client)
     .should_receive('list_transactions')
     .with_args(7)
     .and_return(['one', 'two']))

    assert client.do_recent(None)._messages == ['one', 'two']


def test_do_recent_days(client):
    (flexmock(client)
     .should_receive('list_transactions')
     .with_args(3)
     .and_return(['one', 'two']))

    assert client.do_recent(3)._messages == ['one', 'two']


def test_do_new(client):
    (flexmock(client)
     .should_receive('list_transactions')
     .with_args()
     .and_return(['one', 'two']))

    reply = client.do_new()
    assert reply._messages == ['one', 'two']
    for r in reply.replies:
        assert r.send_args == {'disable_notification': True,
                               'disable_web_page_preview': False,
                               'parse_mode': 'Markdown'}


@pytest.mark.parametrize('config', [{'login_url': 'bank.url/login',
                                     'csv_url': 'bank.url/csv',
                                     'uid': '1234',
                                     'pwd': 'secret'}])
def test_list_transactions(client):
    db = flexmock()

    (flexmock(client.database)
     .should_receive('init_db')
     .with_args('/data/bank.db')
     .and_return(db))

    bank = flexmock()

    (flexmock(client.fetch)
     .should_receive('BankAustralia')
     .with_args('bank.url/login', 'bank.url/csv', '1234', 'secret')
     .and_return(bank))

    account = {'account_name': 'First', 'last_balance_str': '$1,000.00'}
    transactions = [{'iso_entered_date': '2010-02-01',
                     'amount_str': '-$10.00',
                     'cleaned_desc': 'visa-lorum ipsum',
                     'label_names': '⊟,💳,👩'}]

    (flexmock(client.process)
     .should_receive('latest_transactions')
     .with_args(db, bank, None)
     .and_return([(account, transactions)]))

    assert client.list_transactions() == [
        'First\n'
        '*2010-02-01*\n'
        '-$10.00 visa-lorum ipsum ⊟ 💳 👩\n'
        'Balance *$1,000.00*'
    ]


@pytest.mark.parametrize('config', [{'login_url': 'bank.url/login',
                                     'csv_url': 'bank.url/csv',
                                     'uid': '1234',
                                     'pwd': 'secret'}])
def test_list_transactions_days(client):
    db = flexmock()

    (flexmock(client.database)
     .should_receive('init_db')
     .with_args('/data/bank.db')
     .and_return(db))

    bank = flexmock()

    (flexmock(client.fetch)
     .should_receive('BankAustralia')
     .with_args('bank.url/login', 'bank.url/csv', '1234', 'secret')
     .and_return(bank))

    account = {'account_name': 'First', 'last_balance_str': '$1,000.00'}
    transactions = []

    (flexmock(client.process)
     .should_receive('latest_transactions')
     .with_args(db, bank, 5)
     .and_return([(account, transactions)]))

    assert client.list_transactions(5) == [
        'First\n'
        'Balance *$1,000.00*'
    ]


def test_format_transactions(client):
    account = {'account_name': 'First', 'last_balance_str': '$1,000.00'}
    transactions = [{'iso_entered_date': '2010-02-01',
                     'amount_str': '-$10.00',
                     'cleaned_desc': 'visa-lorum *ipsum',
                     'label_names': '⊟,💳,👩'},
                    {'iso_entered_date': '2010-02-01',
                     'amount_str': '-$20.00',
                     'cleaned_desc': 'visa-dolor sit amet',
                     'label_names': '⊟,💳,👨'},
                    {'iso_entered_date': '2010-02-02',
                     'amount_str': '-$30.00',
                     'cleaned_desc': 'visa-consectetur adipiscing',
                     'label_names': '⊞,fee'}]
    assert list(client.format_transactions(account, transactions)) == [
        'First',
        '*2010-02-01*',
        r'-$10.00 visa-lorum \*ipsum ⊟ 💳 👩',
        '-$20.00 visa-dolor sit amet ⊟ 💳 👨',
        '*2010-02-02*',
        '-$30.00 visa-consectetur adipiscing ⊞ fee',
        'Balance *$1,000.00*',
    ]
