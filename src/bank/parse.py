from pyparsing import Group, Optional, Suppress
from common.parse import keyword
from pyparsing import pyparsing_common

Days = pyparsing_common.integer

Recent = Group(keyword('bank', 'recent', action='recent')('action')
               + Optional(Days, default=None)('days'))

New = Group(keyword('bank', 'new', action='new')('action'))

Message = Optional(Suppress('/')) + (Recent | New)

def parse_msg(msg):
    for res in Message.parseString(msg):
        kwargs = {key: res[key] for key in ('days',)
                  if key in res}
        yield res['action'], kwargs

HELP = """
*bank* recent [days]
*bank* new
"""
