import sys
from better_exceptions import format_exception
from bankcheck import database, process, fetch
from chat.msg_types import ReplyList
from common.util import escape_markdown
from . import config


enabled = config.pwd.required('Bank functions disabled.')


@enabled
def do_recent(days):
    if days is None:
        days = int(config.recent_days.value)
    return ReplyList(*list_transactions(days))


@enabled
def do_new():
    return ReplyList(*list_transactions(),
                     disable_notification=True)


def list_transactions(days=None):
    db = database.init_db('/data/bank.db')
    bank = fetch.BankAustralia(config.login_url.value, config.csv_url.value,
                               config.uid.value, config.pwd.value)

    try:
        return ['\n'.join(format_transactions(account, transactions))
                for account, transactions
                in process.latest_transactions(db, bank, days)]
    except:
        print(format_exception(*sys.exc_info()))
        return ['Error fetching recent bank transactions']


def format_transactions(account, transactions):
    yield account['account_name']

    entered_date = None
    for row in transactions:
        if row['iso_entered_date'] != entered_date:
            entered_date = row['iso_entered_date']
            yield '*{}*'.format(entered_date)

        # TODO: output one-off/recurring, histogram of amounts
        yield '{amount_str} {cleaned_desc} {label_names}'.format(
            amount_str=row['amount_str'],
            cleaned_desc=escape_markdown(row['cleaned_desc']),
            label_names=escape_markdown(row['label_names']
                                        .replace(',', ' ')))

    yield 'Balance *{}*'.format(account['last_balance_str'])
