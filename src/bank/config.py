from config.var_types import ConfigParam, EnvVar, InputVar

login_url = EnvVar('BANK_LOGIN_URL')
csv_url = EnvVar('BANK_CSV_URL')

uid = ConfigParam(name='Bank user id')
pwd = InputVar(name='Bank password')

recent_days = ConfigParam(name='Recent days default', default='7')
