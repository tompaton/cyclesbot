import pytest
from pyparsing import ParseException
from . import parse_msg


@pytest.mark.parametrize('msg,cmd,kwargs', [
    ('/bank recent', 'recent', {'days': None}),
    ('bank recent 60', 'recent', {'days': 60}),
    ('bank new', 'new', {}),
])
def test_bank(msg, cmd, kwargs):
    assert list(parse_msg(msg)) == [(cmd, kwargs)]


@pytest.mark.parametrize('msg', ['don\'t bank this'])
def test_dont_bank(msg):
    with pytest.raises(ParseException):
        list(parse_msg(msg))
