from pyparsing import Group, oneOf, Optional, Suppress, Word, alphas

HelpCommand = Optional(Suppress('/')) + oneOf('help', caseless=True)
Message = Group(HelpCommand('command')
                + Optional(Word(alphas), default='')('module').setName('module'))

def parse_msg(msg):
    for res in Message.parseString(msg):
        yield res['command'][0], {'module': res['module']}

HELP = """
*help* [module]
"""
