import pytest
from pyparsing import ParseException
from . import parse_msg


@pytest.mark.parametrize('msg,cmd,kwargs', [
    ('/help', 'help', {'module': ''}),
    ('help', 'help', {'module': ''}),
    ('help module', 'help', {'module': 'module'}),
])
def test_help(msg, cmd, kwargs):
    assert list(parse_msg(msg)) == [(cmd, kwargs)]


@pytest.mark.parametrize('msg', [
    'don\'t help this',
])
def test_dont_help(msg):
    with pytest.raises(ParseException):
        list(parse_msg(msg))
