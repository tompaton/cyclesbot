def do_help(module):
    from chat.modules import _modules

    for mod in _modules:
        if mod.__name__ == module:
            doc = getattr(mod.parse, 'HELP', 'Not available').strip()
            # still want bold and italic formatting, only escape '[' to indicate
            # optional args
            return doc.replace('[', r'\[')

    return 'modules: ' + ', '.join(sorted(mod.__name__ for mod in _modules))
