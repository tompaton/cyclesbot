import pytest
from flexmock import flexmock
from . import client


@pytest.fixture
def modules():
    import chat.modules
    mod1 = flexmock(__name__='ghi', parse=flexmock())
    mod2 = flexmock(__name__='def', parse=flexmock(HELP='def [help]'))
    mod3 = flexmock(__name__='abc', parse=flexmock())
    flexmock(chat.modules, _modules=[mod1, mod2, mod3])


def test_help(modules):
    assert client.do_help('') == 'modules: abc, def, ghi'


def test_help_module_not_available(modules):
    assert client.do_help('abc') == 'Not available'


def test_help_module(modules):
    assert client.do_help('def') == r'def \[help]'
