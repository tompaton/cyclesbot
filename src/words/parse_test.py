import pytest
from . import parse_msg


@pytest.mark.parametrize('msg,cmd,kwargs', [
    ('#words', 'words', {}),
    ('#unword one', 'unword', {'word': 'one'}),
    ('#word one ONE', 'word', {'word': 'one', 'result': 'ONE'}),
    ('#word one ONE TWO', 'word', {'word': 'one', 'result': 'ONE TWO'}),
    ('#word one_two ONE', 'word', {'word': 'one_two', 'result': 'ONE'}),
    ('#word one $two', 'word', {'word': 'one', 'result': '$two'}),
    ('#word one lorem\n$two\nipsum', 'word',
     {'word': 'one', 'result': 'lorem\n$two\nipsum'}),
    ('#exec one $two', 'exec', {'result': 'one $two'}),
])
def test_words(msg, cmd, kwargs):
    assert list(parse_msg(msg)) == [(cmd, kwargs)]
