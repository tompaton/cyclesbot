import re
from pyparsing import Group, OneOrMore, oneOf, Suppress, Regex


Word = Regex(r'\w+')('word').setName('word')

Result = Regex('.+$', flags=re.MULTILINE | re.DOTALL)('result').setName('result')

ListKeyword = oneOf('words', caseless=True)('command')
ListCommand = ListKeyword

UndefKeyword = oneOf('unword', caseless=True)('command')
UndefCommand = UndefKeyword + Word

DefineKeyword = oneOf('word', caseless=True)('command')
DefineCommand = DefineKeyword + Word + Result

ExecKeyword = oneOf('exec', caseless=True)('command')
ExecCommand = ExecKeyword + Result

Message = Group(Suppress('#') + (ListCommand | UndefCommand | DefineCommand
                                 | ExecCommand))


def parse_msg(msg):
    for res in Message.parseString(msg, parseAll=True):
        kwargs = {key: res[key] for key in ['word', 'result'] if key in res}
        yield res['command'], kwargs


HELP = """
*#words*
*#word* _word_ _command_
*#exec* _command_
*#unword* _word_
"""
