import sqlite3
import re

from collections import namedtuple

from chat.modules import run_command
from chat.msg_types import Reply
from common.util import escape_markdown as md


def do_words():
    return Reply('\n'.join(row.md() for row in fetch_words()),
                 disable_web_page_preview=True)


def do_word(word, result):
    # TODO: check if word already exists?

    with get_conn() as conn:
        cur = conn.cursor()
        cur.execute('INSERT INTO words (word, result, created) '
                    "VALUES (?, ?, datetime('now'))",
                    (word, result))
        rowid = cur.lastrowid

    def _undo():
        with get_conn() as conn:
            conn.execute("UPDATE words SET deleted=datetime('now') "
                         "WHERE rowid = ?", (rowid,))
        return f'Deleted {WordRow.md_ex(word, result)}'

    return f'Added {WordRow.md_ex(word, result)}', _undo


def do_exec(result):
    for reply in run_command(result)[1]:
        reply.push()

    return f'Finished.'


def do_unword(word):
    words = fetch_words(word=word)
    if words:
        with get_conn() as conn:
            for row in words:
                conn.execute("UPDATE words SET deleted=datetime('now') "
                             "WHERE rowid = ?", (row.rowid,))

        def _undo():
            for row in words:
                conn.execute("UPDATE words SET deleted=NULL "
                             "WHERE rowid = ?", (row.rowid,))
            return '\n'.join(f'Restored {row.md()}' for row in words)

        return ('\n'.join(f'Deleted {row.md()}' for row in words),
                _undo)

    else:
        return f"Couldn't find word *{md(word)}*."


RE_WORD = re.compile(r'\$\w+')


def expand_words(text):
    expanded = []

    words = {f'${row.word}': row.result.splitlines()
             for row in fetch_words()}

    def expand_word(word):
        return words.get(word.lower().strip(), [word])

    if RE_WORD.fullmatch(text.strip()):
        # single $word can expand to one or more commands
        expanded.extend(expand_word(text))

    else:
        def repl(match):
            return expand_word(match[0])[0]

        # expand any $words in command
        expanded.append(RE_WORD.sub(repl, text))

    # recursively expand words further if necessary
    for expanded_word in expanded:
        if expanded_word != text:
            yield from expand_words(expanded_word)
        else:
            yield expanded_word


def get_conn():
    conn = sqlite3.connect('/data/words.db')
    conn.execute("CREATE TABLE IF NOT EXISTS words ("
                 "word TEXT, result TEXT, "
                 "created INTEGER, deleted INTEGER"
                 ")")
    return conn


class WordRow(namedtuple('WordRow', 'word result created deleted rowid')):

    def md(self):
        return self.md_ex(self.word, self.result)

    @staticmethod
    def md_ex(word, result):
        return f'*{word}* {md(result)}'


def fetch_words(**kwargs):
    where = ['deleted IS NULL'] + [key + ' = ?'
                                   for key, value in kwargs.items()]
    params = tuple(value for key, value in kwargs.items())

    cur = get_conn().cursor()
    cur.execute('SELECT word, result, created, deleted, rowid '
                'FROM words WHERE {}'.format(' AND '.join(where)),
                params)
    return [WordRow(*row) for row in cur.fetchall()]
