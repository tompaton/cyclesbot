from unittest.mock import patch

from flexmock import flexmock
import pytest

from common.test import mock_contextmanager

from . import client


@pytest.fixture
def word_rows():
    return [client.WordRow('one', 'UNO WORD', 10, None, 1),
            client.WordRow('two', 'DUO FIRST\nDUO_SECOND', 20, None, 2),
            client.WordRow('three', '$one TWO $zero', 30, None, 3)]


@pytest.mark.parametrize('text,expanded',
                         [('', ['']),
                          ('  ', ['  ']),
                          ('lorem ipsum', ['lorem ipsum']),
                          ('$zero', ['$zero']),
                          ('$one', ['UNO WORD']),
                          ('lorem $one ipsum', ['lorem UNO WORD ipsum']),
                          ('$two', ['DUO FIRST', 'DUO_SECOND']),
                          ('lorem $two ipsum', ['lorem DUO FIRST ipsum']),
                          ('$three', ['UNO WORD TWO $zero'])])
@patch('words.client.fetch_words')
def test_expand_words(mock_fetch_words, word_rows, text, expanded):
    mock_fetch_words.return_value = word_rows
    assert list(client.expand_words(text)) == expanded


def test_fetch_words():
    cur = flexmock()

    (flexmock(cur)
     .should_receive('execute')
     .with_args('SELECT word, result, created, deleted, rowid '
                'FROM words WHERE deleted IS NULL AND word = ?',
                ('one',))
     .once())

    (flexmock(cur)
     .should_receive('fetchall')
     .and_return([('one', 'UNO WORD', 1234567, None, 1),
                  ('two', 'DUO FIRST', 1234567, None, 2)]))

    conn = flexmock()

    (flexmock(conn)
     .should_receive('cursor')
     .and_return(cur))

    (flexmock(client)
     .should_receive('get_conn')
     .and_return(conn))

    assert (client.fetch_words(word='one')
            == [client.WordRow('one', 'UNO WORD', 1234567, None, 1),
                client.WordRow('two', 'DUO FIRST', 1234567, None, 2)])


@patch('words.client.fetch_words')
def test_do_words(mock_fetch_words, word_rows):
    mock_fetch_words.return_value = word_rows
    reply = client.do_words()
    assert reply.message \
        == ('*one* UNO WORD\n'
            '*two* DUO FIRST\nDUO\\_SECOND\n'
            '*three* $one TWO $zero')
    assert reply.send_args == {'disable_web_page_preview': True,
                               'parse_mode': 'Markdown'}


def test_do_word():
    cur = flexmock(lastrowid=3)
    (flexmock(cur)
     .should_receive('execute')
     .with_args("INSERT INTO words (word, result, created) "
                "VALUES (?, ?, datetime('now'))",
                ('one', 'UNO WORD'))
     .once())

    conn = flexmock()
    flexmock(conn).should_receive('cursor').and_return(cur)

    (flexmock(client)
     .should_receive('get_conn')
     .replace_with(mock_contextmanager(conn)))

    reply = client.do_word('one', 'UNO WORD')
    assert reply[0] == 'Added *one* UNO WORD'

    # undo
    (flexmock(conn)
     .should_receive('execute')
     .with_args("UPDATE words SET deleted=datetime('now') "
                "WHERE rowid = ?", (3,))
     .once())

    assert reply[1]() == 'Deleted *one* UNO WORD'


def test_unword_missing():
    (flexmock(client)
     .should_receive('fetch_words')
     .with_args(word='one')
     .and_return([])
     .once())

    assert client.do_unword('one') == "Couldn't find word *one*."


def test_unword():
    (flexmock(client)
     .should_receive('fetch_words')
     .with_args(word='one')
     .and_return([client.WordRow('one', 'UNO WORD', 1234567, None, 1),
                  client.WordRow('one', 'UNO TWO', 1234567, None, 2)])
     .once())

    conn = flexmock()
    (flexmock(conn)
     .should_receive('execute')
     .with_args("UPDATE words SET deleted=datetime('now') "
                "WHERE rowid = ?", (1,))
     .once())
    (flexmock(conn)
     .should_receive('execute')
     .with_args("UPDATE words SET deleted=datetime('now') "
                "WHERE rowid = ?", (2,))
     .once())

    (flexmock(client)
     .should_receive('get_conn')
     .replace_with(mock_contextmanager(conn)))

    reply = client.do_unword('one')
    assert reply[0] == "Deleted *one* UNO WORD\nDeleted *one* UNO TWO"

    # undo
    (flexmock(conn)
     .should_receive('execute')
     .with_args("UPDATE words SET deleted=NULL WHERE rowid = ?", (1,))
     .once())
    (flexmock(conn)
     .should_receive('execute')
     .with_args("UPDATE words SET deleted=NULL WHERE rowid = ?", (2,))
     .once())

    assert reply[1]() == 'Restored *one* UNO WORD\nRestored *one* UNO TWO'


def test_exec():
    reply1 = flexmock()
    (flexmock(reply1).should_receive('push').and_return(False).twice())

    (flexmock(client)
     .should_receive('run_command')
     .with_args('$one')
     .and_return((1, [reply1, reply1])))

    assert client.do_exec('$one') == "Finished."


@pytest.mark.parametrize('word,result,md',
                         [('one', 'two', '*one* two'),
                          ('o_ne', 'tw_o', '*o_ne* tw\\_o'),])
def test_md(word, result, md):
    assert client.WordRow.md_ex(word, result) == md
