import pytest
from pyparsing import ParseException
from . import parse_msg


@pytest.mark.parametrize('msg,cmd,kwargs', [
    ('/radar', 'radar', {'check': ''}),
    ('radar', 'radar', {'check': ''}),
    ('radar check', 'radar', {'check': 'check'}),
    ('radar url', 'radar', {'url': 'url'}),
])
def test_radar(msg, cmd, kwargs):
    assert list(parse_msg(msg)) == [(cmd, kwargs)]


@pytest.mark.parametrize('msg', [
    'don\'t radar this',
])
def test_dont_radar(msg):
    with pytest.raises(ParseException):
        list(parse_msg(msg))


@pytest.mark.parametrize('msg,cmd,kwargs', [
    ('/weather 12345', 'weather', {'location': '12345'}),
    ('weather 12345', 'weather', {'location': '12345'}),
    ('/temperatures 12345', 'temperatures', {'location': '12345',
                                             'save': ''}),
    ('temperatures 12345', 'temperatures', {'location': '12345',
                                            'save': ''}),
    ('temperatures 12345 save', 'temperatures', {'location': '12345',
                                                 'save': 'save'}),
    ('week weather 12345', 'week_weather', {'location': '12345'}),
])
def test_weather(msg, cmd, kwargs):
    assert list(parse_msg(msg)) == [(cmd, kwargs)]


@pytest.mark.parametrize('msg', [
    'don\'t weather this',
])
def test_dont_weather(msg):
    with pytest.raises(ParseException):
        list(parse_msg(msg))
