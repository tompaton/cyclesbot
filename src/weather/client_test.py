from datetime import datetime

import pytest

from flexmock import flexmock
from common.test import mock_contextmanager

from . import client


def test_weather():
    flexmock(client.config.weather_api_key, get_value='api_key')

    (flexmock(client.Weather)
     .should_receive('get_weather')
     .and_return(client.Weather.Forecast('weather', '2-5', 10)))

    assert client.do_weather('loc').message == 'weather Rainfall: 2-5mm 10%'


def test_weather_error():
    flexmock(client.config.weather_api_key, get_value='api_key')

    (flexmock(client.Weather)
     .should_receive('get_weather')
     .and_return(client.Weather.Forecast('Error in get_weather()', '0', 100)))

    assert client.do_weather('loc').message \
        == 'Error in get\\_weather() Rainfall: 0mm 100%'


def test_week_weather():
    flexmock(client.config.weather_api_key, get_value='api_key')

    (flexmock(client.Weather)
     .should_receive('get_week_weather')
     .and_return([client.Weather.DayForecast('Mon', 'Partly Cloudy', 20, 30),
                  client.Weather.DayForecast('Tue', 'Very Cloudy', 21, 31),
                  client.Weather.DayForecast('Wed', 'Extremely Cloudy', 22, 32),
                  client.Weather.DayForecast('Thu', 'Clear', 23, 33)]))

    assert client.do_week_weather('loc').message \
        == ('*Tue* Min 21 Max 31\nVery Cloudy\n'
            '*Wed* Min 22 Max 32\nExtremely Cloudy\n'
            '*Thu* Min 23 Max 33\nClear')


def test_week_weather_error():
    flexmock(client.config.weather_api_key, get_value='api_key')

    (flexmock(client.Weather)
     .should_receive('get_week_weather')
     .and_return([client.Weather.DayForecast('', 'Error in get_week_weather()',
                                             0, 0)]))

    assert client.do_week_weather('loc').message \
        == '** Min 0 Max 0\nError in get\\_week\\_weather()'


def test_do_temperatures():
    flexmock(client.config.weather_api_key, get_value='api_key')

    temps = [15.0, 20.0, 25.0, 30.0, 25.0, 20.0]

    (flexmock(client.Weather)
     .should_receive('get_temperatures')
     .and_return(temps))

    assert client.do_temperatures('loc', '').message \
        == '15.0 20.0 25.0 30.0 25.0 20.0'


def test_do_temperatures_save():
    flexmock(client.config.weather_api_key, get_value='api_key')

    temps = [15.0, 20.0, 25.0, 30.0, 25.0, 20.0]

    (flexmock(client.Weather)
     .should_receive('get_temperatures')
     .and_return(temps))

    conn = flexmock()

    (flexmock(client.heater_db)
     .should_receive('init_db')
     .replace_with(mock_contextmanager(conn)))

    now = datetime(2018, 9, 16, 10, 11, 12)

    (flexmock(conn)
     .should_receive('get_place_id')
     .with_args('taylor-st')
     .and_return(123))

    for h, t in enumerate(temps):
        (flexmock(conn)
         .should_receive('insert_forecast')
         .with_args(now, 123, datetime(2018, 9, 16, h, 0, 0), t)
         .once())

    assert client.do_temperatures('loc', 'save', _now=now).message \
        == '15.0 20.0 25.0 30.0 25.0 20.0'


def test_radar_url():
    flexmock(client.config.radar_locations, get_value='IDR023,IDR513,IDR013')

    reply = client.do_radar(url='url', _now='now')
    assert reply.message \
        == 'http://www.bom.gov.au/products/IDR023.loop.shtml'
    assert reply.send_args['disable_web_page_preview'] is True


@pytest.mark.skip(reason="rewrite for echo image")
def test_radar():
    flexmock(client.config.radar_locations, get_value='IDR023,IDR513,IDR013')

    reply = client.do_radar(False, _now='now')
    assert reply.photos \
        == ['http://www.bom.gov.au/radar/IDR023.gif?now',
            'http://www.bom.gov.au/radar/IDR513.gif?now',
            'http://www.bom.gov.au/radar/IDR013.gif?now']
    assert reply.send_args['disable_notification'] is False


@pytest.mark.skip(reason="rewrite for echo image")
def test_radar_check():
    flexmock(client.config.radar_locations, get_value='IDR023,IDR513,IDR013')
    flexmock(client.config.rain_radar_pixel_cutoff, get_value='1000')

    (flexmock(client.Radar)
     .should_receive('any_rain')
     .and_return(True))

    reply = client.do_radar(True, _now='now')
    assert reply.photos \
        == ['http://www.bom.gov.au/radar/IDR023.gif?now',
            'http://www.bom.gov.au/radar/IDR513.gif?now',
            'http://www.bom.gov.au/radar/IDR013.gif?now']
    assert reply.send_args['disable_notification'] is False


def test_radar_check_no_rain():
    flexmock(client.config.radar_locations, get_value='IDR023')
    flexmock(client.config.rain_radar_pixel_cutoff, get_value='1000')

    (flexmock(client.Radar)
     .should_receive('any_rain')
     .and_return(False))

    reply = client.do_radar(True)
    assert reply is None
