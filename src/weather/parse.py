from pyparsing import Group, Optional, Suppress, Word, alphanums
from common.parse import keyword

RadarCommand = Group(keyword('radar')('command')
                     + Optional(keyword('check'), default='')('check'))
RadarCommand2 = Group(keyword('radar')('command')
                      + keyword('url')('url'))

Location = Word(alphanums).setName('location')

WeatherCommand = Group(keyword('weather')('command') + Location('location'))
TemperaturesCommand = Group(keyword('temperatures')('command')
                            + Location('location')
                            + Optional(keyword('save'), default='')('save'))
WeekWeatherCommand = Group(keyword('week', 'weather',
                                   action='week_weather')('command')
                           + Location('location'))

Message = Optional(Suppress('/')) + (WeekWeatherCommand
                                     | WeatherCommand
                                     | TemperaturesCommand
                                     | RadarCommand2 | RadarCommand)

def parse_msg(msg):
    for res in Message.parseString(msg):
        kwargs = {key: res[key] for key in ['location', 'check', 'save', 'url']
                  if key in res}
        yield res['command'], kwargs

HELP = """
*weather* _location_
*week weather* _location_
*temperatures* _location_ [save]
*radar* [check]
*radar url*
"""
