from datetime import datetime

from weather_client import Weather, Radar
from heater_logs import database as heater_db

from chat.client import do_echo_image
from chat.msg_types import Reply
from common.util import escape_markdown

from . import config


@config.radar_locations.required()
def do_radar(check='', url='', _now=None):
    radar = Radar(config.radar_locations.values, request_mode='ftp')

    if check and not radar.any_rain(int(config.rain_radar_pixel_cutoff.value)):
        return None

    # TODO: fallback to other urls if there is an error
    imgurl = radar.urls(_now=_now)[0]
    if url:
        # moved this to a separate image as otherwise these urls clog
        # up the chat history because only the images are deleted and
        # combined into the animated gif
        return Reply('http://www.bom.gov.au/products/{}.loop.shtml'
                     .format(config.radar_locations.values[0]),
                     disable_web_page_preview=True)
    else:
        # echo image so it can be saved to dashboard
        return do_echo_image(imgurl, 'weather')


@config.weather_api_key.required()
def do_weather(location):
    weather = Weather(config.weather_api_key.value, location)
    return Reply(escape_markdown(
        '{forecast} Rainfall: {rain_amount}mm {rain_prob}%'
        .format(**weather.get_weather()._asdict())))


@config.weather_api_key.required()
def do_week_weather(location):
    weather = Weather(config.weather_api_key.value, location)

    forecast = weather.get_week_weather()

    if len(forecast) > 1:
        forecast.pop(0)

    return Reply('\n'.join(f'*{day.day}* Min {day.min} Max {day.max}\n'
                           f'{escape_markdown(day.forecast)}'
                           for day in forecast))


@config.weather_api_key.required()
def do_temperatures(location, save, _now=None):
    weather = Weather(config.weather_api_key.value, location)

    temps = weather.get_temperatures()

    if save == 'save':
        if _now is None:
            _now = datetime.now()

        with heater_db.init_db() as conn:
            place_id = conn.get_place_id('taylor-st')
            # TODO: figure out how to discard the right hour for the end
            # of daylight saving when there are 25 hours...
            for h, temp in enumerate(temps[:24]):
                ts = datetime(_now.year, _now.month, _now.day, h)
                conn.insert_forecast(_now, place_id, ts, temp)

    return Reply(' '.join(str(temp) for temp in temps))
