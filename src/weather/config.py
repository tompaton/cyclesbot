from config.var_types import ConfigParam, EnvVar

weather_api_key = EnvVar('WILLY_WEATHER')

radar_locations = ConfigParam(name='Radar locations (comma separated)')
rain_probability = ConfigParam(name='Rain probability cutoff to show radar',
                               default='5')
rain_radar_pixel_cutoff = ConfigParam(name='Rain pixel cutoff to show radar',
                                      default='1500')
