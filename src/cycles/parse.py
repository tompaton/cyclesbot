from pyparsing import (Forward, Word, alphas, Group, OneOrMore, oneOf,
                       Optional, Suppress, WordEnd, Combine)

from common.parse import permutationOf, keyword
from common.when import future_dow_keywords, get_future_date

Commands = Forward()
KeyWords = Forward()
When = Combine(oneOf(future_dow_keywords(), caseless=True)
               + WordEnd()).setName('weekday')
Name = Group(OneOrMore(Word(alphas), stopOn=KeyWords | When)).setName('name')
Message = OneOrMore(Group(Commands))

ListsKeyword = Optional(Suppress('/')) + oneOf('lists', caseless=True)
ListsCommand = ListsKeyword('command')

ListKeyword = Optional(Suppress('/')) + oneOf('list', caseless=True)
ListCommand = ListKeyword('command') + Name('name')

TodoKeyword = Optional(Suppress('/')) + oneOf('todo done', caseless=True)
Details = (Suppress(Optional(oneOf('with show', caseless=True)))
           + oneOf('details summary', caseless=True))
TodoCommand = (TodoKeyword('command')
               + permutationOf(Optional(When, default='today')('when'),
                               Optional(Details, default='summary')('show')))

CycleKeyword = (Optional(Suppress('/'))
                + oneOf('tick next skip', caseless=True))
CycleCommand = (CycleKeyword('command')
                + Name('name')
                + Optional(When, default='today')('when'))

ReminderKeyword = (Optional(Suppress('/'))
                   + oneOf('reminder', caseless=True))
ReminderCommand = (ReminderKeyword('command')
                   + Name('name'))
UrlCommand = (Group(keyword('cycles', action='url'))('command')
              + Group(keyword('ical login'))('show'))

KeyWords << (ListsKeyword | ListKeyword | TodoKeyword
             | CycleKeyword | ReminderKeyword)

Commands << (ListsCommand | ListCommand | TodoCommand
             | CycleCommand | ReminderCommand | UrlCommand)


def parse_msg(msg):
    for res in Message.parseString(msg):
        kwargs = {}
        if 'when' in res:
            kwargs['when'] = get_future_date(res['when'])
        if 'name' in res:
            kwargs['name'] = list(res['name'])
        if 'show' in res:
            kwargs['show'] = res['show'][0]
        yield res['command'][0], kwargs


HELP = """
*lists*
*list* _name_
*todo* [summary|details]
*done* [summary|details]
*tick* _name_ [today|weekday]
*skip* _name_ [today|weekday]
*next* _name_ [today|weekday]
*reminder* _name_
*cycles* [ical|login]
"""
