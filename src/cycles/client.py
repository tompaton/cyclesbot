from cycles_client import Cycles, CycleNotFound
from chat.msg_types import Reply
from . import config


_CYCLES = None


@config.cycles_token.required(False)
def init():
    global _CYCLES
    _CYCLES = Cycles(api_token=config.cycles_token.value)

    print('Logging into Cycles...')
    _CYCLES.login()
    print('Done.\n')


def do_lists():
    return 'lists:\n{}'.format(
        '\n'.join(['{title}: /list {slug}'.format(**l)
                   for l in _CYCLES.get_lists()]))


def do_list(name):
    return 'list:\n{}'.format(
        '\n'.join(['{title} - {list_title}: '
                   '{slug}/{list_slug}'
                   .format(**cycle)
                   for cycle in _CYCLES.get_list_cycles(name)]))


def do_todo(when, show):
    return 'To-do today:\n{}'.format(
        _show_cycles(_CYCLES.get_cycles_todo(when), show, True))


def do_done(when, show):
    return 'Ticked today:\n{}'.format(
        _show_cycles(_CYCLES.get_cycles_done(when), show))


def _show_cycles(cycles, show, length_label=False):
    if show == 'details':
        fmt = '{title} - {list_title}: {summary}'
    elif length_label:
        fmt = '{title}{length_label}{overdue_label}'
    else:  # show == 'summary'
        fmt = '{title}'
    return '\n'.join([fmt.format(**cycle) for cycle in cycles])


def do_tick(name, when):
    try:
        cycle, action = _CYCLES.tick_cycle(name, when)
    except CycleNotFound as ex:
        return str(ex)
    else:
        def _undo():
            cycle, action = _CYCLES.tick_cycle(name, when)
            return "Un-ticked {title} - {list_title}, {action}".format(
                action=action['action'], **cycle)

        return "Ticked {title} - {list_title}, {action}".format(
            action=action['action'], **cycle), _undo


def do_next(name, when):
    try:
        cycle, action = _CYCLES.schedule_cycle(name, when)
    except CycleNotFound as ex:
        return str(ex)
    else:
        def _undo():
            cycle, action = _CYCLES.schedule_cycle(name, when)
            return "Un-scheduled {title} - {list_title}, {action}".format(
                action=action['action'], **cycle)

        return "Scheduled {title} - {list_title}, {action}".format(
            action=action['action'], **cycle), _undo


do_skip = do_next


def do_reminder(name):
    try:
        cycle = _CYCLES.get_cycle(name)
    except CycleNotFound as ex:
        return str(ex)
    else:
        if not cycle['ticked_today']:
            return Reply("{title} due today".format(**cycle))
        else:
            return None


def do_url(show):
    return Reply(_CYCLES.get_ical_url() if show == 'ical'
                 else _CYCLES.get_login_url(),
                 parse_mode=None)
