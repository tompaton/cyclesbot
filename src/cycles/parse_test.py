import pytest
from flexmock import flexmock
from pyparsing import ParseException
from . import parse_msg

from . import parse


@pytest.mark.parametrize('msg,cmd,kwargs', [
    ('lists', 'lists', {}),
    ('/list house', 'list', {'name': ['house']}),
    ('/list one two', 'list', {'name': ['one', 'two']}),
    ('todo', 'todo', {'show': 'summary', 'when': 'today'}),
    ('todo summary', 'todo', {'show': 'summary', 'when': 'today'}),
    ('todo show details', 'todo', {'show': 'details', 'when': 'today'}),
    ('todo with details', 'todo', {'show': 'details', 'when': 'today'}),
    ('todo details', 'todo', {'show': 'details', 'when': 'today'}),
    ('done', 'done', {'show': 'summary', 'when': 'today'}),
    ('done summary', 'done', {'show': 'summary', 'when': 'today'}),
    ('done show details', 'done', {'show': 'details', 'when': 'today'}),
    ('done with details', 'done', {'show': 'details', 'when': 'today'}),
    ('done details', 'done', {'show': 'details', 'when': 'today'}),
    ('cycles ical', 'url', {'show': 'ical'}),
    ('cycles login', 'url', {'show': 'login'}),
])
def test_cycles(msg, cmd, kwargs):
    (flexmock(parse)
     .should_receive('get_future_date')
     .replace_with(lambda when: when))

    assert list(parse_msg(msg)) == [(cmd, kwargs)]


@pytest.mark.parametrize('msg,cmd,kwargs', [
    ('tick vacuum', 'tick', {'name': ['vacuum'], 'when': 'today'}),
    ('Tick vacuum', 'tick', {'name': ['vacuum'], 'when': 'today'}),
    # m - monday, f - friday
    ('Tick mop floors', 'tick', {'name': ['mop', 'floors'], 'when': 'today'}),
    ('Tick mop', 'tick', {'name': ['mop'], 'when': 'today'}),
    ('Tick floors', 'tick', {'name': ['floors'], 'when': 'today'}),
    # w - wednesday
    ('Tick water potplants',
     'tick', {'name': ['water', 'potplants'], 'when': 'today'}),
    ('next vacuum friday', 'next', {'name': ['vacuum'], 'when': 'friday'}),
    ('/tick camera photos',
     'tick', {'name': ['camera', 'photos'], 'when': 'today'}),
    ('/skip vacuum friday', 'skip', {'name': ['vacuum'], 'when': 'friday'}),
    ('reminder blue bag', 'reminder', {'name': ['blue', 'bag']}),
])
def test_cycle(msg, cmd, kwargs):
    (flexmock(parse)
     .should_receive('get_future_date')
     .replace_with(lambda when: when))

    assert list(parse_msg(msg)) == [(cmd, kwargs)]


@pytest.mark.parametrize('msg,results', [
    ('tick camera photos todo monday',
     [('tick', {'name': ['camera', 'photos'], 'when': 'today'}),
      ('todo', {'show': 'summary', 'when': 'monday'})]),
    (' /tick run  /tick cold shower ',
     [('tick', {'name': ['run'], 'when': 'today'}),
      ('tick', {'name': ['cold', 'shower'], 'when': 'today'})]),
    ('tick run tick cold shower',
     [('tick', {'name': ['run'], 'when': 'today'}),
      ('tick', {'name': ['cold', 'shower'], 'when': 'today'})]),
    ('tick lawn next lawn sunday todo',
     [('tick', {'name': ['lawn'], 'when': 'today'}),
      ('next', {'name': ['lawn'], 'when': 'sunday'}),
      ('todo', {'show': 'summary', 'when': 'today'})]),
    ('tick sheets tick towels',
     [('tick', {'name': ['sheets'], 'when': 'today'}),
      ('tick', {'name': ['towels'], 'when': 'today'})]),
])
def test_multiple(msg, results):
    (flexmock(parse)
     .should_receive('get_future_date')
     .replace_with(lambda when: when))

    assert list(parse_msg(msg)) == results


@pytest.mark.parametrize('msg', [
    'tick tomorrow',
])
def test_no_name(msg):
    with pytest.raises(ParseException):
        list(parse_msg(msg))
