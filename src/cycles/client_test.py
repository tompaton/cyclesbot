from datetime import datetime
import pytest
from flexmock import flexmock
from . import client


def test_init():
    client._CYCLES = None

    flexmock(client.config.cycles_token, get_value='secret-token')

    cycles = flexmock()

    (flexmock(client)
     .should_receive('Cycles')
     .with_args(api_token='secret-token')
     .and_return(cycles))

    (flexmock(cycles)
     .should_receive('login')
     .once())

    assert client.init() is None

    assert client._CYCLES == cycles


def test_init_disabled():
    client._CYCLES = None

    flexmock(client.config.cycles_token, get_value=None)

    assert not client.init()
    assert client._CYCLES is None


@pytest.fixture
def cycles():
    cycles = client.Cycles(api_token='secret-token')
    # TODO: restore in teardown
    client._CYCLES = cycles
    return cycles


def test_lists(cycles):
    (flexmock(cycles)
     .should_receive('get_lists')
     .with_args()
     .and_return([{'slug': 'one', 'title': 'List One'},
                  {'slug': 'two', 'title': 'List Two'}]))

    assert client.do_lists() == ('lists:\n'
                                 'List One: /list one\n'
                                 'List Two: /list two')


def test_list(cycles):
    (flexmock(cycles)
     .should_receive('get_list_cycles')
     .with_args(['list', 'one'])
     .and_return([{'slug': 'cycle-a',
                   'title': 'Cycle A',
                   'list_slug': 'list-one',
                   'list_title': 'List One'},
                  {'slug': 'cycle-b',
                   'title': 'Cycle B',
                   'list_slug': 'list-one',
                   'list_title': 'List One'}]))

    assert client.do_list(name=['list', 'one']) \
        == ('list:\n'
            'Cycle A - List One: cycle-a/list-one\n'
            'Cycle B - List One: cycle-b/list-one')


@pytest.fixture
def cycles_todo():
    return [
        {'title': 'Cycle A',
         'summary': 'Summary A',
         'list_title': 'List One',
         'length_label': '',
         'due_today': True,
         'ticked_today': False,
         'overdue_label': ''},
        {'title': 'Cycle B',
         'summary': 'Summary B',
         'list_title': 'List One',
         'length_label': '/7d',
         'due_today': True,
         'ticked_today': False,
         'overdue_label': ''},
        {'title': 'Cycle E',
         'summary': 'Summary E',
         'list_title': 'List Two',
         'length_label': '/3w',
         'due_today': True,
         'ticked_today': False,
         'overdue_label': ' Overdue'},
    ]


def test_todo_summary(cycles, cycles_todo):
    (flexmock(cycles)
     .should_receive('get_cycles_todo')
     .with_args('today')
     .and_return(cycles_todo))

    assert client.do_todo(when='today', show='summary') \
        == ('To-do today:\n'
            'Cycle A\n'
            'Cycle B/7d\n'
            'Cycle E/3w Overdue')


def test_todo_details(cycles, cycles_todo):
    (flexmock(cycles)
     .should_receive('get_cycles_todo')
     .with_args('today')
     .and_return(cycles_todo))

    assert client.do_todo(when='today', show='details') \
        == ('To-do today:\n'
            'Cycle A - List One: Summary A\n'
            'Cycle B - List One: Summary B\n'
            'Cycle E - List Two: Summary E')


def test_done(cycles):
    (flexmock(cycles)
     .should_receive('get_cycles_done')
     .with_args('today')
     .and_return([
         {'title': 'Cycle A'},
         {'title': 'Cycle B'},
     ]))

    assert client.do_done(when='today', show='summary') \
        == ('Ticked today:\n'
            'Cycle A\n'
            'Cycle B')


def test_tick(cycles):
    (flexmock(cycles)
     .should_receive('tick_cycle')
     .with_args(['one', 'two'], datetime(2017, 8, 30, 9, 0, 0))
     .and_return(({'slug': 'one-two',
                   'title': 'One Two',
                   'list_title': 'List A'},
                  {'action': 'Ticked...'})))

    reply = client.do_tick(name=['one', 'two'],
                           when=datetime(2017, 8, 30, 9, 0, 0))

    assert reply[0] == 'Ticked One Two - List A, Ticked...'

    # undo
    (flexmock(cycles)
     .should_receive('tick_cycle')
     .with_args(['one', 'two'], datetime(2017, 8, 30, 9, 0, 0))
     .and_return(({'slug': 'one-two',
                   'title': 'One Two',
                   'list_title': 'List A'},
                  {'action': 'Un-ticked...'})))

    assert reply[1]() == 'Un-ticked One Two - List A, Un-ticked...'


def test_tick_error(cycles):
    (flexmock(cycles)
     .should_receive('tick_cycle')
     .with_args(['one', 'two'], datetime(2017, 8, 30, 9, 0, 0))
     .and_raise(client.CycleNotFound, 'error'))

    reply = client.do_tick(name=['one', 'two'],
                           when=datetime(2017, 8, 30, 9, 0, 0))
    assert reply == 'error'


def test_next(cycles):
    (flexmock(cycles)
     .should_receive('schedule_cycle')
     .with_args(['one', 'two'], datetime(2017, 8, 30, 9, 0, 0))
     .and_return(({'slug': 'one-two',
                   'title': 'One Two',
                   'list_title': 'List A'},
                  {'action': 'Scheduled...'})))

    reply = client.do_next(name=['one', 'two'],
                           when=datetime(2017, 8, 30, 9, 0, 0))
    assert reply[0] == 'Scheduled One Two - List A, Scheduled...'

    # undo
    (flexmock(cycles)
     .should_receive('schedule_cycle')
     .with_args(['one', 'two'], datetime(2017, 8, 30, 9, 0, 0))
     .and_return(({'slug': 'one-two',
                   'title': 'One Two',
                   'list_title': 'List A'},
                  {'action': 'Un-scheduled...'})))

    assert reply[1]() == 'Un-scheduled One Two - List A, Un-scheduled...'


def test_next_error(cycles):
    (flexmock(cycles)
     .should_receive('schedule_cycle')
     .with_args(['one', 'two'], datetime(2017, 8, 30, 9, 0, 0))
     .and_raise(client.CycleNotFound, 'error'))

    reply = client.do_next(name=['one', 'two'],
                           when=datetime(2017, 8, 30, 9, 0, 0))
    assert reply == 'error'


def test_reminder_due(cycles):
    (flexmock(cycles)
     .should_receive('get_cycle')
     .with_args(['one', 'two'])
     .and_return({'title': 'Cycle One Two',
                  'ticked_today': False}))

    assert client.do_reminder(name=['one', 'two']).message \
        == 'Cycle One Two due today'


def test_reminder_not_due(cycles):
    (flexmock(cycles)
     .should_receive('get_cycle')
     .with_args(['one', 'two'])
     .and_return({'ticked_today': True}))

    assert client.do_reminder(name=['one', 'two']) is None


def test_reminder_error(cycles):
    (flexmock(cycles)
     .should_receive('get_cycle')
     .with_args(['one', 'two'])
     .and_raise(client.CycleNotFound, 'error'))

    assert client.do_reminder(name=['one', 'two']) == 'error'


def test_ical(cycles):
    assert client.do_url('ical').message \
        == 'https://cycles.tompaton.com/ical/secret-token'


def test_login(cycles):
    assert client.do_url('login').message \
        == 'https://cycles.tompaton.com/login/secret-token'
