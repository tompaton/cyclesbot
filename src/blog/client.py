import contextlib
from datetime import datetime, timedelta
from pathlib import Path
import blog_generate
from common import webpage, git, util
from chat.msg_types import Reply, Question, Questions
from . import config


def do_generate():
    with util.workdir('/blog/generate'):
        return Reply(
            blog_generate.generate_blog(
                '/blog/content/posts.json',
                # TODO: parameterise these somehow
                '../../tompaton-com-blog-content/content/',
                '../../tompaton-com-blog-cache/',
                quiet=True)
        ).then(do_status(brief=True))


def do_release(check):
    with util.workdir('/blog/generate'):
        if check:
            reply = blog_generate.release_blog()
            if reply is not None:
                return Reply(reply)
            else:
                return None
        else:
            return Reply(blog_generate.release_blog(force=True))


def do_rollback():
    with util.workdir('/blog/generate'):
        return Reply(blog_generate.rollback_blog())


def do_status(brief=False):
    with util.workdir('/blog/generate'):
        folders = [('Next', 'next_release', 'blog-next'),
                   ('Current', 'released', 'blog'),
                   ('Previous', 'prev_release', 'blog-prev')]

        if brief:
            folders = folders[:1]

        releases \
            = '\n'.join('{name}: {date} {blog}{path}/'
                        .format(name=name,
                                date=_release(blog_generate.release_date(folder)),
                                blog=config.blog_url.value,
                                path=path)
                        for name, folder, path in folders)
        return Reply(
            releases
            + _auto_release(blog_generate.release_date('next_release')),
            disable_web_page_preview=True)


def _release(date):
    if date:
        return '{:%b %d %H:%M}'.format(date)
    else:
        return 'None'


def _auto_release(date):
    if date:
        now = _now()
        for cron in _cron_times(now):
            delta = cron - date
            if delta.days > 0 or delta.seconds >= 60*60:
                return '\n\nAuto-releasing at *{:%H:%M}* ({} minutes)' \
                    .format(cron, (cron - now).seconds // 60)

    return ''


def _now():  # pragma: no cover
    return datetime.now()


def _cron_times(now):
    cron = now.replace(minute=15 * (now.minute // 15),
                       second=0, microsecond=0)
    for d in [15, 30, 45, 60, 75]:
        yield cron + timedelta(seconds=d*60)


# TODO: move this into blog_generate.release
def do_diff():
    if not _exists(Path('/blog/generate') / 'next_release'):
        return 'No next_release'

    with util.workdir('/blog/generate'):
        return '\n'.join(list(blog_generate.diff_posts())
                         + list(blog_generate.diff_files())) \
                   .replace('/blog/', '/blog-next/') or 'No differences'


def _exists(path):  # pragma: no cover
    return path.exists()


# TODO: use post url, not integer, as id so it won't be messed up by new posts
# and will make for more useful commit messages.
# TODO: timeout current post
# * current post id for editing
_context = {}


def is_editing_post():
    return 'post_id' in _context


def _set_current_post(post_id):
    _context['post_id'] = post_id


def _del_current_post():
    _context.pop('post_id', None)


def pull_content():
    git.Repo('/blog/content').pull()


@contextlib.contextmanager
def commit_content(message):
    repo = git.Repo('/blog/content')
    with repo.commit(message) as repo:
        yield repo
        print(repo.add('posts.json'))


@contextlib.contextmanager
def commit_cache(message):
    repo = git.Repo('/blog/cache')
    with repo.commit(message):
        yield repo
        print(repo.add('--all'))


def do_bitbucket():
    return ('https://bitbucket.org/tompaton/tompaton-com-blog-content'
            '/src/HEAD/posts.json?at=master&fileviewer=file-view-default')


def do_posts(page):
    pull_content()
    return Reply(blog_generate.list_posts('/blog/content/posts.json',
                                          page=page),
                 parse_mode=None)


def do_edit(index):
    _set_current_post(index)
    pull_content()
    return Reply(blog_generate.show_post('/blog/content/posts.json', index),
                 parse_mode=None)


def _edit_post(action, message, **kwargs):
    post_id = _context['post_id']
    with commit_content('{}[{}] {}'.format(action, post_id, message)):
        return Reply(blog_generate.edit_post('/blog/content/posts.json',
                                             post_id, **kwargs),
                     parse_mode=None)


def do_edit_title(title):
    return _edit_post('Edit title', title, title=title)


def do_edit_external_url(url):
    return _edit_post('Edit external url', url, external_url=url)


def do_edit_tag(tags):
    return _edit_post('Added tags', ' '.join(tags), tag=tags)


def do_edit_attachment(url):
    return _edit_post('Added attachment', url, attachment=url)


def do_edit_via(title, url):
    return _edit_post('Added via', url, via=(title, url))


def do_cache(index, url='external_url'):
    _set_current_post(index)
    with commit_cache('fetched ' + url):
        with commit_content('Cached[{}] {}'
                            .format(index, 'external_url')):
            reply = Reply(blog_generate.cache_post('/blog/content/posts.json',
                                                   index, '/blog/cache/',
                                                   ignore_errors=True),
                          parse_mode=None)
    return reply.replace(' cached/',
                         ' {}blog/cached/'.format(config.blog_url.value))


def do_bookmark(url):
    def _done(title, comment):
        with commit_content('Add bookmark: {}'.format(url)):
            blog_generate.add_bookmark('/blog/content/posts.json',
                                       title, url, content_text=comment)
            _set_current_post(0)

            return _cache_followup('Added bookmark.', url)

    page_title, page_description = webpage.summary(url)

    return (Questions()
            .add('title', 'Title?', default=page_title)
            .add('comment', 'Comment?', default=page_description,
                 required=False, parse_mode=None)
            .done(_done))


def do_podcast(url):
    def _done(title, attachment, comment):
        with commit_content('Add podcast: {}'.format(url)):
            blog_generate.add_podcast('/blog/content/posts.json', title, url,
                                      attachment, content_text=comment)
            _set_current_post(0)

            return _cache_followup('Added podcast.', url)

    page_title, page_description = webpage.summary(url)

    return (Questions()
            .add('title', 'Title?', default=page_title)
            .add('attachment', 'Attachment url?')
            .add('comment', 'Comment?', default=page_description,
                 required=False, parse_mode=None)
            .done(_done))


def _cache_followup(message, external_url):
    def _cache(text):
        if text.lower() in ('y', 'yes'):
            return do_cache(0, external_url)

    return Reply(message).then(Question('Cache external url?', _cache))


def can_process_photo():
    return is_editing_post()


def process_photo(tmp):
    post_id = _context['post_id']
    with commit_content('{}[{}]'.format('Added image', post_id)) as repo:
        result = blog_generate.edit_image('/blog/content/posts.json', post_id,
                                          tmp, '/blog/content/')
        # TODO: new edit_post_image method to handle this properly
        for image in result:
            repo.add(image)
        return Reply('Added image: {}'.format(', '.join(result)),
                     parse_mode=None)
