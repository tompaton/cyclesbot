import pytest
import contextlib
from datetime import datetime
from flexmock import flexmock
from common import webpage
from pathlib import Path
from . import client


@contextlib.contextmanager
def noop(*args, **kwargs):
    yield


@pytest.fixture
def bookmark():
    (flexmock(client)
     .should_receive('commit_content')
     .replace_with(noop))

    client._del_current_post()


@pytest.fixture
def podcast():
    (flexmock(client)
     .should_receive('commit_content')
     .replace_with(noop))


@pytest.fixture
def url():
    return 'https://tompaton.com/blog/'


@pytest.fixture
def mp3():
    return 'https://tompaton.com/blog/podcast.mp3'


def test_abort_bookmark(bookmark, url):
    (flexmock(webpage)
     .should_receive('summary')
     .with_args(url)
     .and_return('title', 'desc'))

    r1 = client.do_bookmark(url)
    assert r1.message == r'Title? \[Y] "title"'

    r2 = r1.callback('n')
    assert r2.message == 'Aborted.'
    assert r2.callback is None


def test_accept_bookmark(bookmark, url):
    (flexmock(webpage)
     .should_receive('summary')
     .with_args(url)
     .and_return('title', 'desc'))

    assert client._context.get('post_id') is None

    r1 = client.do_bookmark(url)
    assert r1.message == r'Title? \[Y] "title"'

    r2 = r1.callback('y')
    assert r2.message == 'Comment? [Y] "desc"'

    (flexmock(client.blog_generate)
     .should_receive('add_bookmark')
     .with_args('/blog/content/posts.json', 'title', url, content_text='desc')
     .and_return(None))

    (flexmock(client)
     .should_receive('_cache_followup')
     .with_args('Added bookmark.', url)
     .and_return('followup'))

    r3 = r2.callback('y')

    assert r3 == 'followup'

    assert client._context.get('post_id') == 0


def test_empty_comment_bookmark(bookmark, url):
    (flexmock(webpage)
     .should_receive('summary')
     .with_args(url)
     .and_return('title', 'desc'))

    r1 = client.do_bookmark(url)
    assert r1.message == r'Title? \[Y] "title"'

    r2 = r1.callback('y')
    assert r2.message == 'Comment? [Y] "desc"'

    (flexmock(client.blog_generate)
     .should_receive('add_bookmark')
     .with_args('/blog/content/posts.json', 'title', url, content_text=None)
     .and_return(None))

    (flexmock(client)
     .should_receive('_cache_followup')
     .with_args('Added bookmark.', url)
     .and_return('followup'))

    r3 = r2.callback('n')

    assert r3 == 'followup'


def test_override_bookmark(bookmark, url):
    (flexmock(webpage)
     .should_receive('summary')
     .with_args(url)
     .and_return('title', 'desc'))

    r1 = client.do_bookmark(url)
    assert r1.message == r'Title? \[Y] "title"'

    r2 = r1.callback('new title')
    assert r2.message == 'Comment? [Y] "desc"'

    (flexmock(client.blog_generate)
     .should_receive('add_bookmark')
     .with_args('/blog/content/posts.json', 'new title', url,
                content_text='new desc')
     .and_return(None))

    (flexmock(client)
     .should_receive('_cache_followup')
     .with_args('Added bookmark.', url)
     .and_return('followup'))

    r3 = r2.callback('new desc')

    assert r3 == 'followup'


def test_abort_podcast(podcast, url):
    (flexmock(webpage)
     .should_receive('summary')
     .with_args(url)
     .and_return('title', 'desc'))

    r1 = client.do_podcast(url)
    assert r1.message == r'Title? \[Y] "title"'

    r2 = r1.callback('n')
    assert r2.message == 'Aborted.'
    assert r2.callback is None


def test_abort_podcast2(podcast, url):
    (flexmock(webpage)
     .should_receive('summary')
     .with_args(url)
     .and_return('title', 'desc'))

    r1 = client.do_podcast(url)
    assert r1.message == r'Title? \[Y] "title"'

    r2 = r1.callback('y')
    assert r2.message == 'Attachment url?'

    r3 = r2.callback('n')
    assert r3.message == 'Aborted.'
    assert r3.callback is None


def test_accept_podcast(podcast, url, mp3):
    (flexmock(webpage)
     .should_receive('summary')
     .with_args(url)
     .and_return('title', 'desc'))

    r1 = client.do_podcast(url)
    assert r1.message == r'Title? \[Y] "title"'

    r2 = r1.callback('y')
    assert r2.message == 'Attachment url?'

    r3 = r2.callback(mp3)
    assert r3.message == 'Comment? [Y] "desc"'

    (flexmock(client.blog_generate)
     .should_receive('add_podcast')
     .with_args('/blog/content/posts.json', 'title', url, mp3,
                content_text='desc')
     .and_return(None))

    (flexmock(client)
     .should_receive('_cache_followup')
     .with_args('Added podcast.', url)
     .and_return('followup'))

    r4 = r3.callback('y')

    assert r4 == 'followup'


def test_empty_comment_podcast(podcast, url, mp3):
    (flexmock(webpage)
     .should_receive('summary')
     .with_args(url)
     .and_return('title', 'desc'))

    r1 = client.do_podcast(url)
    assert r1.message == r'Title? \[Y] "title"'

    r2 = r1.callback('y')
    assert r2.message == 'Attachment url?'

    r3 = r2.callback(mp3)
    assert r3.message == 'Comment? [Y] "desc"'

    (flexmock(client.blog_generate)
     .should_receive('add_podcast')
     .with_args('/blog/content/posts.json', 'title', url, mp3,
                content_text=None)
     .and_return(None))

    (flexmock(client)
     .should_receive('_cache_followup')
     .with_args('Added podcast.', url)
     .and_return('followup'))

    r4 = r3.callback('n')

    assert r4 == 'followup'


def test_override_podcast(podcast, url, mp3):
    (flexmock(webpage)
     .should_receive('summary')
     .with_args(url)
     .and_return('title', 'desc'))

    r1 = client.do_podcast(url)
    assert r1.message == r'Title? \[Y] "title"'

    r2 = r1.callback('new title')
    assert r2.message == 'Attachment url?'

    r3 = r2.callback(mp3)
    assert r3.message == 'Comment? [Y] "desc"'

    (flexmock(client.blog_generate)
     .should_receive('add_podcast')
     .with_args('/blog/content/posts.json', 'new title', url, mp3,
                content_text='new desc')
     .and_return(None))

    (flexmock(client)
     .should_receive('_cache_followup')
     .with_args('Added podcast.', url)
     .and_return('followup'))

    r4 = r3.callback('new desc')

    assert r4 == 'followup'


def test_cache_followup_n(podcast):
    reply1 = client._cache_followup('message', 'url')

    assert reply1.replies[0].message == 'message'
    assert reply1.replies[1].message == 'Cache external url?'

    reply2 = reply1.replies[1].callback('n')

    assert reply2 is None


def test_cache_followup_y(podcast):
    reply1 = client._cache_followup('message', 'url')

    assert reply1.replies[0].message == 'message'
    assert reply1.replies[1].message == 'Cache external url?'

    (flexmock(client)
     .should_receive('do_cache')
     .with_args(0, 'url')
     .and_return('Cached...'))

    reply2 = reply1.replies[1].callback('Yes')

    assert reply2 == 'Cached...'


def test_manage_status():
    flexmock(client.config.blog_url, get_value='https://tompaton.com/')

    (flexmock(client.blog_generate)
     .should_receive('release_date')
     .with_args('next_release')
     .and_return(None))

    (flexmock(client.blog_generate)
     .should_receive('release_date')
     .with_args('released')
     .and_return(datetime(2017, 8, 30, 9, 0, 0)))

    (flexmock(client.blog_generate)
     .should_receive('release_date')
     .with_args('prev_release')
     .and_return(datetime(2017, 8, 3, 9, 0, 0)))

    assert client.do_status().message \
        == ('Next: None https://tompaton.com/blog-next/\n'
            'Current: Aug 30 09:00 https://tompaton.com/blog/\n'
            'Previous: Aug 03 09:00 https://tompaton.com/blog-prev/')


def test_brief_status():
    flexmock(client.config.blog_url, get_value='https://tompaton.com/')

    (flexmock(client.blog_generate)
     .should_receive('release_date')
     .with_args('next_release')
     .and_return(None))

    assert client.do_status(brief=True).message \
        == 'Next: None https://tompaton.com/blog-next/'


def test_manage_status_auto():
    flexmock(client.config.blog_url, get_value='https://tompaton.com/')

    (flexmock(client)
     .should_receive('_now')
     .and_return(datetime(2017, 8, 30, 19, 11, 0)))

    (flexmock(client.blog_generate)
     .should_receive('release_date')
     .with_args('next_release')
     .and_return(datetime(2017, 8, 30, 19, 1, 0)))

    (flexmock(client.blog_generate)
     .should_receive('release_date')
     .with_args('released')
     .and_return(datetime(2017, 8, 30, 9, 0, 0)))

    (flexmock(client.blog_generate)
     .should_receive('release_date')
     .with_args('prev_release')
     .and_return(datetime(2017, 8, 3, 9, 0, 0)))

    assert client.do_status().message \
        == ('Next: Aug 30 19:01 https://tompaton.com/blog-next/\n'
            'Current: Aug 30 09:00 https://tompaton.com/blog/\n'
            'Previous: Aug 03 09:00 https://tompaton.com/blog-prev/\n\n'
            'Auto-releasing at *20:15* (64 minutes)')


def test_cron_times():
    now = datetime(2017, 8, 30, 21, 20, 0)
    assert list(client._cron_times(now)) \
        == [datetime(2017, 8, 30, 21, 30),
            datetime(2017, 8, 30, 21, 45),
            datetime(2017, 8, 30, 22, 0),
            datetime(2017, 8, 30, 22, 15),
            datetime(2017, 8, 30, 22, 30)]


def test_auto_release():
    flexmock(client).should_receive('_cron_times').and_return([])
    assert client._auto_release(1) == ''


def test_manage_posts():
    (flexmock(client)
     .should_receive('pull_content')
     .and_return(None))

    (flexmock(client.blog_generate)
     .should_receive('list_posts')
     .with_args('/blog/content/posts.json', page=0)
     .and_return('1. all\n2. the\n3. posts\n'))

    assert client.do_posts(0).message == '1. all\n2. the\n3. posts\n'


def test_manage_diff_no_next_release():
    (flexmock(client)
     .should_receive('_exists')
     .with_args(Path('/blog/generate/next_release'))
     .and_return(False))

    assert client.do_diff() == 'No next_release'


def test_manage_diff_no_differences():
    (flexmock(client)
     .should_receive('_exists')
     .with_args(Path('/blog/generate/next_release'))
     .and_return(True))

    (flexmock(client.blog_generate)
     .should_receive('diff_posts')
     .and_return([]))

    (flexmock(client.blog_generate)
     .should_receive('diff_files')
     .and_return([]))

    assert client.do_diff() == 'No differences'


def test_manage_diff_files():
    (flexmock(client)
     .should_receive('_exists')
     .with_args(Path('/blog/generate/next_release'))
     .and_return(True))

    (flexmock(client.blog_generate)
     .should_receive('diff_posts')
     .and_return([]))

    (flexmock(client.blog_generate)
     .should_receive('diff_files')
     .and_return(['New files:', '2017/08/30/bookmark-c.html',
                  'DELETED files:', '2017/08/30/bookmark-a.html']))

    assert client.do_diff() \
        == ('New files:\n'
            '2017/08/30/bookmark-c.html\n'
            'DELETED files:\n'
            '2017/08/30/bookmark-a.html')


def test_manage_diff_posts():
    (flexmock(client)
     .should_receive('_exists')
     .with_args(Path('/blog/generate/next_release'))
     .and_return(True))

    (flexmock(client.blog_generate)
     .should_receive('diff_posts')
     .and_return(['New posts:', 'id-d',
                  'Edited posts:', 'id-c',
                  'DELETED posts:', 'id-a']))

    (flexmock(client.blog_generate)
     .should_receive('diff_files')
     .and_return([]))

    assert client.do_diff() \
        == ('New posts:\n'
            'id-d\n'
            'Edited posts:\n'
            'id-c\n'
            'DELETED posts:\n'
            'id-a')


def test_manage_release():
    (flexmock(client.blog_generate)
     .should_receive('release_blog')
     .with_args(force=True)
     .and_return('released')
     .once())

    assert client.do_release(False).message == 'released'


def test_manage_release_check():
    (flexmock(client.blog_generate)
     .should_receive('release_blog')
     .with_args()
     .and_return('released')
     .once())

    assert client.do_release(True).message == 'released'


def test_manage_release_check_none():
    (flexmock(client.blog_generate)
     .should_receive('release_blog')
     .with_args()
     .and_return(None)
     .once())

    assert client.do_release(True) is None


def test_manage_rollback():
    (flexmock(client.blog_generate)
     .should_receive('rollback_blog')
     .with_args()
     .and_return('rolled back')
     .once())

    assert client.do_rollback().message == 'rolled back'


def test_manage_generate():
    (flexmock(client.blog_generate)
     .should_receive('generate_blog')
     .with_args('/blog/content/posts.json',
                '../../tompaton-com-blog-content/content/',
                '../../tompaton-com-blog-cache/',
                quiet=True)
     .and_return('generated')
     .once())

    (flexmock(client)
     .should_receive('do_status')
     .with_args(brief=True)
     .and_return('status')
     .once())

    result = client.do_generate()
    assert result._messages == ['generated', 'status']


def test_pull_content():
    repo = flexmock()

    (flexmock(client.git)
     .should_receive('Repo')
     .with_args('/blog/content')
     .and_return(repo))

    flexmock(repo).should_receive('pull')

    client.pull_content()


def test_commit_content():
    repo = flexmock()

    (flexmock(client.git)
     .should_receive('Repo')
     .with_args('/blog/content')
     .and_return(repo))

    (flexmock(repo)
     .should_receive('commit')
     .with_args('message')
     .and_return(repo))

    (flexmock(repo)
     .should_receive('add')
     .with_args('posts.json')
     .and_return('added posts.json'))

    with client.commit_content('message') as ctx:
        assert ctx == repo


def test_commit_cache():
    repo = flexmock()

    (flexmock(client.git)
     .should_receive('Repo')
     .with_args('/blog/cache')
     .and_return(repo))

    (flexmock(repo)
     .should_receive('commit')
     .with_args('message')
     .and_return(repo))

    (flexmock(repo)
     .should_receive('add')
     .with_args('--all')
     .and_return('added all'))

    with client.commit_cache('message') as ctx:
        assert ctx == repo


def test_bitbucket():
    assert client.do_bitbucket() \
        == ('https://bitbucket.org/tompaton/tompaton-com-blog-content'
            '/src/HEAD/posts.json?at=master&fileviewer=file-view-default')


def test_cache():
    flexmock(client.config.blog_url, get_value='https://tp.com/')

    (flexmock(client)
     .should_receive('commit_cache')
     .with_args('fetched external_url')
     .replace_with(noop))

    (flexmock(client)
     .should_receive('commit_content')
     .with_args('Cached[123] external_url')
     .replace_with(noop))

    (flexmock(client.blog_generate)
     .should_receive('cache_post')
     .with_args('/blog/content/posts.json', 123, '/blog/cache/',
                ignore_errors=True)
     .and_return('[0] Saved external_url to cached/external_url.html'))

    assert client.do_cache(123).message \
        == ('[0] Saved external_url to '
            'https://tp.com/blog/cached/external_url.html')
    assert client._context.get('post_id') == 123


def test_edit():
    (flexmock(client)
     .should_receive('pull_content')
     .and_return(None))

    (flexmock(client.blog_generate)
     .should_receive('show_post')
     .with_args('/blog/content/posts.json', 1)
     .and_return('{"id": "id-a"}'))

    assert client.do_edit(1).message == '{"id": "id-a"}'

    assert client._context.get('post_id') == 1


def test_edit_title():
    client._set_current_post(1)

    (flexmock(client)
     .should_receive('commit_content')
     .with_args('Edit title[1] new title')
     .replace_with(noop))

    (flexmock(client.blog_generate)
     .should_receive('edit_post')
     .with_args('/blog/content/posts.json', 1, title='new title')
     .and_return('Updated 1: title --> new title'))

    assert client.do_edit_title('new title').message \
        == 'Updated 1: title --> new title'


def test_edit_external_url():
    (flexmock(client)
     .should_receive('_edit_post')
     .with_args('Edit external url', 'url', external_url='url')
     .and_return('Updated...'))

    assert client.do_edit_external_url('url') == 'Updated...'


def test_edit_attachment():
    (flexmock(client)
     .should_receive('_edit_post')
     .with_args('Added attachment', 'url', attachment='url')
     .and_return('Updated...'))

    assert client.do_edit_attachment('url') == 'Updated...'


def test_edit_via():
    (flexmock(client)
     .should_receive('_edit_post')
     .with_args('Added via', 'url', via=('title', 'url'))
     .and_return('Updated...'))

    assert client.do_edit_via('title', 'url') == 'Updated...'


def test_edit_tag():
    (flexmock(client)
     .should_receive('_edit_post')
     .with_args('Added tags', 'one two', tag=['one', 'two'])
     .and_return('Updated...'))

    assert client.do_edit_tag(['one', 'two']) == 'Updated...'


def test_can_process_photo_true():
    (flexmock(client)
     .should_receive('is_editing_post')
     .and_return(True))
    assert client.can_process_photo()


def test_can_process_photo_false():
    (flexmock(client)
     .should_receive('is_editing_post')
     .and_return(False))
    assert not client.can_process_photo()


def test_process_photo():
    client._set_current_post(1)

    repo = flexmock()
    tmp = flexmock(name='/tmp/1234')

    (flexmock(client)
     .should_receive('commit_content')
     .with_args('Added image[1]')
     .and_return(repo))

    (flexmock(client.blog_generate)
     .should_receive('edit_image')
     .with_args('/blog/content/posts.json', 1, tmp, '/blog/content/')
     .and_return(['content/image.jpeg',
                  'content/thumb.jpeg']))

    flexmock(repo).should_receive('add').with_args('content/image.jpeg')

    flexmock(repo).should_receive('add').with_args('content/thumb.jpeg')

    assert client.process_photo(tmp).message == (
        'Added image: '
        'content/image.jpeg, '
        'content/thumb.jpeg')
