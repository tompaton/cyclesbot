import pytest
from pyparsing import ParseException
from . import parse_msg
from . import client


@pytest.mark.parametrize('msg,cmd,kwargs', [
    ('generate blog', 'generate', {}),
    ('release blog', 'release', {'check': ''}),
    ('release blog check', 'release', {'check': 'check'}),
    ('rollback blog', 'rollback', {}),
    ('blog diff', 'diff', {}),
    ('blog status', 'status', {}),
    ('blog posts', 'posts', {'page': 0}),
    ('blog posts 1', 'posts', {'page': 1}),
    ('edit posts.json', 'bitbucket', {}),
    ('edit post', 'edit', {'index': 0}),
    ('edit post 1', 'edit', {'index': 1}),
    ('cache post', 'cache', {'index': 0}),
    ('cache post 1', 'cache', {'index': 1}),
    ('bookmark http://example.com/url',
     'bookmark', {'url': 'http://example.com/url'}),
    ('podcast http://example.com/url',
     'podcast', {'url': 'http://example.com/url'}),
])
def test_blog(msg, cmd, kwargs):
    assert list(parse_msg(msg)) == [(cmd, kwargs)]


def test_not_editing_post():
    client._del_current_post()

    with pytest.raises(ParseException):
        list(parse_msg('title "new post title"'))


@pytest.mark.parametrize('msg,cmd,kwargs', [
    ('title "new post title"', 'edit_title', {'title': 'new post title'}),
    ('external url http://example.com/url',
     'edit_external_url', {'url': 'http://example.com/url'}),
    ('tag code', 'edit_tag', {'tags': ['code']}),
    ('tag cory-doctorow', 'edit_tag', {'tags': ['cory-doctorow']}),
    ('tag @multipolygon cory.doctorow',
     'edit_tag', {'tags': ['@multipolygon', 'cory.doctorow']}),
    ('attach http://example.com/url',
     'edit_attachment', {'url': 'http://example.com/url'}),
    ('via http://example.com/url source name',
     'edit_via', {'title': 'source name', 'url': 'http://example.com/url'}),
    ('via http://example.com/url "source name"',
     'edit_via', {'title': 'source name', 'url': 'http://example.com/url'}),
])
def test_edit_post(msg, cmd, kwargs):
    client._set_current_post(True)

    assert list(parse_msg(msg)) == [(cmd, kwargs)]
