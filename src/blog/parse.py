from pyparsing import Group, OneOrMore, Optional, Word, alphanums
from pyparsing import pyparsing_common

from common.parse import keyword, Url, QuotedString

from . import client

Page = pyparsing_common.integer
PostIndex = pyparsing_common.integer
Title = QuotedString().setName('title')
Tag = Word(alphanums + '@.-').setName('tag')

BlogStatus = Group(keyword('blog', 'status', action='status')('action'))
BlogDiff = Group(keyword('blog', 'diff', action='diff')('action'))
BlogPosts = Group(keyword('blog', 'posts', action='posts')('action')
                  + Optional(Page, default=0)('page'))
GenerateBlog = Group(keyword('generate', 'blog', action='generate')('action'))
ReleaseBlog = Group(keyword('release', 'blog', action='release')('action')
                    + Optional(keyword('check'), default='')('check'))
RollbackBlog = Group(keyword('rollback', 'blog', action='rollback')('action'))

ManageBlog = (GenerateBlog | ReleaseBlog | RollbackBlog
              | BlogStatus | BlogDiff | BlogPosts)

EditPostsJSON = Group(keyword('edit', 'posts.json',
                              action='bitbucket')('action'))

EditPost = Group(keyword('edit', 'post', action='edit')('action')
                 + Optional(PostIndex, default=0)('index'))

CachePost = Group(keyword('cache', 'post', action='cache')('action')
                  + Optional(PostIndex, default=0)('index'))

AddBookmark = Group(keyword('bookmark')('action')
                    + Url('url'))

AddPodcast = Group(keyword('podcast')('action')
                   + Url('url'))

Message = (ManageBlog | EditPostsJSON | EditPost | CachePost
           | AddBookmark | AddPodcast)

EditTitle = Group(keyword('title', action='edit_title')('action')
                  + Title('title'))

EditExternalUrl = Group(keyword('external', 'url',
                                action='edit_external_url')('action')
                        + Url('url'))

EditTag = Group(keyword('tag', action='edit_tag')('action')
                + OneOrMore(Tag)('tags'))

EditAttachment = Group(keyword('attach attachment',
                               action='edit_attachment')('action')
                       + Url('url'))

EditVia = Group(keyword('via', action='edit_via')('action')
                + Url('url')
                + Title('title'))

EditMessage = EditTitle | EditExternalUrl | EditTag | EditAttachment | EditVia


def parse_msg(msg):
    if client.is_editing_post():
        valid = EditMessage | Message
    else:
        valid = Message

    for res in valid.parseString(msg, parseAll=True):
        kwargs = {key: res[key] for key in ('index', 'url', 'title', 'page',
                                            'check')
                  if key in res}
        if 'tags' in res:
            kwargs['tags'] = res['tags'].asList()
        yield res['action'], kwargs


HELP = """
*{generate|rollback} blog*
*release blog* [check]
*blog {diff|status}*
*blog posts* [_page_]
*edit posts.json*
*bookmark* _url_
*podcast* _url_
*edit post* [_index_]
*cache post* [_index_]

*title* _title_
*external url* _url_
*tag* _tags_
*attach* _url_
*via* _url_ _title_
"""
