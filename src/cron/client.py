import re
import schedule
import sqlite3
import time

from collections import namedtuple
from datetime import datetime

from chat.modules import run_command
from chat import modules
from chat.msg_types import Reply
from common.util import escape_markdown as md


def get_conn():  # pragma: no cover
    return sqlite3.connect('/data/cron.db')


def init_db():  # pragma: no cover
    get_conn().execute("""CREATE TABLE IF NOT EXISTS
                          cron (time text,
                                days text,
                                command text,
                                "then" text,
                                "created" integer,
                                "deleted" integer,
                                "until" text,
                                "every" text)""")
    for time_, days, command, then in [
            ('07:45', ['monday', 'thursday'], 'reminder Blue bag', ''),
            ('07:02', [], 'weather', ''),
            ('07:30', [], 'radar check', ''),
            ('15:00', [], 'lunch reminder', ''),
            ('16:30', [], 'radar check', '')]:
        print(do_add(time_, days, command, then)[0])


def migrate_db_1():  # pragma: no cover
    get_conn().execute('ALTER TABLE cron ADD COLUMN "then" text')
    with get_conn() as conn:
        conn.execute("""UPDATE cron SET "then"='' """)


def migrate_db_2():  # pragma: no cover
    get_conn().execute('ALTER TABLE cron ADD COLUMN "created" integer')
    get_conn().execute('ALTER TABLE cron ADD COLUMN "deleted" integer')
    with get_conn() as conn:
        conn.execute("UPDATE cron SET created=datetime('now')")


def migrate_db_3():  # pragma: no cover
    get_conn().execute('ALTER TABLE cron ADD COLUMN "until" text')
    get_conn().execute('ALTER TABLE cron ADD COLUMN "every" text')


def do_list():
    return Reply('\n'.join(f'{row.rowid}. {row.entry()}'
                           for row in fetch_cron()),
                 disable_web_page_preview=True)


WEEKDAYS = ['monday', 'tuesday', 'wednesday', 'thursday', 'friday']
WEEKDAYS_STR = ' '.join(WEEKDAYS)
WEEKEND = ['saturday', 'sunday']
WEEKEND_STR = ' '.join(WEEKEND)
DAILY = WEEKDAYS + WEEKEND
DAILY_STR = ' '.join(DAILY)


class CronRow(namedtuple('CronRow',
                         'rowid time days command then created deleted '
                         'until every')):

    def get_days(self):
        return self.days.split() or WEEKDAYS

    def get_times(self):
        if self.every and self.until:
            match = re.fullmatch(r'\s*(\d+)\s*mins\s*', self.every)

            assert match is not None, 'every must be: "xx mins"'

            every = int(match.groups()[0])

            next_time = to_mins(self.time)
            until = to_mins(self.until)

            assert next_time <= until, 'until must be after start time'

            while next_time <= until:
                yield from_mins(next_time)
                next_time += every

        else:
            yield self.time

    def entry(self):
        if self.every and self.until:
            entry = f'every {self.every} between {self.time} and {self.until}'
        else:
            entry = self.time

        entry += f' _{self.entry_days()}_ {md(self.command)}'

        if self.then:
            entry += f' THEN {md(self.then)}'

        return entry

    def entry_days(self):
        if not self.days or self.days == WEEKDAYS_STR:
            return 'weekdays'

        if self.days == WEEKEND_STR:
            return 'weekend'

        if self.days == DAILY_STR:
            return 'daily'

        return self.days


def to_mins(hh_mm):
    match = re.fullmatch(r'(\d{2}):(\d{2})', hh_mm)

    assert match is not None, 'must be hh:mm format'

    hh, mm = map(int, match.groups())

    assert 0 <= hh <= 24, 'hh must be between 0 and 24'
    assert 0 <= mm <= 59, 'mm must be between 0 and 59'

    return hh * 60 + mm


def from_mins(mins):
    return '{0:02d}:{1:02d}'.format(mins // 60, mins % 60)


def fetch_cron(**kwargs):
    where = ['deleted IS NULL'] + [key + ' = ?' for key in kwargs]
    params = tuple(kwargs.values())

    cur = get_conn().cursor()
    cur.execute('SELECT rowid, time, days, command, "then", created, deleted, '
                '"until", "every" '
                'FROM cron WHERE {}'.format(' AND '.join(where)),
                params)
    return [CronRow(*row) for row in cur.fetchall()]


def do_add(time, days, command, then):
    with get_conn() as conn:
        rowid = _insert_row(conn.cursor(), time=time, days=' '.join(days),
                            command=command, then=then)

    def _undo():
        return do_delete(rowid)[0]

    schedule_jobs()

    return 'Scheduled {}'.format(CronRow(0, time, ' '.join(days),
                                         command, then, 0, 0,
                                         None, None).entry()), _undo


def do_delete(entry):
    row = fetch_cron(rowid=entry)[0]

    with get_conn() as conn:
        _delete_row(conn, entry)

    def _undo():
        with get_conn() as conn:
            _undelete_row(conn, entry)

        schedule_jobs()

        return 'Restored ' + row.entry()

    schedule_jobs()

    return f'Deleted {row.entry()}', _undo


def _insert_row(cur, **fields):
    cur.execute("INSERT INTO cron ({}, created) VALUES ({}, datetime('now'))"
                .format(', '.join(f'"{field}"' for field in fields),
                        ', '.join('?' for field in fields)),
                tuple(fields.values()))

    return cur.lastrowid


def _update_row(cur, rowid, **fields):
    cur.execute("UPDATE cron SET {} WHERE rowid=?"
                .format(', '.join(f'"{field}"=?' for field in fields)),
                tuple(fields.values()) + (rowid,))


def _delete_row(conn, rowid):
    conn.execute("UPDATE cron SET deleted=datetime('now') WHERE rowid = ?",
                 (rowid,))


def _undelete_row(conn, rowid):
    conn.execute("UPDATE cron SET deleted=NULL WHERE rowid = ?", (rowid,))


def do_once(time, command):
    with get_conn() as conn:
        rowid = _insert_row(conn.cursor(), time=time, days=DAILY_STR,
                            command=command, then='')
        _update_row(conn.cursor(), rowid, then=f'mute cron delete {rowid}')

    def _undo():
        return do_delete(rowid)[0]

    schedule_jobs()

    return f'Scheduled {md(command)} at {time}', _undo


def do_once_relative(delay, command, now=None):
    now = (now or datetime.now()).strftime('%H:%M')
    time = from_mins((to_mins(now) + to_mins(delay))
                     % (24 * 60))
    return do_once(time, command)


def do_repeat(entry, until, every):
    row = fetch_cron(rowid=entry)[0]

    if row.deleted:
        return f'Entry {entry} has been deleted!'

    repeated = CronRow(0, row.time, row.days, row.command, row.then, 0, 0,
                       until, every)

    with get_conn() as conn:
        _delete_row(conn, entry)

        rowid = _insert_row(conn.cursor(),
                            time=repeated.time, days=repeated.days,
                            command=repeated.command, then=repeated.then,
                            until=repeated.until, every=repeated.every)

    def _undo():
        with get_conn() as conn:
            _undelete_row(conn, entry)
            _delete_row(conn, rowid)

        schedule_jobs()

        return 'Restored ' + row.entry()

    schedule_jobs()

    return f'Repeat {repeated.entry()}', _undo


def do_run(entry):
    for row in fetch_cron():
        if row.rowid == entry:
            push_replies(row.command, row.then)
            return f'Finished "{md(row.command)}"'


def do_edit(entry, command):
    row = fetch_cron(rowid=entry)[0]

    if row.deleted:
        return f'Entry {entry} has been deleted!'

    edited = CronRow(0, row.time, row.days, command, row.then, 0, 0,
                     row.until, row.every)

    with get_conn() as conn:
        _delete_row(conn, entry)

        rowid = _insert_row(conn.cursor(),
                            time=edited.time, days=edited.days,
                            command=edited.command, then=edited.then,
                            until=edited.until, every=edited.every)

    def _undo():
        with get_conn() as conn:
            _undelete_row(conn, entry)
            _delete_row(conn, rowid)

        schedule_jobs()

        return 'Restored ' + row.entry()

    schedule_jobs()

    return f'Edited {edited.entry()}', _undo


def schedule_jobs():
    schedule.clear()

    for row in fetch_cron():
        # use list to make easier to test with_args
        for job in _schedule_jobs(list(row.get_days()), list(row.get_times())):
            job.do(push_replies, row.command, row.then)


def _schedule_jobs(days, times):
    for day in days:
        for time_ in times:
            yield getattr(schedule.every(), day.lower()).at(time_)


def push_replies(command, then):
    replied = False
    for reply in run_command(command)[1]:
        if reply.push():
            replied = True
            modules.set_callback(reply, interactive=False)

    if replied and then:
        push_replies(then, None)


def run_jobs():  # pragma: no cover
    schedule_jobs()

    while True:
        schedule.run_pending()
        time.sleep(10)
