from pyparsing import Optional, Suppress, Group, Regex, oneOf, OneOrMore
from pyparsing import pyparsing_common
from common.parse import keyword, Command2, CommandWithThen

ListCommand = keyword('cron', action='list')('action')

Time = Regex(r'\d?\d:\d\d').setName('hh:mm')
Every = Regex(r'\d+ mins').setName('nn mins')
Days = OneOrMore(oneOf('monday tuesday wednesday thursday friday '
                       'saturday sunday',
                       caseless=True))
DayGroup = oneOf('weekday weekdays weekend weekends daily')

AddCommand = (keyword('cron', 'add', action='add')('action')
               + Time('time')
               + (Days('days') | DayGroup('day_group'))
               + CommandWithThen)

Entry = pyparsing_common.integer
DeleteCommand = keyword('cron', 'delete', action='delete')('action') + Entry('entry')
RunCommand = keyword('cron', 'run', action='run')('action') + Entry('entry')
RepeatCommand = (keyword('cron', 'repeat', action='repeat')('action')
                 + Entry('entry')
                 + Suppress(keyword('every'))
                 + Every('every')
                 + Suppress(keyword('until'))
                 + Time('until'))
EditCommand = (keyword('cron', 'edit', action='edit')('action')
               + Entry('entry')
               + Command2('command'))

OnceCommand = (keyword('cron', 'at', action='once')('action')
               + Time('time')
               + Command2)

RelativeOnceCommand = (keyword('cron', 'in', action='once_relative')('action')
                       + Time('delay')
                       + Command2)

Message = Optional(Suppress('/')) + Group(AddCommand
                                          | OnceCommand | RelativeOnceCommand
                                          | DeleteCommand | RunCommand | EditCommand
                                          | RepeatCommand
                                          | ListCommand)

def parse_msg(msg):
    for res in Message.parseString(msg, parseAll=True):
        kwargs = {key: res[key] or ''
                  for key in ('command', 'entry', 'time', 'every', 'until',
                              'then', 'delay')
                  if key in res}
        if 'days' in res:
            kwargs['days'] = res['days'].asList()
        if 'day_group' in res:
            if res['day_group'].startswith('weekday'):
                kwargs['days'] = []
            elif res['day_group'].startswith('weekend'):
                kwargs['days'] = ['saturday', 'sunday']
            elif res['day_group'] == 'daily':
                kwargs['days'] = ['monday', 'tuesday', 'wednesday', 'thursday',
                                  'friday', 'saturday', 'sunday']
        yield res['action'], kwargs

HELP = """
*cron*
*cron add* _hh:mm_ _days_ _command_ [then _command_]
*cron repeat* _entry_ every _nn mins_ until _hh:mm_
*cron run* _entry_
*cron delete* _entry_
*cron edit* _entry_ _command_
*cron at* _hh:mm_ _command_
*cron in* _hh:mm_ _command_
"""
