import pytest
from datetime import datetime
from flexmock import flexmock
from unittest.mock import patch, call
from common.test import mock_contextmanager
from . import client


def test_list():
    cur = flexmock()
    (flexmock(cur)
     .should_receive('execute')
     .with_args('SELECT rowid, time, days, command, "then", '
                'created, deleted, "until", "every" '
                'FROM cron WHERE deleted IS NULL', ())
     .once())

    (flexmock(cur)
     .should_receive('fetchall')
     .and_return([[1, '09:00', '', 'radar check', '', 1234567, None,
                   '19:00', '30 mins'],
                  [2, '10:00', 'monday', 'reminder light_blue bag', 'weather',
                   1234567, None, None, None]]))

    conn = flexmock()
    flexmock(conn).should_receive('cursor').and_return(cur)

    (flexmock(client)
     .should_receive('get_conn')
     .and_return(conn))

    reply = client.do_list()
    assert reply.message \
        == ('1. every 30 mins between 09:00 and 19:00 _weekdays_ radar check\n'
            '2. 10:00 _monday_ reminder light\\_blue bag THEN weather')
    assert reply.send_args == {'disable_web_page_preview': True,
                               'parse_mode': 'Markdown'}


@pytest.mark.parametrize('row,result', [
    (client.CronRow(1, '09:00', '', 'radar check', '', 1234567, None,
                    '19:00', '30 mins'),
     'every 30 mins between 09:00 and 19:00 _weekdays_ radar check'),
    (client.CronRow(2, '10:00', 'monday', 'reminder blue bag', 'weather',
                    1234567, None, None, None),
     '10:00 _monday_ reminder blue bag THEN weather'),
])
def test_entry(row, result):
    assert row.entry() == result


@pytest.mark.parametrize('days,result', [
    ('', 'weekdays'),
    ('monday', 'monday'),
    ('saturday sunday', 'weekend'),
    ('monday tuesday wednesday thursday friday saturday sunday', 'daily'),
    ('monday tuesday wednesday thursday friday', 'weekdays'),
])
def test_entry_days(days, result):
    row = client.CronRow(0, '', days, '', '', 0, 0, None, None)
    assert row.entry_days() == result


def test_add():
    cur = flexmock(lastrowid=3)
    (flexmock(cur)
     .should_receive('execute')
     .with_args('INSERT INTO cron '
                '("time", "days", "command", "then", created) '
                "VALUES (?, ?, ?, ?, datetime('now'))",
                ('09:00', 'monday', 'radar check', 'weather'))
     .once())

    conn = flexmock()
    flexmock(conn).should_receive('cursor').and_return(cur)

    (flexmock(client)
     .should_receive('get_conn')
     .replace_with(mock_contextmanager(conn)))

    (flexmock(client)
     .should_receive('schedule_jobs')
     .once())

    reply = client.do_add('09:00', ['monday'], 'radar check', 'weather')

    assert reply[0] == 'Scheduled 09:00 _monday_ radar check THEN weather'

    # undo
    (flexmock(client)
     .should_receive('do_delete')
     .with_args(3)
     .and_return(('deleted', 'undo_callback')))

    assert reply[1]() == 'deleted'


def test_delete():
    (flexmock(client)
     .should_receive('fetch_cron')
     .with_args(rowid=3)
     .and_return([client.CronRow(3, '09:00', '', 'radar check', '',
                                 1234567, None, None, None)]))

    conn = flexmock()

    (flexmock(conn)
     .should_receive('execute')
     .with_args("UPDATE cron SET deleted=datetime('now') WHERE rowid = ?",
                (3,))
     .once())

    (flexmock(client)
     .should_receive('get_conn')
     .replace_with(mock_contextmanager(conn)))

    (flexmock(client)
     .should_receive('schedule_jobs')
     .times(2))  # for repeat, and undo

    reply = client.do_delete(3)

    assert reply[0] == 'Deleted 09:00 _weekdays_ radar check'

    # undo
    (flexmock(conn)
     .should_receive('execute')
     .with_args('UPDATE cron SET deleted=NULL WHERE rowid = ?', (3,))
     .once())

    assert reply[1]() == 'Restored 09:00 _weekdays_ radar check'


def test_repeat():
    (flexmock(client)
     .should_receive('fetch_cron')
     .with_args(rowid=3)
     .and_return([client.CronRow(3, '09:00', 'monday tuesday',
                                 'radar check', '',
                                 1234567, None, None, None)]))

    conn = flexmock()

    (flexmock(conn)
     .should_receive('execute')
     .with_args("UPDATE cron SET deleted=datetime('now') WHERE rowid = ?",
                (3,))
     .once())

    cur = flexmock(lastrowid=5)
    (flexmock(cur)
     .should_receive('execute')
     .with_args(
         'INSERT INTO cron '
         '("time", "days", "command", "then", "until", "every", created) '
         "VALUES (?, ?, ?, ?, ?, ?, datetime('now'))",
         ('09:00', 'monday tuesday', 'radar check', '',
          '21:00', '30 mins'))
     .once())

    flexmock(conn).should_receive('cursor').and_return(cur)

    (flexmock(client)
     .should_receive('get_conn')
     .replace_with(mock_contextmanager(conn)))

    (flexmock(client)
     .should_receive('schedule_jobs')
     .times(2))  # for repeat, and undo

    reply = client.do_repeat(3, '21:00', '30 mins')

    assert reply[0] == ('Repeat every 30 mins between 09:00 and 21:00 '
                        '_monday tuesday_ radar check')

    # undo

    (flexmock(conn)
     .should_receive('execute')
     .with_args('UPDATE cron SET deleted=NULL WHERE rowid = ?', (3,))
     .once())

    (flexmock(conn)
     .should_receive('execute')
     .with_args("UPDATE cron SET deleted=datetime('now') WHERE rowid = ?",
                (5,))
     .once())

    assert reply[1]() == 'Restored 09:00 _monday tuesday_ radar check'


def test_repeat_deleted():
    (flexmock(client)
     .should_receive('fetch_cron')
     .with_args(rowid=3)
     .and_return([client.CronRow(3, '09:00', 'monday tuesday',
                                 'radar check', '',
                                 1234567, 2345678, None, None)]))

    reply = client.do_repeat(3, '21:00', '30 mins')

    assert reply == 'Entry 3 has been deleted!'

def test_edit():
    (flexmock(client)
     .should_receive('fetch_cron')
     .with_args(rowid=3)
     .and_return([client.CronRow(3, '09:00', 'monday tuesday',
                                 'radar check', '',
                                 1234567, None, None, None)]))

    conn = flexmock()

    (flexmock(conn)
     .should_receive('execute')
     .with_args("UPDATE cron SET deleted=datetime('now') WHERE rowid = ?",
                (3,))
     .once())

    cur = flexmock(lastrowid=5)
    (flexmock(cur)
     .should_receive('execute')
     .with_args(
         'INSERT INTO cron '
         '("time", "days", "command", "then", "until", "every", created) '
         "VALUES (?, ?, ?, ?, ?, ?, datetime('now'))",
         ('09:00', 'monday tuesday', 'radar', '', None, None))
     .once())

    flexmock(conn).should_receive('cursor').and_return(cur)

    (flexmock(client)
     .should_receive('get_conn')
     .replace_with(mock_contextmanager(conn)))

    (flexmock(client)
     .should_receive('schedule_jobs')
     .times(2))  # for repeat, and undo

    reply = client.do_edit(3, 'radar')

    assert reply[0] == ('Edited 09:00 _monday tuesday_ radar')

    # undo

    (flexmock(conn)
     .should_receive('execute')
     .with_args('UPDATE cron SET deleted=NULL WHERE rowid = ?', (3,))
     .once())

    (flexmock(conn)
     .should_receive('execute')
     .with_args("UPDATE cron SET deleted=datetime('now') WHERE rowid = ?",
                (5,))
     .once())

    assert reply[1]() == 'Restored 09:00 _monday tuesday_ radar check'


def test_edit_deleted():
    (flexmock(client)
     .should_receive('fetch_cron')
     .with_args(rowid=3)
     .and_return([client.CronRow(3, '09:00', 'monday tuesday',
                                 'radar check', '',
                                 1234567, 2345678, None, None)]))

    reply = client.do_edit(3, 'radar')

    assert reply == 'Entry 3 has been deleted!'


def test_run():
    (flexmock(client)
     .should_receive('fetch_cron')
     .and_return([client.CronRow(1, '09:00', 'monday', 'radar check', '',
                                 1234567, None, None, None)]))

    reply = flexmock(callback='callback')
    (flexmock(reply).should_receive('push').and_return(True))

    (flexmock(client)
     .should_receive('run_command')
     .with_args('radar check')
     .and_return(1, reply))

    with patch.object(client, 'modules') as modules:
        assert client.do_run(1) == 'Finished "radar check"'

        assert modules.mock_calls == [
            call.set_callback(reply, interactive=False)
        ]


def test_run_callback():
    (flexmock(client)
     .should_receive('fetch_cron')
     .and_return([client.CronRow(1, '09:00', 'monday', 'radar check', '',
                                 1234567, None, None, None)]))

    reply = flexmock()
    (flexmock(reply).should_receive('push').and_return(True))

    (flexmock(client)
     .should_receive('run_command')
     .with_args('radar check')
     .and_return(1, reply))

    with patch.object(client, 'modules') as modules:
        assert client.do_run(1) == 'Finished "radar check"'

        assert modules.mock_calls == [
            call.set_callback(reply, interactive=False)
        ]


def test_run_bad():
    (flexmock(client)
     .should_receive('fetch_cron')
     .and_return([client.CronRow(1, '09:00', 'monday', 'radar check', '',
                                 1234567, None, None, None)]))

    assert client.do_run(2) is None


def test_run_empty():
    (flexmock(client)
     .should_receive('fetch_cron')
     .and_return([]))

    assert client.do_run(1) is None


def test_run_then():
    (flexmock(client)
     .should_receive('fetch_cron')
     .and_return([client.CronRow(1, '09:00', 'monday', 'radar $word_check',
                                 'weather', 1234567, None, None, None)]))

    reply1 = flexmock()
    (flexmock(reply1).should_receive('push').and_return(True))

    (flexmock(client)
     .should_receive('run_command')
     .with_args('radar $word_check')
     .and_return(1, reply1))

    reply2 = flexmock()
    (flexmock(reply2).should_receive('push').and_return(True))

    (flexmock(client)
     .should_receive('run_command')
     .with_args('weather')
     .and_return(2, reply2))

    with patch.object(client, 'modules') as modules:
        assert client.do_run(1) == 'Finished "radar $word\\_check"'

    assert modules.mock_calls == [
        call.set_callback(reply1, interactive=False),
        call.set_callback(reply2, interactive=False)
    ]


def test_run_not_then():
    (flexmock(client)
     .should_receive('fetch_cron')
     .and_return([client.CronRow(1, '09:00', 'monday', 'radar check',
                                 'weather', 1234567, None, None, None)]))

    reply1 = flexmock()
    (flexmock(reply1).should_receive('push').and_return(False))

    (flexmock(client)
     .should_receive('run_command')
     .with_args('radar check')
     .and_return(1, reply1))

    assert client.do_run(1) == 'Finished "radar check"'


def test_schedule_jobs_empty():
    (flexmock(client.schedule)
     .should_receive('clear')
     .once())

    (flexmock(client)
     .should_receive('fetch_cron')
     .and_return([]))

    client.schedule_jobs()


def test_schedule_jobs():
    (flexmock(client.schedule)
     .should_receive('clear')
     .once())

    (flexmock(client)
     .should_receive('fetch_cron')
     .and_return([client.CronRow(1, '09:00', '', 'radar', 'weather',
                                 1234567, None, None, None),
                  client.CronRow(2, '10:00', 'monday tuesday', 'todo', '',
                                 1234567, None, None, None)]))

    job1 = flexmock()
    (flexmock(job1)
     .should_receive('do')
     .with_args(client.push_replies, 'radar', 'weather')
     .and_return(job1))

    (flexmock(client)
     .should_receive('_schedule_jobs')
     .with_args(['monday', 'tuesday', 'wednesday', 'thursday', 'friday'],
                ['09:00'])
     .and_return(job1)
     .once())

    job2 = flexmock()
    (flexmock(job2)
     .should_receive('do')
     .with_args(client.push_replies, 'todo', '')
     .and_return(job2))

    (flexmock(client)
     .should_receive('_schedule_jobs')
     .with_args(['monday', 'tuesday'], ['10:00'])
     .and_return(job2)
     .once())

    client.schedule_jobs()


def test_schedule_jobs_one():
    job = flexmock()
    (flexmock(job)
     .should_receive('at').with_args('09:00')
     .and_return(job))
    (flexmock(client.schedule)
     .should_receive('every')
     .and_return(flexmock(monday=job)))
    assert list(client._schedule_jobs(['monday'], ['09:00'])) == [job]


def test_schedule_jobs_days():
    job1 = flexmock()
    (flexmock(job1)
     .should_receive('at').with_args('09:00')
     .and_return(job1))
    job2 = flexmock()
    (flexmock(job2)
     .should_receive('at').with_args('09:00')
     .and_return(job2))
    (flexmock(client.schedule)
     .should_receive('every')
     .and_return(flexmock(monday=job1))
     .and_return(flexmock(tuesday=job2)))
    assert list(client._schedule_jobs(['monday', 'tuesday'], ['09:00'])) \
        == [job1, job2]


def test_schedule_jobs_times():
    job1 = flexmock()
    (flexmock(job1)
     .should_receive('at').with_args('09:00')
     .and_return(job1))
    job2 = flexmock()
    (flexmock(job2)
     .should_receive('at').with_args('10:00')
     .and_return(job2))
    (flexmock(client.schedule)
     .should_receive('every')
     .and_return(flexmock(monday=job1))
     .and_return(flexmock(monday=job2)))
    assert list(client._schedule_jobs(['monday'], ['09:00', '10:00'])) \
        == [job1, job2]


@pytest.mark.parametrize('row_days,days', [
    ('', ['monday', 'tuesday', 'wednesday', 'thursday', 'friday']),
    ('monday', ['monday']),
    ('monday tuesday', ['monday', 'tuesday']),
])
def test_get_days(row_days, days):
    row = client.CronRow(0, '', row_days, '', '', 0, 0, None, None)
    assert list(row.get_days()) == days


@pytest.mark.parametrize('row_time,row_until,row_every,times', [
    ('09:00', None, None, ['09:00']),
    ('09:00', '11:00', '30 mins', ['09:00', '09:30', '10:00', '10:30',
                                   '11:00']),
    ('11:45', '14:00', '45 mins', ['11:45', '12:30', '13:15', '14:00']),
    ('00:00', '23:59', '15 mins', ['00:00', '00:15', '00:30',
                                   ...,
                                   '23:15', '23:30', '23:45']),
])
def test_get_times(row_time, row_until, row_every, times):
    row = client.CronRow(0, row_time, '', '', '', 0, 0, row_until, row_every)
    result = list(row.get_times())
    if Ellipsis in times:
        pos = times.index(Ellipsis)
        assert result[:pos] == times[:pos]
        rest = times[pos + 1:]
        assert result[-len(rest):] == rest
    else:
        assert result == times


@pytest.mark.parametrize('hh_mm,mins', [
    ('00:00', 0),
    ('00:01', 1),
    ('00:59', 59),
    ('01:00', 60),
    ('01:01', 61),
    ('23:59', 24 * 60 - 1),
])
def test_to_mins(hh_mm, mins):
    assert client.to_mins(hh_mm) == mins


@pytest.mark.parametrize('hh_mm,mins', [
    ('00:00', 0),
    ('00:01', 1),
    ('00:59', 59),
    ('01:00', 60),
    ('01:01', 61),
    ('23:59', 24 * 60 - 1),
])
def test_from_mins(hh_mm, mins):
    assert client.from_mins(mins) == hh_mm


def test_once():
    cur = flexmock(lastrowid=3)
    (flexmock(cur)
     .should_receive('execute')
     .with_args('INSERT INTO cron '
                '("time", "days", "command", "then", created) '
                "VALUES (?, ?, ?, ?, datetime('now'))",
                ('09:00',
                 'monday tuesday wednesday thursday friday saturday sunday',
                 'heater $word_home',
                 ''))
     .once())

    (flexmock(cur)
     .should_receive('execute')
     .with_args('UPDATE cron SET "then"=? WHERE rowid=?',
                ('mute cron delete 3', 3))
     .once())

    conn = flexmock()
    flexmock(conn).should_receive('cursor').and_return(cur)

    (flexmock(client)
     .should_receive('get_conn')
     .replace_with(mock_contextmanager(conn)))

    (flexmock(client)
     .should_receive('schedule_jobs')
     .once())

    reply = client.do_once('09:00', 'heater $word_home')

    assert reply[0] == 'Scheduled heater $word\\_home at 09:00'

    # undo
    (flexmock(client)
     .should_receive('do_delete')
     .with_args(3)
     .and_return(('deleted', 'undo_callback')))

    assert reply[1]() == 'deleted'


@pytest.mark.parametrize('now,delay,result', [
    (datetime(2010, 11, 12, 9, 15, 23), '01:20', '10:35'),
    (datetime(2010, 11, 12, 9, 15, 23), '12:20', '21:35'),
    (datetime(2010, 11, 12, 18, 15, 23), '12:20', '06:35'),
])
def test_once_relative(now, delay, result):
    (flexmock(client)
     .should_receive('do_once')
     .with_args(result, 'command')
     .and_return('result'))

    assert client.do_once_relative(delay, 'command', now=now) == 'result'
