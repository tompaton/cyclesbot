import pytest
from . import parse_msg


@pytest.mark.parametrize('msg,cmd,kwargs', [
    ('/cron', 'list', {}),
    ('cron add 7:30 tuesday radar check',
     'add', {'time': '7:30', 'days': ['tuesday'],
             'command': 'radar check',
             'then': ''}),
    ('cron add 7:02 weekdays weather',
     'add', {'time': '7:02', 'days': [],
             'command': 'weather',
             'then': ''}),
    ('cron add 7:02 weekend weather',
     'add', {'time': '7:02', 'days': ['saturday', 'sunday'],
             'command': 'weather',
             'then': ''}),
    ('cron add 7:02 daily weather',
     'add', {'time': '7:02', 'days': ['monday', 'tuesday', 'wednesday',
                                      'thursday', 'friday', 'saturday',
                                      'sunday'],
             'command': 'weather',
             'then': ''}),
    ('cron add 19:30 weekdays reminder Check expenses '
     'then bank csv 45',
     'add', {'time': '19:30', 'days': [],
             'command': 'reminder Check expenses',
             'then': 'bank csv 45'}),
    ('cron run 1', 'run', {'entry': 1}),
    ('cron delete 1', 'delete', {'entry': 1}),
    ('cron repeat 1 every 30 mins until 21:00',
     'repeat', {'entry': 1, 'every': '30 mins', 'until': '21:00'}),
    ('cron at 7:02 heater day',
     'once', {'time': '7:02', 'command': 'heater day'}),
    ('cron in 1:30 heater away',
     'once_relative', {'delay': '1:30', 'command': 'heater away'}),
    ('cron add 19:30 weekdays cc 1234 mqtt check topic/one > 10 "Huh?"',
     'add', {'time': '19:30', 'days': [], 'then': '',
             'command': 'cc 1234 mqtt check topic/one > 10 "Huh?"'}),
    ('cron edit 1 weather 12345',
     'edit', {'entry': 1, 'command': 'weather 12345'}),
])
def test_cron(msg, cmd, kwargs):
    assert list(parse_msg(msg)) == [(cmd, kwargs)]
