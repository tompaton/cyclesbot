from pyparsing import Or, And
from common import parse


class TestFileName:

    def test_name(self):
        assert str(parse.FileName) == 'filename'


class TestNumber:

    def test_name(self):
        assert str(parse.number) == 'number'


class TestPermutations:

    def test_two(self):
        assert (parse.permutationOf('1', '2', setName=False)
                == Or([And(['1', '2']),
                       And(['2', '1'])]))

    def test_three(self):
        assert (parse.permutationOf('1', '2', '3', setName=False)
                == Or([And(['1', '2', '3']),
                       And(['1', '3', '2']),
                       And(['2', '1', '3']),
                       And(['2', '3', '1']),
                       And(['3', '1', '2']),
                       And(['3', '2', '1'])]))

    def test_name(self):
        assert (str(parse.permutationOf('1', '2'))
                == str(And(['1', '2'])))


class TestCombinations:

    def test_two(self):
        assert (parse.combinationsOf('1', '2', setName=False)
                == Or([And(['1', '2']),
                       And(['2', '1']),
                       And(['1']),
                       And(['2'])]))

    def test_three(self):
        assert (parse.combinationsOf('1', '2', '3', setName=False)
                == Or([And(['1', '2', '3']),
                       And(['1', '3', '2']),
                       And(['2', '1', '3']),
                       And(['2', '3', '1']),
                       And(['3', '1', '2']),
                       And(['3', '2', '1']),
                       And(['1', '2']),
                       And(['2', '1']),
                       And(['1', '3']),
                       And(['3', '1']),
                       And(['2', '3']),
                       And(['3', '2']),
                       And(['1']),
                       And(['2']),
                       And(['3'])]))

    def test_name(self):
        assert (str(parse.combinationsOf('1', '2'))
                == str(And(['1', '2'])))
