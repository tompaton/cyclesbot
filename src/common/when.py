import os

from datetime import datetime, timedelta
from functools import partial

print('Timezone: {} {}'.format(os.environ.get('TZ'), datetime.now()))

def past(to_dow, from_dow):
    return (from_dow - to_dow) % 7

def future(to_dow, from_dow):
    return (to_dow - from_dow - 1) % 7 + 1

def offset(value, from_dow):
    return value

# 'm' used for meat in lunch
DAYS = [['mon', 'monday'],
        ['tu', 'tue', 'tuesday'],
        ['w', 'wed', 'wednesday'],
        ['th', 'thu', 'thursday'],
        ['f', 'fri', 'friday'],
        ['sa', 'sat', 'saturday'],
        ['su', 'sun', 'sunday']]

PAST = {kw: partial(past, i)
            for i, kws in enumerate(DAYS)
            for kw in kws}
PAST['today'] = partial(offset, 0)
PAST['yesterday'] = partial(offset, 1)

FUTURE = {kw: partial(future, i)
            for i, kws in enumerate(DAYS)
            for kw in kws}
FUTURE['tomorrow'] = partial(offset, 1)
FUTURE['today'] = partial(offset, 0)
FUTURE['yesterday'] = partial(offset, -1)
FUTURE['week'] = partial(offset, 7)

def past_dow_keywords():
    return PAST.keys()

def future_dow_keywords():
    return FUTURE.keys()

def get_past_date(dow, now=None):
    now = now or datetime.now()
    return now - timedelta(days=PAST[dow](now.weekday()))

def get_future_date(dow, now=None):
    now = now or datetime.now()
    return now + timedelta(days=FUTURE[dow](now.weekday()))
