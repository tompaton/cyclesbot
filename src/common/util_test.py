import pytest
import os
from . import util


@pytest.mark.parametrize('before,after', [
    ('this is a regular string',
     'this is a regular string'),
    ('this has snake_case_words',
     r'this has snake\_case\_words'),
    ('this is [not] a link',
     r'this is \[not] a link'),
    ('an *asterisk',
     r'an \*asterisk'),
])
def test_escape_markdown(before, after):
    assert util.escape_markdown(before) == after


def test_workdir():
    cwd = os.getcwd()

    assert cwd != '/tmp'

    with util.workdir('/tmp'):
        assert os.getcwd() == '/tmp'

    assert os.getcwd() == cwd
