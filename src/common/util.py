import os
from contextlib import contextmanager


def escape_markdown(value):
    return value.replace('_', r'\_').replace('[', r'\[').replace('*', r'\*')


@contextmanager
def workdir(directory):
    cwd = os.getcwd()
    try:
        os.chdir(directory)
        yield directory
    finally:
        os.chdir(cwd)
