import subprocess
import shlex

# python 3.4
def run(*args):  # pragma: no cover
    return subprocess.check_output(' '.join(map(shlex.quote, args)),
                                   shell=True, executable='/bin/bash') \
                     .decode("utf-8")

# python 3.5
def run35(*args):  # pragma: no cover
    result = subprocess.run(' '.join(map(shlex.quote, args)),
                            shell=True, executable='/bin/bash',
                            stdout=subprocess.PIPE)
    return result.stdout
