import contextlib


def mock_contextmanager(result):
    @contextlib.contextmanager
    def inner(*args, **kwargs):
        yield result
    return inner
