import traceback
from urllib.request import urlopen
from bs4 import BeautifulSoup

def summary(url):  # pragma: no cover  # not worth testing this...
    try:
        soup = BeautifulSoup(urlopen(url).read(), 'html.parser')
    except:
        traceback.print_exc()
        return ('', '')
    else:
        return (find_tag(soup,
                         ('meta', {'property': 'og:title', 'content': True}),
                         ('meta', {'name': 'twitter:title', 'content': True}),
                         ('title', {})),
                find_tag(soup,
                         ('meta', {'property': 'og:description',
                                   'content': True}),
                         ('meta', {'name': 'twitter:description',
                                   'content': True}),
                         ('meta', {'name': 'description', 'content': True})))

def find_tag(soup, *tags):  # pragma: no cover
    for tag, attrs in tags:
        title = soup.find(tag, attrs=attrs)
        if title:
            print('Found {} {}'.format(tag, attrs))
            if attrs.get('content'):
                return title['content']
            else:
                return title.string

    print('Not found')
    return ''

if __name__ == '__main__':  # pragma: no cover
    print(summary('https://www.youtube.com/watch?v=PFkCfvY-2es'))
    print(summary('https://tompaton.com/'))
    print(summary('https://tompaton.com/blog/'))
