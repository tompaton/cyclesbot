import contextlib
import subprocess

# by default, git pull/push/commit are disabled to make unit tests easier to
# write.  this will be enabled in the bot unless the --disable=git command line
# option is used.
enabled = False

class Repo():
    def __init__(self, path):
        self.path = path

    def _call(self, *args):
        cmd = ['git', '-C', self.path] + list(args)
        if enabled:
            return subprocess.check_output(cmd).decode('utf-8')
        else:
            return 'GIT: {}'.format(cmd)

    def pull(self):
        return self._call('pull', '-r')

    def add(self, *args):
        return self._call('add', *args)

    @contextlib.contextmanager
    def commit(self, message):
        print(self.pull())
        yield self
        print(self._commit(message))
        print(self.push())

    def _commit(self, message):
        return self._call('commit', '-m', message)

    def push(self):
        return self._call('push')
