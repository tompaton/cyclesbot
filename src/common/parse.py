import re

from itertools import combinations, permutations

from pyparsing import (Or, And, oneOf, Regex, Word, alphanums, OneOrMore,
                       quotedString, removeQuotes)
from pyparsing import pyparsing_common

from .jsonParser import jsonValue

assert(jsonValue)

Url = Regex('https?://[^ ]+').setName('url')
FileName = Word(alphanums + '_.-').setName('filename')


def QuotedString(**kwargs):
    BareWords = OneOrMore(Word(alphanums + '_.'), **kwargs) \
                .setParseAction(lambda toks: ' '.join(toks))
    return quotedString.setParseAction(removeQuotes) | BareWords


real0 = Regex(r'[+-]?\.\d+').setParseAction(pyparsing_common.convertToFloat)
number = (pyparsing_common.number | real0).setName('number')


def permutationOf(*exprs, setName=True):
    groups = [And(list(permutation)) for permutation in permutations(exprs)]
    expr = Or(groups)
    if setName:  # disable for tests as otherwise assert == fails due to name
        expr.setName(str(groups[0]))
    return expr


def combinationsOf(*exprs, setName=True):
    groups = []

    for n in reversed(range(1, len(exprs) + 1)):
        # if n == 1:
        #     groups.extend(expr for expr in exprs)
        # else:
        for combination in combinations(exprs, n):
            groups.extend(And(list(permutation))
                          for permutation in permutations(combination))

    expr = Or(groups)
    if setName:  # disable for tests as otherwise assert == fails due to name
        expr.setName(str(groups[0]))
    return expr


def keyword(*words, action=None):
    if len(words) == 1:
        kw = oneOf(words[0], caseless=True)
    else:
        kw = And([oneOf(word, caseless=True)
                  for word in words])

    return kw.setParseAction(lambda toks: action) if action else kw


# optional zero-width-joiner plus skin tone & male/female modifiers
SKIN_TONES = re.compile('\u200D?[\U0001F3FB-\U0001F3FF\u2640\u2642]')


def normalize_emoji(string):
    return SKIN_TONES.sub('', string)


# TODO: allow $words
# TODO: tests for each in at least one module

Command = QuotedString(stopOn=keyword('then')).setName('command')

Command2 = Regex(r"""
                  (?P<q1>"?)       # optional quote
                  (?P<command>.+?) # non-greedy
                  (?P=q1)          # close quote
                  $""",
                 flags=re.VERBOSE).setName('command')

CommandWithThen = Regex(r"""
                         (?P<q1>"?)       # optional quote
                         (?P<command>.+?) # non-greedy
                         (?P=q1)          # close quote
                         (\s+
                          [Tt]hen
                          \s+
                          (?P<q2>"?)      # optional quote
                          (?P<then>.+?)   # non-greedy
                          (?P=q2)         # close quote
                         )?               # then is optional
                         $""",
                        flags=re.VERBOSE).setName('command with then')
