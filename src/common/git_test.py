import pytest
from flexmock import flexmock
from . import git


@pytest.fixture
def call():
    return ()


@pytest.fixture
def repo(call):
    repo = git.Repo('/repo/path')

    if call:
        (flexmock(repo)
         .should_receive('_call')
         .with_args(*call[:-1])
         .and_return(call[-1]))

    return repo


def test_call(repo):
    assert not git.enabled
    assert repo._call('a', 'b') == "GIT: ['git', '-C', '/repo/path', 'a', 'b']"


def test_call_enabled(repo):
    flexmock(git, enabled=True)

    (flexmock(git.subprocess)
     .should_receive('check_output')
     .with_args(['git', '-C', '/repo/path', 'a', 'b'])
     .and_return(b'output'))

    assert repo._call('a', 'b') == "output"


@pytest.mark.parametrize('call', [('pull', '-r', 'output')])
def test_pull(repo):
    assert repo.pull() == 'output'


@pytest.mark.parametrize('call', [('add', '--all', 'output')])
def test_add(repo):
    assert repo.add('--all') == 'output'


@pytest.mark.parametrize('call', [('commit', '-m', 'message', 'output')])
def test_commit(repo):
    assert repo._commit('message') == 'output'


@pytest.mark.parametrize('call', [('push', 'output')])
def test_push(repo):
    assert repo.push() == 'output'


def test_commit_contextmanager(repo):
    (flexmock(repo)
     .should_receive('pull').and_return('pull')
     .once())
    (flexmock(repo)
     .should_receive('add').with_args('--all')
     .once())
    (flexmock(repo)
     .should_receive('_commit').with_args('message').and_return('commit')
     .once())
    (flexmock(repo)
     .should_receive('push').and_return('push')
     .once())

    with repo.commit('message') as repo:
        repo.add('--all')
