from flexmock import flexmock
import pytest
from . import parse_msg
from . import parse


@pytest.mark.parametrize('msg,cmd,kwargs', [
    ('yearly pullups add 10 today',
     'add', {'goal': 'pullups', 'count': [10], 'when': 'today'}),
    ('yearly pullups add 10, 20 today',
     'add', {'goal': 'pullups', 'count': [10, 20], 'when': 'today'}),
    ('yearly pullups add 10,20 today',
     'add', {'goal': 'pullups', 'count': [10, 20], 'when': 'today'}),
    ('yearly pullups 10',
     'add', {'goal': 'pullups', 'count': [10], 'when': 'today'}),
    ('yearly pullups 10, 20',
     'add', {'goal': 'pullups', 'count': [10, 20], 'when': 'today'}),
    ('yearly pullups 10,20',
     'add', {'goal': 'pullups', 'count': [10, 20], 'when': 'today'}),
    ('yearly reps pullups',
     'reps', {'goals': ['pullups']}),
    ('yearly reps pullups pushups',
     'reps', {'goals': ['pullups', 'pushups']}),
    ('yearly reps pullups tick exercises',
     'reps', {'goals': ['pullups'], 'cycle': 'exercises'}),
    ('yearly reps pullups pushups tick exercises',
     'reps', {'goals': ['pullups', 'pushups'], 'cycle': 'exercises'}),
    ('yearly pullups', 'status', {'goal': 'pullups'}),
    ('yearly pullups status', 'status', {'goal': 'pullups'}),
])
def test_opentabs(msg, cmd, kwargs):
    (flexmock(parse)
     .should_receive('get_past_date')
     .replace_with(lambda when: when))

    assert list(parse_msg(msg)) == [(cmd, kwargs)]
