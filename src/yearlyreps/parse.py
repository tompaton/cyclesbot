from pyparsing import Group, Optional, Word, alphas, oneOf, OneOrMore, Suppress

from pyparsing import pyparsing_common

from common.parse import keyword
from common.when import past_dow_keywords, get_past_date


Goal = Word(alphas)

TickKeyword = keyword('tick')
Cycle = Word(alphas)

YearlyReps = Group(keyword('yearly', 'reps', action='reps')('command')
                   + Group(OneOrMore(Goal, stopOn=TickKeyword))('goals')
                   + Optional(TickKeyword + Cycle('cycle')))

Count = Group(OneOrMore(pyparsing_common.integer
                        + Suppress(Optional(Word(','))),
                        stopOn=TickKeyword))
When = oneOf(past_dow_keywords(), caseless=True)('when').setName('weekday')

GoalStatus = Group(keyword('yearly', action='status')('command')
                   + Goal('goal')
                   + Optional(keyword('status')))

AddReps = Group(keyword('yearly', action='add')('command')
                + Goal('goal')
                + Optional(keyword('add'))
                + Count('count')
                + Optional(When, default='today')('when'))

Message = YearlyReps | AddReps | GoalStatus


def parse_msg(msg):
    for res in Message.parseString(msg, parseAll=True):
        kwargs = {key: res[key] for key in ('goal', 'count', 'cycle')
                  if key in res}
        if 'when' in res:
            kwargs['when'] = get_past_date(res['when'])
        if 'goals' in res:
            kwargs['goals'] = list(res['goals'])
        if 'count' in res:
            kwargs['count'] = list(res['count'])
        yield res['command'], kwargs


HELP = """
*yearly* [_goal_] [status]
*yearly* [_goal_] [add] _count_ [, _count_, ...] [today]
*yearly reps* [_goal_ ...] [tick _cycle_]
"""

# TODO: *yearly* [pullups|pushups] chart
