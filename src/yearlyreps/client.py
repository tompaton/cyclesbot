from datetime import datetime

from yearlyreps_client import YearlyReps2

from cycles.client import do_reminder, do_tick

from chat.msg_types import Reply, Questions
from . import config


def _reps():
    return YearlyReps2(config.yearlyreps_uid.get_value(),
                       config.yearlyreps_pwd.get_value())


def do_add(goal, count, when):
    return Reply('\n'.join(_reps()
                           .goal(goal)
                           .add_rep(date=when, count=count_)
                           .status()
                           .done()
                           for count_ in count))


def do_status(goal):
    return Reply(_reps()
                 .goal(goal)
                 .status()
                 .done())


_LAST_INPUT = {}


def do_reps(goals, cycle=None):
    def _done(**counts):
        status = []
        for goal, count_input in counts.items():
            try:
                count = [int(set_count)
                         for set_count in count_input.replace(' ', ',').split(',')
                         if set_count]
            except ValueError:
                count = []

            for set_count in count:
                if set_count:
                    _LAST_INPUT[goal] = count_input
                    status.append(_reps()
                                  .goal(goal)
                                  .add_rep(count=set_count)
                                  .status()
                                  .done())

        if status:
            status.insert(0, _reps().status_url)

            if cycle and do_reminder(name=[cycle]):
                resp = do_tick(name=[cycle], when=datetime.now())
                return (Reply
                        .from_response(resp)
                        .module(name='cycles')
                        .then('\n'.join(status)))
            else:
                return Reply('\n'.join(status))

    qns = Questions()
    for goal in goals:
        qns.add(goal, f'{goal}?', default=_LAST_INPUT.get(goal))
    return qns.done(_done)
