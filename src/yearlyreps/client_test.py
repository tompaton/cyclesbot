import datetime
import pytest
from unittest.mock import patch, call
from freezegun import freeze_time


@pytest.fixture
def client():
    from . import client
    with patch.object(client, 'config') as mock_config:
        mock_config.yearlyreps_uid.get_value.return_value = 'uid'
        mock_config.yearlyreps_pwd.get_value.return_value = 'pwd'

        client._LAST_INPUT.clear()

        yield client


@pytest.fixture
def cycles():
    with patch('yearlyreps.client.do_reminder'), \
         patch('yearlyreps.client.do_tick'):
        yield


def test_add_rep(client):
    today = datetime.date(2019, 3, 1)

    with patch.object(client, 'YearlyReps2') as mock_reps:
        instance = mock_reps.return_value
        instance2 = instance.goal.return_value
        instance3 = instance2.add_rep.return_value
        instance4 = instance3.status.return_value
        instance4.done.return_value \
            = '2019-03-01: 10 Pullups W+1 M+3 Q+7 Y+20'

        assert client.do_add('pullups', [10], today).message \
            == ('2019-03-01: 10 Pullups W+1 M+3 Q+7 Y+20')

    print(mock_reps.mock_calls)
    assert mock_reps.mock_calls == [
        call('uid', 'pwd'),
        call().goal('pullups'),
        call().goal().add_rep(count=10, date=today),
        call().goal().add_rep().status(),
        call().goal().add_rep().status().done()]


def test_add_rep_sets(client):
    today = datetime.date(2019, 3, 1)

    with patch.object(client, 'YearlyReps2') as mock_reps:
        instance = mock_reps.return_value
        instance.goal.return_value = instance
        instance.add_rep.return_value = instance
        instance.status.return_value = instance
        instance.done.side_effect \
            = ['2019-03-01: 10 Pullups W+1 M+3 Q+7 Y+20',
               '2019-03-01: 20 Pullups W+2 M+4 Q+8 Y+21']

        assert client.do_add('pullups', [10, 20], today).message \
            == ('2019-03-01: 10 Pullups W+1 M+3 Q+7 Y+20\n'
                '2019-03-01: 20 Pullups W+2 M+4 Q+8 Y+21')

    assert mock_reps.mock_calls == [
        call('uid', 'pwd'),
        call().goal('pullups'),
        call().add_rep(count=10, date=today),
        call().status(),
        call().done(),
        call('uid', 'pwd'),
        call().goal('pullups'),
        call().add_rep(count=20, date=today),
        call().status(),
        call().done()]


def test_goal_status(client):

    with patch.object(client, 'YearlyReps2') as mock_reps:
        instance = mock_reps.return_value
        instance2 = instance.goal.return_value
        instance3 = instance2.status.return_value
        instance3.done.return_value \
            = '2019-03-01: 10 Pullups W+1 M+3 Q+7 Y+20'

        assert client.do_status('pullups').message \
            == ('2019-03-01: 10 Pullups W+1 M+3 Q+7 Y+20')

    print(mock_reps.mock_calls)
    assert mock_reps.mock_calls == [
        call('uid', 'pwd'),
        call().goal('pullups'),
        call().goal().status(),
        call().goal().status().done()]


def test_reps(client, cycles):
    client.do_reminder.return_value = None

    with patch.object(client, 'YearlyReps2') as mock_reps:
        instance = mock_reps.return_value
        instance.status_url = 'https://yearlyreps.tompaton.com/goals/status'
        instance2 = instance.goal.return_value
        instance3 = instance2.add_rep.return_value
        instance4 = instance3.status.return_value
        instance4.done.side_effect \
            = ['2019-03-01: 10 Pullups W+1 M+3 Q+7 Y+20',
               '2019-03-01: 20 Pushups W+0 M+2 Q+5 Y+12']

        reply1 = client.do_reps(['Pullups', 'Pushups'], 'Exercises')

        assert reply1.message == 'Pullups?'
        reply2 = reply1.callback('10')
        assert reply2.message == 'Pushups?'
        reply3 = reply2.callback('20')
        assert reply3.message == (
            'https://yearlyreps.tompaton.com/goals/status\n'
            '2019-03-01: 10 Pullups W+1 M+3 Q+7 Y+20\n'
            '2019-03-01: 20 Pushups W+0 M+2 Q+5 Y+12'
        )

    assert mock_reps.mock_calls == [
        call('uid', 'pwd'),
        call().goal('Pullups'),
        call().goal().add_rep(count=10),
        call().goal().add_rep().status(),
        call().goal().add_rep().status().done(),
        call('uid', 'pwd'),
        call().goal('Pushups'),
        call().goal().add_rep(count=20),
        call().goal().add_rep().status(),
        call().goal().add_rep().status().done(),
        call('uid', 'pwd')]

    assert client.do_reminder.mock_calls == [
        call(name=['Exercises']),
    ]

    assert client._LAST_INPUT == {'Pullups': '10', 'Pushups': '20'}


def test_reps_no_cycle(client, cycles):

    with patch.object(client, 'YearlyReps2') as mock_reps:
        instance = mock_reps.return_value
        instance.status_url = 'https://yearlyreps.tompaton.com/goals/status'
        instance2 = instance.goal.return_value
        instance3 = instance2.add_rep.return_value
        instance4 = instance3.status.return_value
        instance4.done.side_effect \
            = ['2019-03-01: 5 Run W+1 M+3 Q+7 Y+20']

        reply1 = client.do_reps(['Run'])

        assert reply1.message == 'Run?'
        reply2 = reply1.callback('5')
        assert reply2.message == (
            'https://yearlyreps.tompaton.com/goals/status\n'
            '2019-03-01: 5 Run W+1 M+3 Q+7 Y+20'
        )

    assert mock_reps.mock_calls == [
        call('uid', 'pwd'),
        call().goal('Run'),
        call().goal().add_rep(count=5),
        call().goal().add_rep().status(),
        call().goal().add_rep().status().done(),
        call('uid', 'pwd')]

    assert client.do_reminder.mock_calls == []

    assert client._LAST_INPUT == {'Run': '5'}


@pytest.mark.parametrize('reply,count,reps',  # type: ignore
                         [('Y', 5, '5'),
                          ('N', 0, '5'),
                          ('6', 6, '6')])
def test_reps_default(client, cycles, reply, count, reps):
    client._LAST_INPUT['Run'] = '5'

    with patch.object(client, 'YearlyReps2') as mock_reps:
        instance = mock_reps.return_value
        instance.status_url = 'https://yearlyreps.tompaton.com/goals/status'
        instance2 = instance.goal.return_value
        instance3 = instance2.add_rep.return_value
        instance4 = instance3.status.return_value
        instance4.done.side_effect \
            = ['2019-03-01: 5 Run W+1 M+3 Q+7 Y+20']

        reply1 = client.do_reps(['Run'])

        assert reply1.message == 'Run? \\[Y] "5"'
        reply2 = reply1.callback(reply)

    if reply == 'N':
        assert reply2.message == 'Aborted.'
        assert mock_reps.mock_calls == []

    else:
        assert reply2.message == (
            'https://yearlyreps.tompaton.com/goals/status\n'
            '2019-03-01: 5 Run W+1 M+3 Q+7 Y+20'
        )

        assert mock_reps.mock_calls == [
            call('uid', 'pwd'),
            call().goal('Run'),
            call().goal().add_rep(count=count),
            call().goal().add_rep().status(),
            call().goal().add_rep().status().done(),
            call('uid', 'pwd')]

    assert client.do_reminder.mock_calls == []

    assert client._LAST_INPUT == {'Run': reps}


@pytest.mark.parametrize('reps',  # type: ignore
                         ['5, 6', '5 6', '5,6,'])
def test_reps_sets(client, cycles, reps):

    with patch.object(client, 'YearlyReps2') as mock_reps:
        instance = mock_reps.return_value
        instance.status_url = 'https://yearlyreps.tompaton.com/goals/status'
        instance.goal.return_value = instance
        instance.add_rep.return_value = instance
        instance.status.return_value = instance
        instance.done.side_effect \
            = ['2019-03-01: 5 Run W+1 M+3 Q+7 Y+20',
               '2019-03-01: 6 Run W+2 M+4 Q+8 Y+21']

        reply1 = client.do_reps(['Run'])

        assert reply1.message == 'Run?'
        reply2 = reply1.callback(reps)
        assert reply2.message == (
            'https://yearlyreps.tompaton.com/goals/status\n'
            '2019-03-01: 5 Run W+1 M+3 Q+7 Y+20\n'
            '2019-03-01: 6 Run W+2 M+4 Q+8 Y+21'
        )

    assert mock_reps.mock_calls == [
        call('uid', 'pwd'),
        call().goal('Run'),
        call().add_rep(count=5),
        call().status(),
        call().done(),
        call('uid', 'pwd'),
        call().goal('Run'),
        call().add_rep(count=6),
        call().status(),
        call().done(),
        call('uid', 'pwd')]

    assert client.do_reminder.mock_calls == []

    assert client._LAST_INPUT == {'Run': reps}


@freeze_time('2019-03-01')
def test_reps_zeros(client, cycles):
    client.do_reminder.return_value = client.Reply('Exercises due today')
    client.do_tick.return_value = 'Ticked Exercises', lambda: 'undo'

    with patch.object(client, 'YearlyReps2') as mock_reps:
        instance = mock_reps.return_value
        instance.status_url = 'https://yearlyreps.tompaton.com/goals/status'
        instance2 = instance.goal.return_value
        instance3 = instance2.add_rep.return_value
        instance4 = instance3.status.return_value
        instance4.done.side_effect \
            = ['2019-03-01: 20 Pushups W+0 M+2 Q+5 Y+12']

        reply1 = client.do_reps(['Pullups', 'Pushups'], 'Exercises')

        assert reply1.message == 'Pullups?'
        reply2 = reply1.callback('0')
        assert reply2.message == 'Pushups?'
        reply3 = reply2.callback('20')
        assert reply3._messages == [
            'Ticked Exercises',
            'https://yearlyreps.tompaton.com/goals/status\n'
            '2019-03-01: 20 Pushups W+0 M+2 Q+5 Y+12',
        ]
        assert [reply.get_module() for reply in reply3.replies] \
            == ['cycles', None]

    assert mock_reps.mock_calls == [
        call('uid', 'pwd'),
        call().goal('Pushups'),
        call().goal().add_rep(count=20),
        call().goal().add_rep().status(),
        call().goal().add_rep().status().done(),
        call('uid', 'pwd')]

    assert client.do_reminder.mock_calls == [
        call(name=['Exercises']),
    ]
    assert client.do_tick.mock_calls == [
        call(name=['Exercises'],
             when=datetime.datetime(2019, 3, 1)),
    ]

    assert client._LAST_INPUT == {'Pushups': '20'}
