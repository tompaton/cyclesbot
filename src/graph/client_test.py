# coding: utf-8

import pytest
from flexmock import flexmock

from datetime import date

from . import client

@pytest.mark.parametrize('value', [
    '',
    'a message',
    'a 1.2.3.4 ip address',
    'not 1b2 numbers either',
    'neither is2 this3',
    'this is a date 1/2/2016',
    'another date 2016-2-1',
])
def test_parse_non_numeric(value):
    assert client.parse_data(value) == []


@pytest.mark.parametrize('value', [
    'this is a date 1/2/2016',
    'another date 2016-2-1'
])
def test_parse_date(value):
    assert client.parse_data(value, parse_dates=True) == [date(2016, 2, 1)]


@pytest.mark.parametrize('value', [
    '0 1 2 3 4',
    '0,1,2,3,4',
    '0, 1, 2, 3, 4',
    '0\n1\n2\n3\n4',
    'a 0 b 1 c 2 d 3 e 4',
    '*a 0* b _1_ c *2* d _3 e_ 4',
    'a 0\nb 1\nc 2\nd 3\ne 4',
])
def test_parse_simple(value):
    assert client.parse_data(value) == [0, 1, 2, 3, 4]


def test_parse_reals():
    assert client.parse_data('0.1 1.2 2.1 3.4 .3') == [0.1, 1.2, 2.1, 3.4, 0.3]


@pytest.mark.parametrize('value', [
    '11 12\n21 22\n31 32\n',
    '11, 12\n21, 22\n31, 32\n',
])
def test_parse_2d(value):
    assert client.parse_data(value) == [[11, 12], [21, 22], [31, 32]]


def test_parse_weight_log():
    assert client.parse_data('*Weight*\n'
                             '2018-01-11 74.90kg\n'
                             '2018-01-12 74.50kg\n'
                             '2018-01-13 74.40kg\n'
                             '2018-01-14 75.20kg\n'
                             '2018-01-15 75.20kg\n'
                             '2018-01-16 74.70kg\n'
                             '2018-01-17 75.30kg\n'
                             '2018-01-19 75.20kg\n'
                             '2018-01-20 74.80kg') \
                             == [74.90, 74.50, 74.40, 75.20, 75.20, 74.70,
                                 75.30, 75.20, 74.80]


def test_parse_weight_log_dates():
    assert client.parse_data('*Weight*\n'
                             '2018-01-11 74.90kg\n'
                             '2018-01-12 74.50kg\n'
                             '2018-01-13 74.40kg\n'
                             '2018-01-14 75.20kg\n'
                             '2018-01-15 75.20kg\n'
                             '2018-01-16 74.70kg\n'
                             '2018-01-17 75.30kg\n'
                             '2018-01-19 75.20kg\n'
                             '2018-01-20 74.80kg',
                             parse_dates=True) \
                             == [[date(2018, 1, 11), 74.90],
                                 [date(2018, 1, 12), 74.50],
                                 [date(2018, 1, 13), 74.40],
                                 [date(2018, 1, 14), 75.20],
                                 [date(2018, 1, 15), 75.20],
                                 [date(2018, 1, 16), 74.70],
                                 [date(2018, 1, 17), 75.30],
                                 [date(2018, 1, 19), 75.20],
                                 [date(2018, 1, 20), 74.80]]


@pytest.mark.parametrize('value', ['0', '0.0', '0.1', '.1', '1', '1.2'])
@pytest.mark.parametrize('unit', ['', 'km', 'm', 'cm', 'mm',
                                  'kg', 'g', 'mg',
                                  'l', 'ml',
                                  'b', 'kb', 'mb', 'gb', 'tb', 'pb',
                                  '%'])
def test_numeric(value, unit):
    for case in [value + unit, value + unit.upper()]:
        assert client.isnumeric(case)
        assert client.isnumeric(case.upper())


@pytest.mark.parametrize('value_str,value',
                         [('0', 0), ('0.0', 0), ('0.1', .1),
                          ('.1', .1), ('1', 1), ('1.2', 1.2)])
@pytest.mark.parametrize('unit', ['', 'km', 'm', 'cm', 'mm',
                                  'kg', 'g', 'mg',
                                  'l', 'ml',
                                  'b', 'kb', 'mb', 'gb', 'tb', 'pb',
                                  '%'])
def test_parse_numeric(value_str, value, unit):
    word = value_str + unit
    assert client.parse_numeric(word) == value
    assert client.parse_numeric(word.upper()) == value


@pytest.mark.parametrize('case', ['', 'abc', '1a', 'a1', '1.2.2', '1,3', '1a2',
                                  '1+1', '1-1', '1-2-2015', '1/2', '1/2/2015',
                                  '1*2'])
def test_non_numeric(case):
    assert not client.isnumeric(case)


@pytest.mark.parametrize('word,value', [
    ('2018-01-02', date(2018, 1, 2)),
    ('2018-12-31', date(2018, 12, 31)),
    ('1-2-1999', date(1999, 2, 1)),
    ('2018/1/2', date(2018, 1, 2)),
    ('18/1/2', date(2018, 1, 2)),
])
def test_is_date(word, value):
    assert client.isdate(word)
    assert client.parse_date(word) == value


@pytest.mark.parametrize('case', ['', 'abc', '1a', 'a1', '1.2.2', '1,3', '1a2',
                                  '1+1', '1-1', '1-2-3015', '1/2', '1/32/2015',
                                  '1*2', '1999-99-99'])
def test_is_non_date(case):
    assert not client.isdate(case)


def test_empty():
    assert client.average_data([], None) == []


def test_none():
    assert client.average_data([1, 2, 3], None) == [1, 2, 3]


def test_one():
    assert client.average_data([1, 2, 3], 1) == [1, 2, 3]


def test_two():
    assert client.average_data([1, 2, 3, 4], 2) == [1.5, 3.5]


def test_remainder():
    assert client.average_data([1, 2, 3], 2) == [1.5, 3]


def test_decimals():
    # [2.5333333333333337, 5.166666666666667]
    assert client.average_data([2.1, 2.2, 3.3, 4.4, 5.5, 5.6], 3) \
        == [2.53, 5.17]


def test_sig_figures():
    assert client.sig_figures([1, 2, 3]) == 0
    assert client.sig_figures([1.0, 1.1, 1.2]) == 1
    assert client.sig_figures([1, 1.2, 1.32, 1.233]) == 3


def test_parse_data_cmd():
    (flexmock(client)
     .should_receive('run_command')
     .with_args('weight log')
     .and_return(('module',
                  [flexmock(message=('Date,Reading,Notes\n'
                                     '2009-10-01,76.60,\n'
                                     '2009-10-02,77.60,\n'
                                     '2009-10-03,77.10,\n'))])))

    assert client.parse_data_cmd('weight log') == ('module', [76.6, 77.6, 77.1])


def test_parse_data_cmd_dates():
    (flexmock(client)
     .should_receive('run_command')
     .with_args('weight log')
     .and_return(('module',
                  [flexmock(message=('Date,Reading,Notes\n'
                                     '2009-10-01,76.60,\n'
                                     '2009-10-02,77.60,\n'
                                     '2009-10-03,77.10,\n'))])))

    assert client.parse_data_cmd('weight log', parse_dates=True) \
        == ('module',
            [[date(2009, 10, 1), 76.6],
             [date(2009, 10, 2), 77.6],
             [date(2009, 10, 3), 77.1]])


def test_graph():
    (flexmock(client)
     .should_receive('parse_data_cmd')
     .with_args('weight log')
     .and_return((flexmock(__name__='module'),
                  [76.6, 77.6, 77.1])))

    reply = client.do_graph('weight log')
    assert reply.message == ('76.6 '
                             '▁█▅'
                             ' 77.1')
    assert reply.get_module() == 'module'


def test_graph_waist():
    (flexmock(client)
     .should_receive('parse_data_cmd')
     .with_args('waist log')
     .and_return((flexmock(__name__='module'),
                  [81.0, 83.0, 82.0])))

    reply = client.do_graph('waist log')
    assert reply.message == ('81.0 '
                             '▁█▅'
                             ' 82.0')
    assert reply.get_module() == 'module'


def test_graph_debug():
    (flexmock(client)
     .should_receive('parse_data_cmd')
     .with_args('weight log')
     .and_return((flexmock(__name__='module'),
                  [76.6, 77.6, 77.1])))

    reply = client.do_graph('weight log', debug='debug')
    assert reply.message == '[76.6, 77.6, 77.1]'
    assert reply.get_module() == 'module'


def test_graph_average():
    (flexmock(client)
     .should_receive('parse_data_cmd')
     .with_args('weight log')
     .and_return((flexmock(__name__='module'),
                  [76.61, 76.62, 76.63,
                   77.61, 77.62, 77.63,
                   77.11, 77.12, 77.13])))

    reply = client.do_graph('weight log', average=3)
    assert reply.message == ('76.62 '
                             '▁█▅'
                             ' 77.12')
    assert reply.get_module() == 'module'


def test_graph_average_debug():
    (flexmock(client)
     .should_receive('parse_data_cmd')
     .with_args('weight log')
     .and_return((flexmock(__name__='module'),
                  [76.61, 76.62, 76.63,
                   77.61, 77.62, 77.63,
                   77.11, 77.12, 77.13])))

    reply = client.do_graph('weight log', debug='debug', average=3)
    assert reply.message == '[76.62, 77.62, 77.12]'
    assert reply.get_module() == 'module'


def test_graph_average_minmax():
    (flexmock(client)
     .should_receive('parse_data_cmd')
     .with_args('weight log')
     .and_return((flexmock(__name__='module'),
                  [76.61, 76.62, 76.63,
                   77.61, 77.62, 77.63,
                   77.11, 77.12, 77.13])))

    reply = client.do_graph('weight log', minmax='minmax', average=3)
    assert reply.message == ('76.62 '
                             '▁█▅'
                             ' 77.62')
    assert reply.get_module() == 'module'


def test_cal_str():
    data = ([0, 0, 0, 4, 5, 6, 7]
            + ([1, 2, 3, 4, 5, 6, 7] * 3)
            + [1, 2, 3, 4, 5, 0, 0])
    assert client.cal_str(client.bar_chart(data)) == ('   ▅▆▇█\n'
                                                      '▁▂▃▅▆▇█\n'
                                                      '▁▂▃▅▆▇█\n'
                                                      '▁▂▃▅▆▇█\n'
                                                      '▁▂▃▅▆  ')


def test_bool_str():
    assert client.bool_str([True, False, None, 1, 0, 2]) == '■□ ■□■'


def test_int_str():
    assert client.int_str([None, 0, 1, 2, 19, 20, 21]) == '○⓪①②⑲⑳●'


def test_date_dict_empty():
    assert client.date_dict([]) == {}


def test_date_dict_single():
    assert client.date_dict([date(2018, 1, 11), date(2018, 1, 12),
                             date(2018, 1, 12)]) \
                             == {date(2018, 1, 11): 1,
                                 date(2018, 1, 12): 2}


def test_date_dict_pairs():
    assert client.date_dict([[date(2018, 1, 11), 1.1],
                             [date(2018, 1, 12), 1.1],
                             [date(2018, 1, 12), 1.1]]) \
                             == {date(2018, 1, 11): 1.1,
                                 date(2018, 1, 12): 2.2}


def test_fmt_calendar_empty():
    assert client.fmt_calendar({}) == None


def test_fmt_calendar_int():
    data = flexmock()

    (flexmock(client)
     .should_receive('date_dict')
     .with_args(data)
     .and_return({date(2018, 1, 11): 1,
                  date(2018, 1, 21): 2}))

    (flexmock(client)
     .should_receive('calendar_dates')
     .with_args(2018, 1)
     .and_return([None,
                  date(2018, 1, 1),
                  date(2018, 1, 11),
                  date(2018, 1, 21)]))

    assert client.fmt_calendar(data) == 'Jan\n○⓪①②\n'


def test_fmt_calendar_bar():
    data = flexmock()

    (flexmock(client)
     .should_receive('date_dict')
     .with_args(data)
     .and_return({date(2018, 1, 11): 1,
                  date(2018, 1, 21): 2}))

    (flexmock(client)
     .should_receive('calendar_dates')
     .with_args(2018, 1)
     .and_return([None,
                  date(2018, 1, 1),
                  date(2018, 1, 11),
                  date(2018, 1, 21)]))

    assert client.fmt_calendar(data, fmt='bar') == 'Jan\n  ▄█\n'


def test_fmt_calendar_bool():
    data = flexmock()

    (flexmock(client)
     .should_receive('date_dict')
     .with_args(data)
     .and_return({date(2018, 1, 11): 1,
                  date(2018, 1, 21): 1}))

    (flexmock(client)
     .should_receive('calendar_dates')
     .with_args(2018, 1)
     .and_return([None,
                  date(2018, 1, 1),
                  date(2018, 1, 11),
                  date(2018, 1, 21)]))

    assert client.fmt_calendar(data, fmt='bool') == 'Jan\n □■■\n'


def test_fmt_calendar_many():
    data = flexmock()

    (flexmock(client)
     .should_receive('date_dict')
     .with_args(data)
     .and_return({date(2018, 1, 11): 1,
                  date(2018, 1, 21): 2,
                  date(2018, 2, 13): 3}))

    (flexmock(client)
     .should_receive('calendar_dates')
     .with_args(2018, 1)
     .and_return([None,
                  date(2018, 1, 1),
                  date(2018, 1, 11),
                  date(2018, 1, 21)]))

    (flexmock(client)
     .should_receive('calendar_dates')
     .with_args(2018, 2)
     .and_return([None,
                  date(2018, 2, 1),
                  date(2018, 2, 13),
                  date(2018, 2, 21)]))

    assert client.fmt_calendar(data) == ('Jan\n○⓪①②\n'
                                         'Feb\n○⓪③⓪\n')


def test_get_months_empty():
    assert list(client.get_months([])) == []


def test_get_months_1a():
    assert list(client.get_months([date(2018, 1, 11)])) == [date(2018, 1, 1)]


def test_get_months_1b():
    assert list(client.get_months([date(2018, 1, 11),
                                   date(2018, 1, 21)])) == [date(2018, 1, 1)]


def test_get_months_2():
    assert list(client.get_months([date(2018, 1, 11),
                                   date(2018, 1, 21),
                                   date(2018, 2, 13)])) == [date(2018, 1, 1),
                                                            date(2018, 2, 1)]


def test_lookup_dates():
    assert client.lookup_dates([None, 'key1'],
                               {'key1': 1, 'key2': 2}) == [None, 1]


def test_calendar_dates():
    dates = client.calendar_dates(2018, 2)
    assert len(dates) == 5*7
    assert dates[:7] == [None,
                         None,
                         None,
                         date(2018, 2, 1),
                         date(2018, 2, 2),
                         date(2018, 2, 3),
                         date(2018, 2, 4)]
    assert dates[-7:] == [date(2018, 2, 26),
                          date(2018, 2, 27),
                          date(2018, 2, 28),
                          None,
                          None,
                          None,
                          None]


def test_bar_chart_padded_0():
    data = [1, 2, 3, 4, 5]
    assert client.bar_chart_padded(data) == client.bar_chart(data)


def test_bar_chart_padded():
    data = [1, 2, 3, 4, 5]
    data_padded = [None] + data + [None, None]
    assert client.bar_chart_padded(data_padded) \
        == ' ' + client.bar_chart(data) + '  '


def test_calendar_debug():
    (flexmock(client)
     .should_receive('parse_data_cmd')
     .with_args('weight log', parse_dates=True)
     .and_return((flexmock(__name__='module'),
                  [[date(2018, 2, 10), 76.6],
                   [date(2018, 2, 11), 77.6],
                   [date(2018, 2, 12), 77.1]])))

    reply = client.do_calendar('weight log', debug='debug')
    assert reply.message \
        == ('[[datetime.date(2018, 2, 10), 76.6], '
            '[datetime.date(2018, 2, 11), 77.6], '
            '[datetime.date(2018, 2, 12), 77.1]]')
    assert reply.get_module() == 'module'


def test_calendar():
    (flexmock(client)
     .should_receive('parse_data_cmd')
     .with_args('weight log', parse_dates=True)
     .and_return((flexmock(__name__='module'),
                  [[date(2018, 2, 10), 76.6],
                   [date(2018, 2, 11), 77.6],
                   [date(2018, 2, 12), 77.1]])))

    # TODO: fix min_value
    reply = client.do_calendar('weight log')
    assert reply.message \
        == ('Feb\n'
            '       \n'
            '     ██\n'
            '█      \n'
            '       \n'
            '       \n')
    assert reply.get_module() == 'module'
