# coding: utf-8
import calendar
import datetime
import math
import re

from collections import defaultdict

from unicode_charts import bar_chart

from chat.modules import run_command
from chat.msg_types import Reply


def do_graph(command, debug=False, average=None, minmax=None):
    module, data = parse_data_cmd(command)

    if average:
        data = average_data(data, bin_size=average)

    if debug:
        return Reply(repr(data)).module(module)

    else:
        if minmax:
            first, last = min(data), max(data)
        else:
            first, last = data[0], data[-1]

        message = '{} {} {}'.format(first, bar_chart(data), last)
        return Reply(message).module(module)


def do_calendar(command, debug=False, fmt='bar'):
    module, data = parse_data_cmd(command, parse_dates=True)

    if debug:
        return Reply(repr(data)).module(module)

    else:
        return Reply(fmt_calendar(data, fmt=fmt)).module(module)


def parse_data_cmd(command, parse_dates=False):
    module, replies = run_command(command)
    data = parse_data('\n'.join(reply.message for reply in replies),
                      parse_dates=parse_dates)
    return module, data


UNITS = ('mm cm m km '
         'mg g kg '
         'ml l '
         'b kb mb gb tb pb '
         '%')
isnumeric = re.compile(r'([0-9]+|[0-9]*\.[0-9]+)(|{})'
                       .format('|'.join(UNITS.split())),
                       re.IGNORECASE).fullmatch

# TODO: support '1 mar 2018' etc.
isdate = re.compile('('
                    '(19|20)?[0-9][0-9]'
                    '[/-][01]?[0-9]'
                    '[/-][0123]?[0-9]'
                    '|'
                    '[0123]?[0-9]'
                    '[/-][01]?[0-9]'
                    '[/-](19|20)?[0-9][0-9]'
                    ')').fullmatch

def parse_data(msg, parse_dates=False):
    sep = re.compile(r'[\s,\*_]')

    data = []
    for line in msg.splitlines():
        row = []
        for word in sep.split(line):
            if isnumeric(word):
                row.append(parse_numeric(word))
            if parse_dates and isdate(word):
                row.append(parse_date(word))
        if row:
            data.append(row)
    return simplify(data)


def parse_numeric(word):
    return float(word.strip(UNITS + UNITS.upper()))


def parse_date(word):
    for fmt in ['%Y-%m-%d', '%Y/%m/%d',
                '%d-%m-%Y', '%d/%m/%Y',
                '%y-%m-%d', '%y/%m/%d',
                '%d-%m-%y', '%d/%m/%y']:
        try:
            return datetime.datetime.strptime(word, fmt).date()
        except ValueError:
            pass


def simplify(data):
    if all(len(row) == 1 for row in data):
        return [row[0] for row in data]
    elif len(data) == 1:
        return data[0]
    else:
        return data


def average_data(data, bin_size=None):
    if bin_size and bin_size > 1:
        decimals = sig_figures(data) + math.ceil(math.log10(bin_size))

        return [round(average(data[i : i + bin_size]), decimals)
                for i in range(0, len(data), bin_size)]
    return data


def average(data):
    return sum(data) / len(data)


def sig_figures(data):
    return max([len('{:.5f}'.format(x).split('.')[1].strip('0'))
                for x in data])


def cal_str(bars):
    return '\n'.join(bars[i:i+7] for i in range(0, len(bars), 7))


def bool_str(data):
    chars = '□■ '
    return ''.join(chars[2] if value is None else chars[bool(value)]
                   for value in data)


def int_str(data):
    chars = '⓪①②③④⑤⑥⑦⑧⑨⑩⑪⑫⑬⑭⑮⑯⑰⑱⑲⑳●○'
    return ''.join(chars[-1] if value is None
                   else chars[value] if 0 <= value <= 20
                   else chars[-2]
                   for value in data)


def date_dict(data, reduce_fn=sum):
    collected = defaultdict(list)

    for row in data:
        try:
            date, value = row
        except TypeError:
            date, value = row, 1

        collected[date].append(value)

    return {key: reduce_fn(values) for key, values in collected.items()}


def fmt_calendar(data, fmt='int'):
    if not data:
        return

    dates = date_dict(data)

    # TODO: same min_value/max_value for bar_chart across all months
    # TODO: exclude 0 from min_value for bar
    formatter = {'int': int_str, 'bar': bar_chart_padded, 'bool': bool_str}[fmt]

    # TODO: ReplyList
    reply = ''

    for date in get_months(dates.keys()):
        month = date.strftime("%b") + '\n'
        reply += month + cal_str(formatter(
            lookup_dates(
                calendar_dates(date.year, date.month),
                dates))) + '\n'

    return reply


def get_months(dates):
    if not dates:
        return

    min_date = min(dates).replace(day=1)
    max_date = max(dates).replace(day=1)

    while min_date <= max_date:
        yield min_date

        if min_date.month == 12:
            min_date = min_date.replace(year=min_date.year + 1, month=1)
        else:
            min_date = min_date.replace(month=min_date.month + 1)


def lookup_dates(dates, data):
    return [data.get(date, 0) if date else None
            for date in dates]


def calendar_dates(year, month):
    return [datetime.date(year, month, day) if day else None
            for week in calendar.Calendar().monthdayscalendar(year, month)
            for day in week]


def bar_chart_padded(data, min_value=None, max_value=None):
    pre = ''
    post = ''

    while data[0] is None:
        data.pop(0)
        pre += ' '

    while data[-1] is None:
        data.pop(-1)
        post += ' '

    return pre + bar_chart(data, min_value=min_value, max_value=max_value) + post
