import pytest
from . import parse_msg


@pytest.mark.parametrize('msg,cmd,kwargs', [
    ('/graph weight log', 'graph', {'command': 'weight log'}),
    ('/graph "weight log"', 'graph', {'command': 'weight log'}),
    ('/graph debug "weight log"',
     'graph', {'command': 'weight log', 'debug': 'debug'}),
    ('/graph average 7 "weight log"',
     'graph', {'command': 'weight log', 'average': 7}),
    ('/graph debug average 7 "weight log"',
     'graph', {'command': 'weight log', 'average': 7, 'debug': 'debug'}),
    ('/graph average 7 debug "weight log"',
     'graph', {'command': 'weight log', 'average': 7, 'debug': 'debug'}),
    ('graph minmax temperatures',
     'graph', {'command': 'temperatures', 'minmax': 'minmax'}),
    ('graph minmax average 3 temperatures',
     'graph', {'command': 'temperatures', 'average': 3, 'minmax': 'minmax'}),
    ('graph average 3 minmax temperatures',
     'graph', {'command': 'temperatures', 'average': 3, 'minmax': 'minmax'}),
    ('graph minmax debug temperatures',
     'graph', {'command': 'temperatures', 'minmax': 'minmax', 'debug': 'debug'}),
])
def test_graph(msg, cmd, kwargs):
    assert list(parse_msg(msg)) == [(cmd, kwargs)]


@pytest.mark.parametrize('msg,cmd,kwargs', [
    ('/calendar weight log',
     'calendar', {'command': 'weight log'}),
    ('/calendar "weight log"',
     'calendar', {'command': 'weight log'}),
    ('/calendar debug "weight log"',
     'calendar', {'command': 'weight log', 'debug': 'debug'}),
    ('/calendar int debug "weight log"',
     'calendar', {'command': 'weight log', 'fmt': 'int', 'debug': 'debug'}),
    ('/calendar debug int "weight log"',
     'calendar', {'command': 'weight log', 'fmt': 'int', 'debug': 'debug'}),
    ('/calendar bar "weight log"',
     'calendar', {'command': 'weight log', 'fmt': 'bar'}),
    ('/calendar int "weight log"',
     'calendar', {'command': 'weight log', 'fmt': 'int'}),
    ('/calendar bool "weight log"',
     'calendar', {'command': 'weight log', 'fmt': 'bool'}),
])
def test_calendar(msg, cmd, kwargs):
    assert list(parse_msg(msg)) == [(cmd, kwargs)]
