from pyparsing import Optional, Suppress, Group, oneOf
from common.parse import keyword, number, combinationsOf, Command


DebugOption = keyword('debug')('debug')
MinMaxOption = keyword('minmax')('minmax')
AverageOption = Suppress(keyword('average')) + number('average')
FormatOption = oneOf('int bool bar', caseless=True)('fmt')

GraphOption = combinationsOf(DebugOption, AverageOption, MinMaxOption)

GraphCommand = (keyword('graph', action='graph')('action')
                + Optional(GraphOption)
                + Command('command'))

CalendarOption = combinationsOf(DebugOption, FormatOption)

CalendarCommand = (keyword('calendar', action='calendar')('action')
                   + Optional(CalendarOption)
                   + Command('command'))

Message = Optional(Suppress('/')) + Group(GraphCommand | CalendarCommand)


def parse_msg(msg):
    for res in Message.parseString(msg, parseAll=True):
        kwargs = {key: res[key]
                  for key in ['command', 'debug', 'average', 'minmax', 'fmt']
                  if key in res}
        yield res['action'], kwargs


HELP = """
*graph* [debug] [average n] [minmax] _command_
*calendar* [debug] [*bar*|int|bool] _command_
"""
