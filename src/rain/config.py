from config.var_types import ConfigParam, EnvVar

api_key = EnvVar('RAIN_TOKEN')

auto_location = ConfigParam(name='Rainfall record location (auto)')
manual_location = ConfigParam(name='Rainfall record location (manual)')
