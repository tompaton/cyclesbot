from pyparsing import Group, oneOf, Optional, Suppress

from common.parse import permutationOf, keyword, number
from common.when import past_dow_keywords, get_past_date

RainKeyword = Group(keyword('rain', action='rain'))('command')

Amount = number('amount').setName('amount') + Suppress(Optional('mm'))

Name = oneOf('auto', caseless=True)('name')
Mode = oneOf('day month week add', caseless=True)('mode')
When = oneOf(past_dow_keywords(), caseless=True)('when').setName('weekday')

Categories = permutationOf(Optional(Name, default='manual')('name'),
                           Optional(Mode, default='day')('mode'),
                           Optional(When, default='today')('when'))

RainFirst = Group(RainKeyword
                  + Amount
                  + Categories)

RainLast = Group(Amount
                 + Optional(Suppress(oneOf('of', caseless=True)))
                 + RainKeyword
                 + Categories)

UrlCommand = Group(Group(keyword('rain', 'url', action='url'))('command'))

Message = RainFirst | RainLast | UrlCommand

def parse_msg(msg):
    for res in Message.parseString(msg, parseAll=True):
        kwargs = {key: res[key] for key in ('amount', 'name', 'mode')
                  if key in res}
        if 'when' in res:
            kwargs['when'] = get_past_date(res['when'])
        yield res['command'][0], kwargs

HELP = """
*rain* _amount_ [mm] [today|weekday] [add|day|week|month] [manual|auto]
_amount_ [mm] [of] *rain* [today|weekday] [add|day|week|month] [manual|auto]
*rain url*
"""
