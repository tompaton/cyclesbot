import pytest
from flexmock import flexmock
from . import parse_msg
from . import parse


@pytest.mark.parametrize('msg,cmd,kwargs', [
    ('3.5mm of rain', 'rain',
     {'amount': 3.5, 'mode': 'day', 'name': 'manual', 'when': 'today'}),
    ('3.5mm rain', 'rain',
     {'amount': 3.5, 'mode': 'day', 'name': 'manual', 'when': 'today'}),
    ('3.5mm rain auto', 'rain',
     {'amount': 3.5, 'mode': 'day', 'name': 'auto', 'when': 'today'}),
    ('rain 3mm friday', 'rain',
     {'amount': 3, 'mode': 'day', 'name': 'manual', 'when': 'friday'}),
    ('rain 3', 'rain',
     {'amount': 3, 'mode': 'day', 'name': 'manual', 'when': 'today'}),
    ('rain 0.3', 'rain',
     {'amount': .3, 'mode': 'day', 'name': 'manual', 'when': 'today'}),
    ('rain .3', 'rain',
     {'amount': .3, 'mode': 'day', 'name': 'manual', 'when': 'today'}),
    ('rain 3mm', 'rain',
     {'amount': 3, 'mode': 'day', 'name': 'manual', 'when': 'today'}),
    ('rain 3mm month', 'rain',
     {'amount': 3, 'mode': 'month', 'name': 'manual', 'when': 'today'}),
    ('rain 3mm week', 'rain',
     {'amount': 3, 'mode': 'week', 'name': 'manual', 'when': 'today'}),
    ('rain 3.5mm', 'rain',
     {'amount': 3.5, 'mode': 'day', 'name': 'manual', 'when': 'today'}),
    ('rain 3.5 mm', 'rain',
     {'amount': 3.5, 'mode': 'day', 'name': 'manual', 'when': 'today'}),
    ('rain 3.5mm auto', 'rain',
     {'amount': 3.5, 'mode': 'day', 'name': 'auto', 'when': 'today'}),
    ('rain .3 add auto', 'rain',
     {'amount': .3, 'mode': 'add', 'name': 'auto', 'when': 'today'}),
])
def test_rain(msg, cmd, kwargs):
    (flexmock(parse)
     .should_receive('get_past_date')
     .replace_with(lambda when: when))

    assert list(parse_msg(msg)) == [(cmd, kwargs)]


@pytest.mark.parametrize('msg,cmd,kwargs', [
    ('rain url', 'url', {}),
])
def test_url(msg, cmd, kwargs):
    assert list(parse_msg(msg)) == [(cmd, kwargs)]
