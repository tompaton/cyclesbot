import pytest
from datetime import datetime
from flexmock import flexmock
from . import client


@pytest.fixture
def config():
    flexmock(client.config.api_key, get_value='token1')
    flexmock(client.config.auto_location, get_value='123')
    flexmock(client.config.manual_location, get_value='456')


@pytest.fixture
def config_no_auto(auto_location):
    flexmock(client.config.api_key, get_value='token1')
    flexmock(client.config.auto_location, get_value=auto_location)
    flexmock(client.config.manual_location, get_value='456')


def test_url(config):
    assert client.do_url() \
        == ('https://api.rainfallrecord.app/locations/456\n'
            'https://api.rainfallrecord.app/locations/123')


def test_empty_url():
    flexmock(client.config.auto_location, get_value='')
    flexmock(client.config.manual_location, get_value='456')
    assert client.do_url() \
        == ('https://api.rainfallrecord.app/locations/456')


@pytest.mark.parametrize('name,location', [
    ('auto', 123),
    ('manual', 456),
    ('other', None),
])
def test_location(name, location, config):
    loc = client._location(name)
    if location is None:
        assert loc is None
    else:
        assert isinstance(loc, client.Location)
        assert loc.location == location


def test_rain_auto(config):
    today = datetime(2017, 8, 27, 9, 0, 0)

    (flexmock(client)
     .should_receive('do_rain_basic')
     .with_args(12.3, 'auto', today, 'day')
     .and_return('logged'))

    reply = client.do_rain(12.3, 'auto', today, 'day')

    assert reply.message == 'logged'
    assert reply.callback is None


def test_rain_manual(config):
    today_date = flexmock()
    flexmock(today_date).should_receive('today').and_return(today_date)
    today = flexmock()
    flexmock(today).should_receive('date').and_return(today_date)

    (flexmock(client)
     .should_receive('do_rain_basic')
     .with_args(12.3, 'manual', today, 'day')
     .and_return('logged manual'))

    reply = client.do_rain(12.3, 'manual', today, 'day')

    assert reply.message == 'logged manual'
    assert reply.callback is None


def test_rain_manual_prompt_for_auto(config):
    today_date = flexmock()
    flexmock(today_date).should_receive('today').and_return(today_date)
    today = flexmock()
    flexmock(today).should_receive('date').and_return(today_date)

    (flexmock(client)
     .should_receive('do_rain_basic')
     .with_args(12.3, 'manual', today, 'day')
     .and_return('logged manual'))

    (flexmock(client)
     .should_receive('_prompt_for_auto')
     .with_args('manual', today, 'day')
     .and_return(True))

    reply1 = client.do_rain(12.3, 'manual', today, 'day')

    assert reply1.replies[0].message == 'logged manual'
    assert reply1.replies[1].message == 'Monthly reading for auto?'

    (flexmock(client)
     .should_receive('do_rain_basic')
     .with_args(23.4, 'auto', today, 'month')
     .and_return('logged auto'))

    reply2 = reply1.replies[1].callback('23.4')

    assert reply2.message == 'logged auto'
    assert reply2.callback is None


def test_rain_manual_past(config):
    when = datetime(2017, 8, 20, 8, 0, 0)

    (flexmock(client)
     .should_receive('do_rain_basic')
     .with_args(12.3, 'manual', when, 'day')
     .and_return('logged manual'))

    reply = client.do_rain(12.3, 'manual', when, 'day')

    assert reply.message == 'logged manual'
    assert reply.callback is None


def test_rain_manual_prompt_for_auto_abort(config):
    today_date = flexmock()
    flexmock(today_date).should_receive('today').and_return(today_date)
    today = flexmock()
    flexmock(today).should_receive('date').and_return(today_date)

    (flexmock(client)
     .should_receive('do_rain_basic')
     .with_args(12.3, 'manual', today, 'day')
     .and_return('logged manual'))

    (flexmock(client)
     .should_receive('_prompt_for_auto')
     .with_args('manual', today, 'day')
     .and_return(True))

    reply1 = client.do_rain(12.3, 'manual', today, 'day')

    assert reply1.replies[0].message == 'logged manual'
    assert reply1.replies[1].message == 'Monthly reading for auto?'

    reply2 = reply1.replies[1].callback('')

    assert reply2 is None


@pytest.mark.parametrize('auto_location', [None, ''])
def test_rain_manual_no_auto(config_no_auto):
    today_date = flexmock()
    flexmock(today_date).should_receive('today').and_return(today_date)
    today = flexmock()
    flexmock(today).should_receive('date').and_return(today_date)

    (flexmock(client)
     .should_receive('do_rain_basic')
     .with_args(12.3, 'manual', today, 'day')
     .and_return('logged manual'))

    reply1 = client.do_rain(12.3, 'manual', today, 'day')

    assert reply1.message == 'logged manual'
    assert reply1.callback is None


def test_rain_basic_day(config):
    when = datetime(2017, 8, 27, 8, 0, 0)

    loc = flexmock()

    (flexmock(loc)
     .should_receive('post')
     .with_args(when, 12.3)
     .and_return(True))

    (flexmock(client)
     .should_receive('Location')
     .with_args(456, 'token1')
     .and_return(loc))

    assert client.do_rain_basic(12.3, 'manual', when, 'day') \
        == 'Logged 12.3mm of rain for 2017-08-27 (manual)'


def test_rain_basic_day_error(config):
    when = datetime(2017, 8, 27, 8, 0, 0)

    loc = flexmock()

    (flexmock(loc)
     .should_receive('post')
     .with_args(when, 12.3)
     .and_raise(client.PostError, 'error!'))

    (flexmock(client)
     .should_receive('Location')
     .with_args(456, 'token1')
     .and_return(loc))

    assert client.do_rain_basic(12.3, 'manual', when, 'day') is None


def test_rain_basic_month(config):
    when = datetime(2017, 8, 27, 8, 0, 0)

    loc = flexmock(readings=flexmock())

    (flexmock(loc.readings)
     .should_receive('get_month_total')
     .with_args(when)
     .and_return(['Aug', 88.5]))

    (flexmock(loc)
     .should_receive('post')
     .with_args(when, 11.5)
     .and_return(True))

    (flexmock(client)
     .should_receive('Location')
     .with_args(456, 'token1')
     .and_return(loc))

    assert client.do_rain_basic(100.0, 'manual', when, 'month') \
        == ('Total for Aug: 88.5mm\n'
            'Logged 11.5mm of rain for 2017-08-27 (manual)')


def test_rain_basic_add(config):
    when = datetime(2017, 8, 27, 8, 0, 0)

    loc = flexmock(readings=flexmock())

    (flexmock(loc.readings)
     .should_receive('get_day_total')
     .with_args(when)
     .and_return(['27 Aug', 14.0]))

    (flexmock(loc)
     .should_receive('post')
     .with_args(when, 14.3)
     .and_return(True))

    (flexmock(client)
     .should_receive('Location')
     .with_args(456, 'token1')
     .and_return(loc))

    assert client.do_rain_basic(0.3, 'manual', when, 'add') \
        == ('Total for 27 Aug: 14.0mm\n'
            'Logged 14.3mm of rain for 2017-08-27 (manual)')


def test_rain_basic_week(config):
    when = datetime(2017, 8, 27, 8, 0, 0)

    loc = flexmock(readings=flexmock())

    (flexmock(loc.readings)
     .should_receive('get_week_total')
     .with_args(when)
     .and_return(29.5))

    (flexmock(loc)
     .should_receive('post')
     .with_args(when, 0.5)
     .and_return(True))

    (flexmock(client)
     .should_receive('Location')
     .with_args(456, 'token1')
     .and_return(loc))

    assert client.do_rain_basic(30.0, 'manual', when, 'week') \
        == ('Total for week: 29.5mm\n'
            'Logged 0.5mm of rain for 2017-08-27 (manual)')
