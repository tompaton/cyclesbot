from rainfallrecord_client import Location, PostError

from chat.msg_types import Reply, Question
from . import config


def _location(name):
    if name == 'auto':
        location = config.auto_location.value
    elif name == 'manual':
        location = config.manual_location.value
    else:
        location = None

    if location:
        return Location(int(location), config.api_key.value)
    else:
        return None


@config.api_key.required('Rain functions disabled.')
def do_rain(amount, name, when, mode):
    reply = Reply.from_response(do_rain_basic(amount, name, when, mode))
    if _prompt_for_auto(name, when, mode):
        def _auto(text):
            try:
                amount = float(text)
            except (ValueError, TypeError):
                pass
            else:
                return Reply(do_rain_basic(amount, 'auto', when, 'month'))

        return reply.then(Question('Monthly reading for auto?', _auto))

    else:
        return reply


def _prompt_for_auto(name, when, mode):
    return False

    if config.auto_location.value:
        if (name, mode) == ('manual', 'day'):
            if when.date() == when.date().today():
                return True
    return False


def do_rain_basic(amount, name, when, mode):
    if mode == 'month':
        month, month_total = _location(name).readings.get_month_total(when)
        amount = round(amount - month_total, 1)
        msg = 'Total for {}: {}mm\n'.format(month, round(month_total, 1))

    elif mode == 'week':
        week_total = _location(name).readings.get_week_total(when)
        amount = round(amount - week_total, 1)
        msg = 'Total for week: {}mm\n'.format(round(week_total, 1))

    elif mode == 'add':
        day, day_total = _location(name).readings.get_day_total(when)
        amount = round(day_total + amount, 1)
        msg = 'Total for {}: {}mm\n'.format(day, round(day_total, 1))

    else:  # mode == 'day'
        msg = ''

    try:
        _location(name).post(when, amount)
    except PostError as ex:
        print(ex)
        return None

    else:
        return '{}Logged {}mm of rain for {} ({})'.format(
            msg, amount, when.date(), name)


def do_url():
    return '\n'.join(location.url
                     for location in [_location('manual'), _location('auto')]
                     if location)
