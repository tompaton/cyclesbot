import pytest
from flexmock import flexmock
from . import parse_msg
from . import parse


@pytest.mark.parametrize('msg,cmd,kwargs', [
    ('/heater', 'heater', {'action': 'status'}),
    ('heater status', 'heater', {'action': 'status'}),
    ('heater on', 'heater', {'action': 'on'}),
    ('heater off', 'heater', {'action': 'off'}),
    ('heater manual', 'heater', {'action': 'manual'}),
    ('heater wake', 'heater', {'action': 'wake'}),
    ('heater away', 'heater', {'action': 'away'}),
    ('heater day', 'heater', {'action': 'day'}),
    ('heater home', 'heater', {'action': 'home'}),
    ('heater sleep', 'heater', {'action': 'sleep'}),

    ('heater chart taylor-st', 'graph',
     {'when': 'today', 'place': 'taylor-st', 'chart': 'radar_day'}),
    ('heater graph taylor-st', 'graph',
     {'when': 'today', 'place': 'taylor-st', 'chart': 'radar_day'}),
    ('heater graph taylor-st today', 'graph',
     {'when': 'today', 'place': 'taylor-st', 'chart': 'radar_day'}),
    ('heater graph taylor-st yesterday', 'graph',
     {'when': 'yesterday', 'place': 'taylor-st', 'chart': 'radar_day'}),
    ('heater graph taylor-st monday', 'graph',
     {'when': 'monday', 'place': 'taylor-st', 'chart': 'radar_day'}),

    ('garden chart taylor-st', 'graph',
     {'when': 'today', 'place': 'taylor-st', 'chart': 'garden_week'}),
    ('garden graph taylor-st', 'graph',
     {'when': 'today', 'place': 'taylor-st', 'chart': 'garden_week'}),
    ('garden graph taylor-st today', 'graph',
     {'when': 'today', 'place': 'taylor-st', 'chart': 'garden_week'}),
    ('garden graph taylor-st yesterday', 'graph',
     {'when': 'yesterday', 'place': 'taylor-st', 'chart': 'garden_week'}),
    ('garden graph taylor-st monday', 'graph',
     {'when': 'monday', 'place': 'taylor-st', 'chart': 'garden_week'}),

    ('house chart taylor-st', 'graph',
     {'when': 'today', 'place': 'taylor-st', 'chart': 'house_history'}),

    ('/mqtt topic', 'mqtt_get', {'topic': 'topic'}),
    ('/mqtt topic/#', 'mqtt_get', {'topic': 'topic/#'}),
    ('/mqtt topic/+/value', 'mqtt_get', {'topic': 'topic/+/value'}),

    ('/mqtt topic "message"',
     'mqtt_pub', {'topic': 'topic', 'message': '"message"',
                  'retained': False}),
    ('mqtt topic/sub-topic 123.0',
     'mqtt_pub', {'topic': 'topic/sub-topic', 'message': '123.0',
                  'retained': False}),
    ('mqtt retained topic {"value": 123.0}',
     'mqtt_pub', {'topic': 'topic', 'message': '{"value": 123.0}',
                  'retained': True}),

    ('mqtt check topic > 10 "message"',
     'mqtt_check', {'topic': 'topic', 'op': '>', 'value': '10',
                    'message': '"message"'}),
])
def test_iot(msg, cmd, kwargs):
    (flexmock(parse)
     .should_receive('get_past_date')
     .replace_with(lambda when: when))

    assert list(parse_msg(msg)) == [(cmd, kwargs)]
