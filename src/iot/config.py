from config.var_types import ConfigParam


mqtt_server = ConfigParam(name='MQTT server', default='localhost')
mqtt_port = ConfigParam(name='MQTT port', default='1883')
mqtt_user = ConfigParam(name='MQTT user')
mqtt_password = ConfigParam(name='MQTT password')

heater_mode_topic = ConfigParam(name='Heater mode topic')
heater_state_topic = ConfigParam(name='Heater state topic')

heater_update_delay = ConfigParam(name='Heater update delay', default='5')
