from pyparsing import (Group, Optional, Suppress, oneOf, Regex, Combine,
                       WordEnd, Word, alphanums, originalTextFor)
from common.parse import keyword, jsonValue
from common.when import get_past_date, past_dow_keywords


Topic = Regex(r'[\w\-/+#]+').setName('mqtt topic')

When = Combine(oneOf(past_dow_keywords(), caseless=True)
               + WordEnd()).setName('weekday')
Place = Word(alphanums + '.-').setName('place')

ChartType = (keyword('heater', action='radar_day')
             | keyword('garden', action='garden_week')
             | keyword('house', action='house_history'))

HeaterGraph = Group(ChartType('chart')
                    + keyword('chart graph', action='graph')('cmd')
                    + Place('place')
                    + Optional(When, default='today')('when'))

HeaterAction = oneOf('status on off manual wake away day home sleep', caseless=True)
Heater = Group(keyword('heater')('cmd')
               + Optional(HeaterAction, default='status')('action'))

Get = Group(keyword('mqtt', action='mqtt_get')('cmd')
            + Topic('topic'))

Publish = Group(keyword('mqtt', action='mqtt_pub')('cmd')
                + Optional(keyword('retained', action=True),
                           default=False)('retained')
                + Topic('topic')
                + originalTextFor(jsonValue)('message').setName('message'))

Operator = Regex(r'(=|!=|<|>)')

Check = Group(keyword('mqtt', 'check', action='mqtt_check')('cmd')
              + Topic('topic')
              + Operator('op')
              + originalTextFor(jsonValue)('value').setName('value')
              + originalTextFor(jsonValue)('message').setName('message'))

Message = Optional(Suppress('/')) + (HeaterGraph | Heater
                                     | Check | Publish | Get)


def parse_msg(msg):
    for res in Message.parseString(msg):
        kwargs = {key: res[key] for key in ('action', 'chart',
                                            'topic', 'message', 'retained',
                                            'value', 'op',
                                            'place')
                  if key in res}
        if 'when' in res:
            kwargs['when']= get_past_date(res['when'])
        yield res['cmd'], kwargs


HELP = """
*mqtt* _topic_
*mqtt* [retained] _topic_ _message_
*mqtt check* _topic_ [=|<|>|!=] _value_ _message_
*heater* [status|on|off|manual|wake|away|home|sleep]
*heater graph* _place_ [today|yesterday|weekday]
*garden graph* _place_ [today|yesterday|weekday]
*house graph* _place_ [today|yesterday|weekday]
"""
