# coding: utf-8

from datetime import datetime
from unittest.mock import call, patch, Mock, ANY, MagicMock
import pytest

from . import client


def test_on_connect():
    mqtt = Mock()

    client.on_connect(mqtt, 'userdata', {}, 0)

    assert mqtt.mock_calls == [call.subscribe('#')]


@pytest.mark.parametrize('topic,payload,before,after', [
    ('string', b'bytes', {}, {'string': 'bytes'}),
    ('topic', b'new', {'topic': 'old'}, {'topic': 'new'}),
    ('two', b'b', {'one': 'a'}, {'one': 'a', 'two': 'b'}),
])
def test_on_message(topic, payload, before, after):
    msg = Mock()
    msg.topic = topic
    msg.payload = payload

    with patch.dict(client.LAST_MESSAGE, values=before), \
         patch.object(client, 'heater_db') as logdb:

        client.on_message('client', 'userdata', msg)

        assert client.LAST_MESSAGE == after

        assert logdb.mock_calls == [
            call.insert_new_message(
                ANY, topic, payload.decode('utf-8')),
        ]


def test_on_message_sub():
    msg = Mock()
    msg.topic = 'status/1234'
    msg.payload = b'OFFLINE'

    client._SUBS.clear()
    client._SUBS.append(('status/#', 'heater status', False))

    with patch.dict(client.LAST_MESSAGE, values={}), \
         patch.object(client, 'run_command') as run_command, \
         patch.object(client, 'modules') as modules, \
         patch.object(client, 'heater_db') as logdb:

        modules._answer_callback = ['waiting', 'mod1']

        reply = Mock(callback='callback')
        run_command.return_value = ('iot', [reply])

        client.on_message('client', 'userdata', msg)

        run_command.assert_called_with('heater status')
        assert reply.mock_calls == [call.push(force_notification=True)]
        assert modules._answer_callback == ['waiting', 'mod1']

        assert logdb.mock_calls == [
            call.insert_new_message(ANY, 'status/1234', 'OFFLINE'),
        ]


def test_on_message_sub_value():
    msg = Mock()
    msg.topic = 'topic/path'
    msg.payload = b'value'

    client._SUBS.clear()
    client._SUBS.append(('topic/path', 'cmd $message', False))

    with patch.dict(client.LAST_MESSAGE, values={}), \
         patch.object(client, 'run_command') as run_command, \
         patch.object(client, 'modules') as modules, \
         patch.object(client, 'heater_db') as logdb:

        reply = Mock(callback='callback')
        run_command.return_value = ('iot', [reply])

        client.on_message('client', 'userdata', msg)

        run_command.assert_called_with('cmd value')
        assert reply.mock_calls == [call.push(force_notification=True)]
        assert modules.mock_calls == [
            call.set_callback(reply, interactive=False)
        ]
        assert logdb.mock_calls == [
            call.insert_new_message(ANY, 'topic/path', 'value'),
        ]


def test_on_message_error():
    msg = Mock()
    msg.topic = 'status/1234'
    msg.payload = b'OFFLINE'

    client._SUBS.clear()
    client._SUBS.append(('status/#', 'heater status', False))

    with patch.dict(client.LAST_MESSAGE, values={}), \
         patch.object(client, 'run_command') as run_command, \
         patch.object(client, 'modules') as modules, \
         patch.object(client, 'heater_db') as logdb, \
         patch.object(client, 'Reply') as mock_Reply:

        reply = Mock()
        reply.push.side_effect = Exception('barf')
        run_command.return_value = ('iot', [reply])

        client.on_message('client', 'userdata', msg)

        run_command.assert_called_with('heater status')
        assert reply.mock_calls == [call.push(force_notification=True)]

        assert logdb.mock_calls == [
            call.insert_new_message(ANY, 'status/1234', 'OFFLINE'),
        ]

        assert mock_Reply.mock_calls == [
            call('⚠️iot.on\\_message status/1234: OFFLINE'),
            call().push(force_notification=False)
        ]


def test_on_message_sub_silent():
    msg = Mock()
    msg.topic = 'status/1234'
    msg.payload = b'OFFLINE'

    client._SUBS.clear()
    client._SUBS.append(('status/#', 'silent heater status', False))

    with patch.dict(client.LAST_MESSAGE, values={}), \
         patch.object(client, 'run_command') as run_command, \
         patch.object(client, 'modules') as modules, \
         patch.object(client, 'heater_db') as logdb:

        reply = Mock()
        run_command.return_value = ('iot', [reply])

        client.on_message('client', 'userdata', msg)

        run_command.assert_called_with('heater status')
        assert reply.mock_calls == [call.push(force_notification=False)]

        assert logdb.mock_calls == [
            call.insert_new_message(ANY, 'status/1234', 'OFFLINE'),
        ]


def test_on_message_sub_ignore_repeat():
    msg = Mock()
    msg.topic = 'status/1234'
    msg.payload = b'OFFLINE'

    client._SUBS.clear()
    client._SUBS.append(('status/#', 'heater status', False))

    with patch.dict(client.LAST_MESSAGE, values={'status/1234': 'OFFLINE'}), \
         patch.object(client, 'run_command') as run_command, \
         patch.object(client, 'modules') as modules, \
         patch.object(client, 'heater_db') as logdb:

        client.on_message('client', 'userdata', msg)

        assert run_command.mock_calls == []

        assert logdb.mock_calls == [
            call.insert_new_message(ANY, 'status/1234', 'OFFLINE'),
        ]


def test_on_message_sub_send_repeat():
    msg = Mock()
    msg.topic = 'status/1234'
    msg.payload = b'OFFLINE'

    client._SUBS.clear()
    client._SUBS.append(('status/#', 'heater status', True))

    with patch.dict(client.LAST_MESSAGE, values={'status/1234': 'OFFLINE'}), \
         patch.object(client, 'run_command') as run_command, \
         patch.object(client, 'modules') as modules, \
         patch.object(client, 'heater_db') as logdb:

        reply = Mock()
        run_command.return_value = ('iot', [reply])

        client.on_message('client', 'userdata', msg)

        run_command.assert_called_with('heater status')
        assert reply.mock_calls == [call.push(force_notification=True)]

        assert logdb.mock_calls == [
            call.insert_new_message(ANY, 'status/1234', 'OFFLINE'),
        ]


def test_initialize():
    with patch.object(client, 'config') as mock_config, \
         patch.object(client.mqtt, 'Client') as mock_mqtt:
        mock_config.mqtt_server.value = 'server'
        mock_config.mqtt_port.value = '1234'
        mock_config.mqtt_user.value = None
        mock_config.mqtt_password.value = None

        client.initialize()

        assert mock_mqtt.mock_calls \
            == [call(),
                call().connect('server', 1234, 60),
                call().loop_start()]

        assert client._CLIENT == mock_mqtt()
        assert client._CLIENT.on_connect == client.on_connect
        assert client._CLIENT.on_message == client.on_message

        assert client._SUBS == [
            ('status/#',
             'silent dedupe clear heater heater status', False),
            ('places/taylor-st/rain/raw',
             'dedupe clear rain rain .3 add auto', True),
            ('places/taylor-st/fishtank/button',
             'echo iot 🐠', True),
            ('places/taylor-st/scales/value',
             'weight auto $message', False)]


def test_initialize_pwd():
    with patch.object(client, 'config') as mock_config, \
         patch.object(client.mqtt, 'Client') as mock_mqtt:
        mock_config.mqtt_server.value = 'server'
        mock_config.mqtt_port.value = '1234'
        mock_config.mqtt_user.value = 'uid'
        mock_config.mqtt_password.value = 'pwd'

        client.initialize()

        assert mock_mqtt.mock_calls \
            == [call(),
                call().username_pw_set('uid', password='pwd'),
                call().connect('server', 1234, 60),
                call().loop_start()]

        assert client._CLIENT == mock_mqtt()
        assert client._CLIENT.on_connect == client.on_connect
        assert client._CLIENT.on_message == client.on_message


def test_do_mqtt_pub_not_initialized():
    client._CLIENT = None
    client.do_mqtt_pub('topic', 'message')


def test_do_mqtt_pub():
    with patch.object(client, '_CLIENT') as mock_client:
        client.do_mqtt_pub('topic', 'message')

        assert mock_client.mock_calls == [call.publish('topic', 'message')]


def test_do_mqtt_pub_retained():
    with patch.object(client, '_CLIENT') as mock_client:
        client.do_mqtt_pub('topic', 'message', retained=True)

        assert mock_client.mock_calls == [call.publish('topic', 'message',
                                                       retain=True, qos=1)]


def test_do_mqtt_get_none():
    with patch.dict(client.LAST_MESSAGE, values={}):
        assert client.do_mqtt_get('topic') == 'None'


def test_do_mqtt_get():
    with patch.dict(client.LAST_MESSAGE, values={'topic': 'value'}):
        assert client.do_mqtt_get('topic') == '*topic* value'


def test_do_mqtt_get_escaped():
    with patch.dict(client.LAST_MESSAGE,
                    values={'topic': '{"old_state": "value"}'}):
        assert client.do_mqtt_get('topic') == '*topic* {"old\\_state": "value"}'


def test_do_mqtt_get_wild_none():
    with patch.dict(client.LAST_MESSAGE, values={'one/topic': 'value'}):
        assert client.do_mqtt_get('two/#') == 'None'


def test_do_mqtt_get_wild():
    with patch.dict(client.LAST_MESSAGE, values={'one/topic': 'value'}):
        assert client.do_mqtt_get('one/#') == '*one/topic* value'


def test_do_mqtt_get_many():
    with patch.dict(client.LAST_MESSAGE, values={'one/topic': 'value',
                                                 'one/other': 'msg',
                                                 'two/no': 'match'}):
        assert client.do_mqtt_get('one/#') \
            == '*one/topic* value\n*one/other* msg'


@pytest.mark.parametrize('op,value,message', [
    ('=', '1', None),
    ('=', '2', 'match: topic *2*'),
    ('>', '2', None),
    ('>', '1', 'match: topic *2*'),
])
def test_do_mqtt_check(op, value, message):
    with patch.dict(client.LAST_MESSAGE, values={'topic': '2'}):
        assert client.do_mqtt_check('topic', op, value, '"match"') == message


@pytest.mark.parametrize('op,value,message', [
    ('==', '"fed"', None),
    ('==', '"due"', 'match: topic *"due"*'),
])
def test_do_mqtt_check_string(op, value, message):
    with patch.dict(client.LAST_MESSAGE, values={'topic': '"due"'}):
        assert client.do_mqtt_check('topic', op, value, '"match"') == message


def test_do_mqtt_check_many():
    with patch.dict(client.LAST_MESSAGE, values={'one/topic': '1',
                                                 'one/other': '1',
                                                 'one/no': '2'}):
        assert client.do_mqtt_check('one/#', '<', '2', '"match"') \
            == 'match:\none/topic *1*\none/other *1*'


@pytest.mark.parametrize('mode,state', [
    (None, None),
    ('abc', None),
    (None, 'abc'),
])
def test_do_heater_no_config(mode, state):
    with patch.object(client.config.heater_mode_topic,
                      'get_value') as mock_mode, \
         patch.object(client.config.heater_state_topic,
                      'get_value') as mock_state:
        mock_mode.return_value = mode
        mock_state.return_value = state

        assert client.do_heater('action') == 'Function not enabled.'


@pytest.mark.parametrize('action,calls,sleep', [
    ('status', [], False),
    ('off', [call('heater/mode', '"OFF"', retained=True)], True),
    ('on', [call('heater/mode', '"ON"', retained=True)], True),
    ('manual', [call('heater/mode', '"MANUAL"', retained=True)], True),
    ('wake', [call('heater/state', '"wake"', retained=True)], True),
    ('away', [call('heater/state', '"away"', retained=True)], True),
    ('home', [call('heater/state', '"home"', retained=True)], True),
    ('sleep', [call('heater/state', '"sleep"', retained=True)], True),
])
def test_do_heater(action, calls, sleep):
    with patch.object(client.config.heater_mode_topic,
                      'get_value') as mock_mode, \
         patch.object(client.config.heater_state_topic,
                      'get_value') as mock_state, \
         patch.object(client.config.heater_update_delay,
                      'get_value') as mock_delay:
        mock_mode.return_value = 'heater/mode'
        mock_state.return_value = 'heater/state'
        mock_delay.return_value = '2'

        with patch.object(client, 'do_mqtt_pub') as mock_pub, \
             patch.object(client, '_get_status') as mock_status, \
             patch.object(client, 'time') as mock_time:
            mock_status.return_value = 'heater status'

            assert client.do_heater(action) == 'heater status'
            assert mock_pub.mock_calls == calls

            if sleep:
                assert mock_time.mock_calls == [call.sleep(2)]
            else:
                assert mock_time.mock_calls == []


@pytest.mark.parametrize('messages,status', [
    ({},
     "relay unknown living-room unknown sun-room unknown "
     "kids-bedroom unknown toms-bedroom unknown outside unknown rain unknown "
     "fishtank unknown scales unknown\n"
     "on? *state?* ?°C relay off?\n"
     "living room ?°C kids room ?°C parents room ?°C sun room ?°C "
     "outside ?°C fishtank ?"),

    ({'places/taylor-st/heater/state': '"home"'},
     "relay unknown living-room unknown sun-room unknown "
     "kids-bedroom unknown toms-bedroom unknown outside unknown rain unknown "
     "fishtank unknown scales unknown\n"
     "on? *home* ?°C relay off?\n"
     "living room ?°C kids room ?°C parents room ?°C sun room ?°C "
     "outside ?°C fishtank ?"),

    ({'places/taylor-st/heater/mode': '"OFF"'},
     "relay unknown living-room unknown sun-room unknown "
     "kids-bedroom unknown toms-bedroom unknown outside unknown rain unknown "
     "fishtank unknown scales unknown\n"
     "OFF *state?* ?°C relay off?\n"
     "living room ?°C kids room ?°C parents room ?°C sun room ?°C "
     "outside ?°C fishtank ?"),

    ({'status/4b350c00': 'OFFLINE'},
     "relay OFFLINE living-room unknown sun-room unknown "
     "kids-bedroom unknown toms-bedroom unknown outside unknown rain unknown "
     "fishtank unknown scales unknown\n"
     "on? *state?* ?°C relay off?\n"
     "living room ?°C kids room ?°C parents room ?°C sun room ?°C "
     "outside ?°C fishtank ?"),
])
def test_get_status(messages, status):
    with patch.dict(client.LAST_MESSAGE, values=messages):
        assert client._get_status().message == status


@pytest.mark.parametrize('topics,status', [
    ({},
     {'mode': 'on?', 'state': 'state?', 'kids_bedroom': '?',
      'toms_bedroom': '?', 'living_room': '?',
      'outside': '?', 'target': '?', 'relay': 'off?', 'sun_room': '?',
      'fishtank': '?'}),
    ({'places/taylor-st/heater/mode': '"OFF"',
      'places/taylor-st/heater/state': '"away"',
      'places/taylor-st/heater/target': '0.0',
      'places/taylor-st/heater/relay': '"OFF"',
      'places/taylor-st/living-room/temperature': '18.0',
      'places/taylor-st/sun-room/temperature': '20.0',
      'places/taylor-st/kids-bedroom/temperature': '19.0',
      'places/taylor-st/toms-bedroom/temperature': '19.1',
      'places/taylor-st/outside/temperature': '12.0',
      'places/taylor-st/fishtank/status': '"fed"'},
     {'mode': 'OFF', 'state': 'away', 'kids_bedroom': '19.0',
      'toms_bedroom': '19.1', 'living_room': '18.0',
      'outside': '12.0', 'target': '0.0', 'relay': 'OFF', 'sun_room': '20.0',
      'fishtank': 'fed'}),
    ({'places/taylor-st/heater/mode': '"ON"',
      'places/taylor-st/heater/state': '"home"',
      'places/taylor-st/heater/target': '21.0',
      'places/taylor-st/heater/relay': '"ON"',
      'places/taylor-st/living-room/temperature': '20.0',
      'places/taylor-st/sun-room/temperature': '22.0',
      'places/taylor-st/kids-bedroom/temperature': '21.0',
      'places/taylor-st/toms-bedroom/temperature': '21.111111',
      'places/taylor-st/outside/temperature': '11.0',
      'places/taylor-st/fishtank/status': '"due"'},
     {'mode': 'ON', 'state': 'home', 'kids_bedroom': '21.0',
      'toms_bedroom': '21.1', 'living_room': '20.0',
      'outside': '11.0', 'target': '21.0', 'relay': 'ON', 'sun_room': '22.0',
      'fishtank': 'due'}),
])
def test_status(topics, status):
    with patch.dict(client.LAST_MESSAGE, values=topics):
        assert client._status() == status


@pytest.mark.parametrize('topics,devices,status', [
    ({},
     [],
     ''),
    ({},
     [('name', 'id')],
     'name unknown\n'),
    ({'status/id': 'OFFLINE'},
     [('name', 'id')],
     'name OFFLINE\n'),
    ({'status/id': 'ONLINE 1.0.0'},
     [('name', 'id')],
     ''),
    ({'status/id1': 'ONLINE 1.0.0', 'status/id2': 'ONLINE 1.0.0'},
     [('name1', 'id1'), ('name2', 'id2')],
     ''),
    ({'status/id1': 'OFFLINE', 'status/id2': 'ONLINE 1.0.0'},
     [('name1', 'id1'), ('name2', 'id2')],
     'name1 OFFLINE\n'),
    ({'status/id1': 'ONLINE 1.0.0', 'status/id2': 'OFFLINE'},
     [('name1', 'id1'), ('name2', 'id2')],
     'name2 OFFLINE\n'),
    ({'status/id1': 'OFFLINE', 'status/id2': 'OFFLINE'},
     [('name1', 'id1'), ('name2', 'id2')],
     'name1 OFFLINE name2 OFFLINE\n'),
    ({'status/id1': 'OFFLINE'},
     [('name1', 'id1'), ('name2', 'id2')],
     'name1 OFFLINE name2 unknown\n'),
])
def test_status_offline(topics, devices, status):
    with patch.dict(client.LAST_MESSAGE, values=topics):
        assert client._status_offline(devices) == status


def test_graph_radar_day():
    when = datetime(2001, 2, 3)

    with patch.object(client, 'heater_db') as logdb, \
         patch.object(client, 'heater_charts') as charts, \
         patch.object(client, 'Path') as path:

        conn1 = MagicMock()
        logdb.init_db.side_effect = [conn1, conn1]
        conn = Mock()
        conn1.__enter__.side_effect = [conn, conn]

        charts.radar_day.return_value \
            = '/output/charts/radar_day_2001-02-03.png'

        conn.get_place.return_value = {'place_id': 123,
                                       'key': 'taylor-st', 'name': 'Taylor St',
                                       'latitude': 23.456, 'longitude': 34.567}
        data = Mock()
        conn.get_day_data.return_value = data

        img = Mock()
        path().open.side_effect = [img]

        reply = client.do_graph(when, 'taylor-st', 'radar_day')

        assert reply.photos == [img]

        assert path.mock_calls == [
            # from mock
            call(),
            # from client
            call('/output/charts'),
            call().mkdir(parents=True, exist_ok=True),
            call('/output/charts/radar_day_2001-02-03.png'),
            call().open('rb')]

        assert logdb.mock_calls == [call.init_db(timeout=30),
                                    call.init_db(timeout=30)]

        assert conn.mock_calls == [
            call.get_place('taylor-st'),
            call.extract_data(123, 'taylor-st'),
            call.get_day_data('2001-02-03', 123),
        ]

        assert charts.mock_calls == [
            call.radar_day(data, when, 'taylor-st-day-2001-02-03',
                           'Taylor St', 23.456, 34.567)]


def test_graph_garden_week():
    when = datetime(2001, 2, 3)

    with patch.object(client, 'heater_db') as logdb, \
         patch.object(client, 'heater_charts') as charts, \
         patch.object(client, 'Path') as path:

        conn1 = MagicMock()
        logdb.init_db.side_effect = [conn1, conn1]
        conn = Mock()
        conn1.__enter__.side_effect = [conn, conn]

        charts.garden_week.return_value \
            = '/output/charts/garden_week_2001-02-03.png'

        conn.get_place.return_value = {'place_id': 123,
                                       'key': 'taylor-st', 'name': 'Taylor St',
                                       'latitude': 23.456, 'longitude': 34.567}
        data = Mock()
        conn.get_week_data.return_value = data

        img = Mock()
        path().open.side_effect = [img]

        reply = client.do_graph(when, 'taylor-st', 'garden_week')

        assert reply.photos == [img]

        assert path.mock_calls == [
            # from mock
            call(),
            # from client
            call('/output/charts'),
            call().mkdir(parents=True, exist_ok=True),
            call('/output/charts/garden_week_2001-02-03.png'),
            call().open('rb')]

        assert logdb.mock_calls == [call.init_db(timeout=30),
                                    call.init_db(timeout=30)]

        assert conn.mock_calls == [
            call.get_place('taylor-st'),
            call.extract_data(123, 'taylor-st'),
            call.get_week_data('2001-02-03', 123),
        ]

        assert charts.mock_calls == [
            call.garden_week(data, when, 'taylor-st-week-2001-02-03',
                             'Taylor St', 23.456, 34.567)]


def test_graph_house_history():
    when = datetime(2001, 2, 3)

    with patch.object(client, 'heater_db') as logdb, \
         patch.object(client, 'heater_charts') as charts, \
         patch.object(client, 'Path') as path:

        conn1 = MagicMock()
        logdb.init_db.side_effect = [conn1, conn1]
        conn = Mock()
        conn1.__enter__.side_effect = [conn, conn]

        charts.house_history.return_value \
            = '/output/charts/garden_house_2001-02-03.png'

        conn.get_place.return_value = {'place_id': 123,
                                       'key': 'taylor-st', 'name': 'Taylor St',
                                       'latitude': 23.456, 'longitude': 34.567}
        data = Mock()
        conn.get_house_data.return_value = data

        img = Mock()
        path().open.side_effect = [img]

        reply = client.do_graph(when, 'taylor-st', 'house_history')

        assert reply.photos == [img]

        assert path.mock_calls == [
            # from mock
            call(),
            # from client
            call('/output/charts'),
            call().mkdir(parents=True, exist_ok=True),
            call('/output/charts/garden_house_2001-02-03.png'),
            call().open('rb')]

        assert logdb.mock_calls == [call.init_db(timeout=30),
                                    call.init_db(timeout=30)]

        assert conn.mock_calls == [
            call.get_place('taylor-st'),
            call.extract_data(123, 'taylor-st'),
            call.get_house_data('2001-02-03', 123),
        ]

        assert charts.mock_calls == [
            call.house_history(data, when, 'taylor-st-house-2001-02-03',
                               'Taylor St')]
