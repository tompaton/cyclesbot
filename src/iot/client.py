# coding: utf-8

import json
import operator
import time

from datetime import datetime
from pathlib import Path
from typing import Any, Dict, List, Optional, Tuple
import paho.mqtt.client as mqtt

from chat.modules import run_command
from chat import modules
from chat.msg_types import Reply, Photo
from common.util import escape_markdown

from heater_logs import database as heater_db
from heater_logs import charts as heater_charts

from . import config


_CLIENT = None  # type: Optional[mqtt.Client]

# topic --> last message
LAST_MESSAGE = {}  # type: Dict[str, str]

# list of (topic, command)
_SUBS = []  # type: List[Tuple[str, str, bool]]


def on_connect(client, userdata, flags, rc):
    # Subscribing in on_connect() means that if we lose the connection and
    # reconnect then subscriptions will be renewed.
    client.subscribe("#")


def on_message(client, userdata, msg):
    message = msg.payload.decode('utf-8')

    heater_db.insert_new_message(datetime.now(), msg.topic, message)

    old_message = LAST_MESSAGE.get(msg.topic)
    LAST_MESSAGE[msg.topic] = message

    # trigger event handlers for particular topics
    for sub, command, repeats in _SUBS:
        if mqtt.topic_matches_sub(sub, msg.topic):
            if message == old_message and not repeats:
                continue

            print(f'iot.on_message {msg.topic}: {message}')

            # TODO: build this into run_command/reply.push somehow
            if command.startswith('silent '):
                force_notification = False
                command = command[7:]
            else:
                force_notification = True

            # sub message into command
            command = command.replace('$message', message)

            try:
                for reply in run_command(command)[1]:
                    reply.push(force_notification=force_notification)
                    modules.set_callback(reply, interactive=False)
            except Exception as ex:
                print(ex)
                Reply(escape_markdown(f'⚠️iot.on_message {msg.topic}: {message}')) \
                    .push(force_notification=False)


def initialize():
    _SUBS.clear()
    _SUBS.extend(_load_subs())

    client = mqtt.Client()

    client.on_connect = on_connect
    client.on_message = on_message

    if config.mqtt_user.value and config.mqtt_password.value:
        client.username_pw_set(config.mqtt_user.value,
                               password=config.mqtt_password.value)

    client.connect(config.mqtt_server.value,
                   int(config.mqtt_port.value), 60)

    client.loop_start()

    global _CLIENT
    _CLIENT = client


def do_mqtt_get(topic):
    reply = []

    for key, msg in _mqtt_get(topic):
        reply.append(f'*{key}* {escape_markdown(msg)}')

    return '\n'.join(reply or ['None'])


def _mqtt_get(topic):
    for key, msg in LAST_MESSAGE.items():
        if mqtt.topic_matches_sub(topic, key):
            yield key, msg


def do_mqtt_pub(topic, message, retained=False):
    if retained:
        kwargs = {'qos': 1, 'retain': True}
    else:
        kwargs = {}

    if _CLIENT is not None:
        _CLIENT.publish(topic, message, **kwargs)


def _load_subs():  # type: () -> List[Tuple[str, str, bool]]
    # TODO: load from table
    # TODO: ('error/#', 'mqtt error/#')
    return [('status/#',
             'silent dedupe clear heater heater status', False),
            # ('places/taylor-st/rain/raw', 'echo 💧', True),
            ('places/taylor-st/rain/raw',
             'dedupe clear rain rain .3 add auto', True),
            ('places/taylor-st/fishtank/button',
             'echo iot 🐠', True),
            ('places/taylor-st/scales/value',
             'weight auto $message', False)]


def do_mqtt_check(topic, op, value, message):
    reply = []

    op = {'=': operator.eq,
          '==': operator.eq,
          '!=': operator.ne,
          '<': operator.lt,
          '>': operator.gt}[op]
    value = json.loads(value)

    for key, msg in _mqtt_get(topic):
        if op(json.loads(msg), value):
            reply.append(f'{key} *{msg}*')

    if reply:
        sep = '\n' if len(reply) > 1 else ' '
        replies = '\n'.join(reply)
        return f'{json.loads(message)}:{sep}{replies}'


# TODO: break these out into their own module
# (or define as emoji macros?)


@config.heater_mode_topic.required()
@config.heater_state_topic.required()
def do_heater(action):
    if action == 'on':
        do_mqtt_pub(config.heater_mode_topic.value, '"ON"', retained=True)

    elif action == 'off':
        do_mqtt_pub(config.heater_mode_topic.value, '"OFF"', retained=True)

    elif action == 'manual':
        do_mqtt_pub(config.heater_mode_topic.value, '"MANUAL"', retained=True)

    elif action != 'status':
        do_mqtt_pub(config.heater_state_topic.value, '"{}"'.format(action),
                    retained=True)

    if action != 'status':
        # allow state to propogate to target etc.
        time.sleep(int(config.heater_update_delay.value))

    return _get_status()


def _get_status():
    offline = _status_offline([('relay', '4b350c00'),
                               ('living-room', '01a05000'),
                               ('sun-room', '33a15000'),
                               ('kids-bedroom', '6ea15000'),
                               ('toms-bedroom', '9f9d4a00'),
                               ('outside', '18a15000'),
                               ('rain', '2fa05000'),
                               ('fishtank', 'c3011c00'),
                               ('scales', '06613100')])

    return Reply('{offline}'
                 '{mode} *{state}* {target}°C relay {relay}\n'
                 'living room {living_room}°C '
                 'kids room {kids_bedroom}°C '
                 'parents room {toms_bedroom}°C '
                 'sun room {sun_room}°C '
                 'outside {outside}°C '
                 'fishtank {fishtank}'
                 .format(offline=offline, **_status()))


def _status():
    # TODO: re-arrange topics so that wildcards could be used
    # places/taylor-st/heater/+
    # places/taylor-st/+/temperature
    # places/taylor-st/+/status

    def get(path, missing='?'):
        return LAST_MESSAGE.get('places/taylor-st/' + path, missing).strip('"')

    def getf(path):
        t = get(path)
        try:
            t = float(t)
        except (ValueError, TypeError):
            return t
        else:
            return str(round(t, 1))

    return {'mode': get('heater/mode', 'on?'),
            'state': get('heater/state', 'state?'),
            'target': get('heater/target'),
            'relay': get('heater/relay', 'off?'),
            'living_room': getf('living-room/temperature'),
            'sun_room': getf('sun-room/temperature'),
            'kids_bedroom': getf('kids-bedroom/temperature'),
            'toms_bedroom': getf('toms-bedroom/temperature'),
            'outside': getf('outside/temperature'),
            'fishtank': get('fishtank/status')}


def _status_offline(devices):
    offline = []
    for name, device in devices:
        status = LAST_MESSAGE.get('status/{}'.format(device), 'unknown')
        if 'ONLINE' not in status:
            offline.append('{} {}'.format(name, status))

    if offline:
        return ' '.join(offline) + '\n'
    else:
        return ''


def do_graph(when, place, chart):
    with heater_db.init_db(timeout=30) as conn:
        place_row = conn.get_place(place)

        conn.extract_data(place_row['place_id'], place_row['key'])

    Path('/output/charts').mkdir(parents=True, exist_ok=True)

    date = when.strftime('%Y-%m-%d')
    with heater_db.init_db(timeout=30) as conn:
        if chart == 'radar_day':
            day_data = conn.get_day_data(date, place_row['place_id'])

            fn = heater_charts.radar_day(day_data, when,
                                         f'{place_row["key"]}-day-{date}',
                                         place_row['name'],
                                         place_row['latitude'],
                                         place_row['longitude'])

        elif chart == 'garden_week':
            week_data = conn.get_week_data(date, place_row['place_id'])

            fn = heater_charts.garden_week(week_data, when,
                                           f'{place_row["key"]}-week-{date}',
                                           place_row['name'],
                                           place_row['latitude'],
                                           place_row['longitude'])

        elif chart == 'house_history':
            house_data = conn.get_house_data(date, place_row['place_id'])

            fn = heater_charts.house_history(house_data, when,
                                             f'{place_row["key"]}-house-{date}',
                                             place_row['name'])

    return Photo([Path(fn).open('rb')])
