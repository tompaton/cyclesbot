from config.var_types import ConfigParam

weight_secret = ConfigParam('Weight log secret')
waist_secret = ConfigParam('Waist log secret')
waist_count = ConfigParam(name='Waist readings per month', default='5')

weight_min = ConfigParam('Unitless weight range min', default='70.0')
weight_max = ConfigParam('Unitless weight range max', default='75.0')
