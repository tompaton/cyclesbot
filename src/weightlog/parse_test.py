import pytest
from pyparsing import ParseException
from flexmock import flexmock
from . import client
from . import parse_msg


@pytest.fixture
def config_min_max():
    flexmock(client.config.weight_min, get_value='70.0')
    flexmock(client.config.weight_max, get_value='75.0')


@pytest.mark.parametrize('msg,cmd,kwargs', [
    # test log
    ('weight log', 'readings', {'name': 'weight'}),
    ('waist log', 'readings', {'name': 'waist'}),

    # test log count
    ('weight log 21', 'readings', {'name': 'weight', 'count': 21}),
    ('waist log 21', 'readings', {'name': 'waist', 'count': 21}),

    # test weight
    ('weight 75kg', 'reading', {'weight': 75}),
    ('weight 75 kg', 'reading', {'weight': 75}),
    ('Weight 75Kg', 'reading', {'weight': 75}),
    ('weight 75', 'reading', {'weight': 75}),
    ('weight 75.5', 'reading', {'weight': 75.5}),

    # implicit weight
    ('72.5', 'reading', {'weight': 72.5}),
    ('75Kg', 'reading', {'weight': 75}),
    ('75 kg', 'reading', {'weight': 75}),
    ('75.5kg', 'reading', {'weight': 75.5}),

    # smart scales
    ('weight auto 75123', 'auto_reading', {'weight': 75123}),
    ('weight auto -75', 'auto_reading', {'weight': -75}),

    # waist
    ('waist 83cm', 'reading', {'waist': 83}),
    ('waist 83 cm', 'reading', {'waist': 83}),

    # implicit waist
    ('83cm', 'reading', {'waist': 83}),
    ('83 cm', 'reading', {'waist': 83}),

    # login
    ('weight login', 'url', {'name': 'weight'}),
    ('waist login', 'url', {'name': 'waist'}),
])
def test_log(msg, cmd, kwargs, config_min_max):
    assert list(parse_msg(msg)) == [(cmd, kwargs)]


@pytest.mark.parametrize('msg', [
    '65.2',
    '75.2',
])
def test_weight_out_of_range(config_min_max, msg):
    with pytest.raises(ParseException):
        list(parse_msg(msg))
