from datetime import date

from weightlog_client import WeightLog, PostError

from chat.msg_types import Reply, Question
from . import config


def _log(name):
    if name.lower() == 'weight':
        return WeightLog(config.weight_secret.value, 'Weight', 'kg')
    elif name.lower() == 'waist':
        return WeightLog(config.waist_secret.value, 'Waist', 'cm')
    else:
        raise ValueError(name)


def do_readings(name, count=10):
    log = _log(name)
    readings = ['{} {}{}'.format(*reading)
                for reading in log.get_readings(count)]
    return Reply('*{}*\n{}'.format(log.title, '\n'.join(readings)))


def _logged(name, count):
    this_month = _today().strftime('%Y-%m-')
    rows = _log(name).get_readings()
    return len([row for row in rows if row[0].startswith(this_month)]) >= count


def _today():  # pragma: no cover
    return date.today()


def do_auto_reading(weight=None):
    weight = round(weight / 1000.0, 1)

    if float(config.weight_min.value) < weight <= float(config.weight_max.value):
        return do_reading(weight=weight)

    # TODO: calibration mode

    if weight < 1:
        # zero - ignore for now
        pass
    else:
        return Reply('Scale weight: {weight} kg'.format(weight=weight))


def do_reading(weight=None, waist=None):

    if weight is not None:
        reply = _log_reading(_log('weight'), reading=weight)
        if reply and not _logged('waist', count=int(config.waist_count.value)):
            def _waist(text):
                try:
                    amount = float(text)
                except:
                    pass
                else:
                    return _log_reading(_log('waist'), reading=amount)
            return reply.then(Question('Waist measurement? (cm)', _waist))
        else:
            return reply

    elif waist is not None:
        return _log_reading(_log('waist'), reading=waist)

    else:
        raise ValueError('No reading!')


def _log_reading(log, reading):
    when = _today()
    try:
        log.log_reading(when, reading)
    except PostError as ex:
        print(ex)
    else:
        return Reply('Logged {} *{}{}* for {}'
                     .format(log.title.lower(), reading, log.unit,
                             when.strftime('%a %d %b %Y')))


def do_url(name):
    return Reply(_log(name).get_url(), parse_mode=None)
