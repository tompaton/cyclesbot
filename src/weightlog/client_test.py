# coding: utf-8

import pytest
from datetime import date
from flexmock import flexmock


@pytest.fixture
def client():
    from . import client
    flexmock(client.config.weight_secret, get_value='secret1')
    flexmock(client.config.waist_secret, get_value='secret2')
    flexmock(client.config.waist_count, get_value=5)

    flexmock(client.config.weight_min, get_value='70.0')
    flexmock(client.config.weight_max, get_value='75.0')

    flexmock(client).should_receive('_today').and_return(date(2009, 10, 4))

    return client


def test_readings(client):
    (flexmock(client.WeightLog)
     .should_receive('get_readings')
     .with_args(10)
     .and_return([client.WeightLog.Reading('2009-10-01', '76.60', 'kg'),
                  client.WeightLog.Reading('2009-10-02', '77.60', 'kg'),
                  client.WeightLog.Reading('2009-10-03', '77.10', 'kg')]))

    assert client.do_readings('weight').message \
        == ('*Weight*\n'
            '2009-10-01 76.60kg\n'
            '2009-10-02 77.60kg\n'
            '2009-10-03 77.10kg')


def test_readings_waist(client):
    (flexmock(client.WeightLog)
     .should_receive('get_readings')
     .with_args(10)
     .and_return([client.WeightLog.Reading('2009-10-01', '81.00', 'cm'),
                  client.WeightLog.Reading('2009-10-02', '83.00', 'cm'),
                  client.WeightLog.Reading('2009-10-03', '82.00', 'cm')]))

    assert client.do_readings('waist').message \
        == ('*Waist*\n'
            '2009-10-01 81.00cm\n'
            '2009-10-02 83.00cm\n'
            '2009-10-03 82.00cm')


def test_readings_count(client):
    (flexmock(client.WeightLog)
     .should_receive('get_readings')
     .with_args(2)
     .and_return([client.WeightLog.Reading('2009-10-02', '77.60', 'kg'),
                  client.WeightLog.Reading('2009-10-03', '77.10', 'kg')]))

    assert client.do_readings('weight', count=2).message \
        == ('*Weight*\n'
            '2009-10-02 77.60kg\n'
            '2009-10-03 77.10kg')


def test_reading_waist(client):
    (flexmock(client.WeightLog)
     .should_receive('log_reading')
     .with_args(date(2009, 10, 4), 80)
     .and_return(client.WeightLog.Reading('2009-10-04', '80', 'cm')))

    assert client.do_reading(waist=80).message \
        == 'Logged waist *80cm* for Sun 04 Oct 2009'


def test_reading_post_error(client):
    (flexmock(client.WeightLog)
     .should_receive('log_reading')
     .with_args(date(2009, 10, 4), 80)
     .and_raise(client.PostError, 'error'))

    assert client.do_reading(waist=80) is None


def test_reading_waist_logged(client):
    (flexmock(client.WeightLog)
     .should_receive('log_reading')
     .with_args(date(2009, 10, 4), 75)
     .and_return(client.WeightLog.Reading('2009-10-04', '75', 'kg')))

    (flexmock(client.WeightLog)
     .should_receive('get_readings')
     .with_args()
     .and_return([client.WeightLog.Reading('2009-10-01', '81.00', 'cm'),
                  client.WeightLog.Reading('2009-10-02', '82.00', 'cm'),
                  client.WeightLog.Reading('2009-10-03', '83.00', 'cm'),
                  client.WeightLog.Reading('2009-10-04', '84.00', 'cm'),
                  client.WeightLog.Reading('2009-10-05', '85.00', 'cm')]))

    reply = client.do_reading(weight=75)
    assert reply.message == 'Logged weight *75kg* for Sun 04 Oct 2009'

    assert reply.callback is None


def test_reading(client):
    (flexmock(client.WeightLog)
     .should_receive('log_reading')
     .with_args(date(2009, 10, 4), 75)
     .and_return(client.WeightLog.Reading('2009-10-04', '75', 'kg')))

    (flexmock(client.WeightLog)
     .should_receive('get_readings')
     .with_args()
     .and_return([client.WeightLog.Reading('2009-10-01', '81.00', 'cm'),
                  client.WeightLog.Reading('2009-10-02', '82.00', 'cm'),
                  client.WeightLog.Reading('2009-10-03', '83.00', 'cm'),
                  client.WeightLog.Reading('2009-10-04', '84.00', 'cm')]))

    reply1 = client.do_reading(weight=75)
    assert reply1.replies[0].message \
        == 'Logged weight *75kg* for Sun 04 Oct 2009'
    assert reply1.replies[1].message == 'Waist measurement? (cm)'

    (flexmock(client.WeightLog)
     .should_receive('log_reading')
     .with_args(date(2009, 10, 4), 85)
     .and_return(client.WeightLog.Reading('2009-10-04', '85.0', 'cm')))

    # not actually calling .send() so have to find callback
    reply2 = reply1.replies[-1].callback(85)

    assert reply2.message == 'Logged waist *85.0cm* for Sun 04 Oct 2009'

    assert reply2.callback is None


def test_reading_no_waist(client):
    (flexmock(client.WeightLog)
     .should_receive('log_reading')
     .with_args(date(2009, 10, 4), 75)
     .and_return(client.WeightLog.Reading('2009-10-04', '75', 'kg')))

    (flexmock(client.WeightLog)
     .should_receive('get_readings')
     .with_args()
     .and_return([client.WeightLog.Reading('2009-10-01', '81.00', 'cm'),
                  client.WeightLog.Reading('2009-10-02', '82.00', 'cm'),
                  client.WeightLog.Reading('2009-10-03', '83.00', 'cm'),
                  client.WeightLog.Reading('2009-10-04', '84.00', 'cm')]))

    reply1 = client.do_reading(weight=75)
    assert reply1.replies[0].message \
        == 'Logged weight *75kg* for Sun 04 Oct 2009'
    assert reply1.replies[1].message == 'Waist measurement? (cm)'

    # not actually calling .send() so have to find callback
    reply2 = reply1.replies[-1].callback('tick run')

    assert reply2 is None


def test_readings_unknown(client):
    with pytest.raises(ValueError):
        client.do_readings('unknown')


def test_reading_unknown(client):
    with pytest.raises(ValueError):
        client.do_reading()


def test_login(client):
    assert client.do_url('weight').message \
        == 'https://weightlog.tompaton.com/secret1'
    assert client.do_url('waist').message \
        == 'https://weightlog.tompaton.com/secret2'


def test_auto_reading_weight(client):
    (flexmock(client)
     .should_receive('do_reading')
     .with_args(weight=71.2)
     .and_return('reply'))

    reply = client.do_auto_reading(weight=71234)

    assert reply == 'reply'


def test_auto_reading_zero(client):
    assert client.do_auto_reading(weight=751) is None
    assert client.do_auto_reading(weight=-751) is None


def test_auto_reading_other(client):
    assert client.do_auto_reading(weight=1751).message == 'Scale weight: 1.8 kg'
