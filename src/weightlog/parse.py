from pyparsing import Group, oneOf, Optional

from common.parse import keyword, number

from . import config

Weight = number('weight').setName('measurement')
Waist = number('waist').setName('measurement')
Name = oneOf('weight waist', caseless=True)('name')

UrlCommand = Group(Name + keyword('login', action='url')('action'))

WeightLog = Group(Name + keyword('log', action='readings')('action')
                  + Optional(number('count')))

WeightMeasurement1 = Group(keyword('weight', action='reading')('action')
                           + Weight + Optional(keyword('kg')))

WeightMeasurementAuto = Group(keyword('weight', 'auto',
                                      action='auto_reading')('action')
                              + Weight)

WeightMeasurement2 = Group(Weight + keyword('kg', action='reading')('action'))

WaistMeasurement1 = Group(keyword('waist', action='reading')('action')
                          + Waist + Optional(keyword('cm')))

WaistMeasurement2 = Group(Waist + keyword('cm', action='reading')('action'))

Message = (UrlCommand | WeightLog
           | WeightMeasurementAuto
           | WeightMeasurement1 | WeightMeasurement2
           | WaistMeasurement1 | WaistMeasurement2)


def parse_msg(msg):
    try:
        value = float(msg)
        if float(config.weight_min.value) < value <= float(config.weight_max.value):
            yield 'reading', {'weight': value}
            return
    except:
        pass

    for res in Message.parseString(msg, parseAll=True):
        kwargs = {key: res[key] for key in ('weight', 'waist', 'name', 'count')
                  if key in res}
        yield res['action'], kwargs


HELP = """
*weight log* [count]
*weight* _measurement_ [kg]
*weight auto* _measurement_
_measurement_ [kg]
*waist log* [count]
*waist* _measurement_ [cm]
_measurement_ [cm]
[weight|waist] *login*
"""
