import sqlite3
import feedparser
import requests

from bs4 import BeautifulSoup

from chat.msg_types import ReplyList, Reply, Photo


_ETAG = {}
_LAST_MODIFIED = {}


def do_rss(url):
    with get_conn() as db:
        title, link, entries = _get_rss(url)
        replies = [reply for entry in entries
                   if _new_entry(db, getattr(entry, 'id', None)
                                 or getattr(entry, 'link', None))
                   for reply in _entry_replies(entry)]

        if replies:
            replies.insert(0, Reply(f'<a href="{link}">{title}</a>' if link
                                    else title,
                                    parse_mode='HTML',
                                    disable_web_page_preview=True,
                                    disable_notification=True))

        return ReplyList(*replies)


def get_conn():
    conn = sqlite3.connect('/data/rss.db')
    conn.execute("""CREATE TABLE IF NOT EXISTS rss (guid text)""")
    return conn


def _get_rss(url):
    headers = {'User-Agent': 'tompaton.com/1.0'}

    if _ETAG.get(url):
        headers['If-None-Match'] = _ETAG.get(url)
    elif _LAST_MODIFIED.get(url):
        headers['If-Modified-Since'] = _LAST_MODIFIED.get(url)

    res = requests.get(url, headers=headers, timeout=20)

    if res.status_code == 200:
        etag = res.headers.get("ETag")
        if etag:
            _ETAG[url] = etag

        lmod = res.headers.get("Last-Modified")
        if lmod:
            _LAST_MODIFIED[url] = lmod

        feed = feedparser.parse(res.text)

        if feed.feed.has_key('link'):
            link = feed.feed.link
        else:
            link = None

        return feed.feed.title, link, feed.entries

    return url, None, []


def _new_entry(db, guid):
    cur = db.cursor()
    cur.execute('SELECT guid FROM rss WHERE guid = ?', (guid,))
    if any(cur.fetchall()):
        return False

    cur = db.cursor()
    cur.execute('INSERT INTO rss (guid) VALUES (?)', (guid,))

    return True


def _entry_replies(entry):
    summary = (getattr(entry, 'description', None)
               or getattr(entry, 'summary', None)
               or getattr(entry, 'content', None)
               or '')
    images = []

    soup = BeautifulSoup(str(summary), 'html.parser')

    for img in soup.select('img'):
        if img.get('src'):
            images.append(img['src'])
            img.replace_with(f'[{img.get("alt") or "img"}]')

    for br in soup.select('br'):
        br.replace_with('\n')

    for h in soup.select('h1,h2,h3,h4,h5'):
        h.name = 'b'

    for p in soup.select('p,div'):
        p.unwrap()

    for image in images:
        yield Photo([image], disable_notification=True)

    link = getattr(entry, 'link', None)
    title = getattr(entry, 'title', None)
    yield Reply(f'<a href="{link}">{title}</a>\n{soup}',
                parse_mode='HTML', disable_web_page_preview=True,
                disable_notification=True)
