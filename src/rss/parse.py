from pyparsing import Group
from common.parse import keyword, Url

RSSCommand = Group(keyword('rss')('action') + Url('url'))

Message = RSSCommand


def parse_msg(msg):
    for res in Message.parseString(msg, parseAll=True):
        kwargs = {key: res[key] for key in ('url',)
                  if key in res}
        yield res['action'], kwargs


HELP = """
*rss* _url_
"""
