import pytest
from unittest.mock import patch, call, Mock

from common.test import mock_contextmanager
from . import client


@patch('rss.client.get_conn')
@patch('rss.client._get_rss')
def test_rss(mock_get_rss, mock_get_conn):
    old_entry = Mock(id='old')
    one_entry = Mock(spec_set=['link', 'title', 'description', 'summary'],
                     link='new1-url',
                     title='New One',
                     description=None,
                     summary='no image')
    img_entry = Mock(id='new2',
                     link='new2-url',
                     title='New Two',
                     description='new2 <img src="new2-src" alt="img" />')
    mock_get_rss.return_value = ('title', None, [old_entry, one_entry, img_entry])

    db = Mock()
    mock_get_conn.return_value = mock_contextmanager(db)()
    db.cursor.return_value = cur = Mock()

    cur.fetchall.side_effect = [['old'], [], []]

    replies = client.do_rss('url')

    assert len(replies.replies) == 4
    assert replies.replies[0].message == 'title'
    assert replies.replies[0].send_args == {'disable_notification': True,
                                            'disable_web_page_preview': True,
                                            'parse_mode': 'HTML'}
    assert replies.replies[1].message \
        == '<a href="new1-url">New One</a>\nno image'
    assert replies.replies[2].photos == ['new2-src']
    assert replies.replies[3].message \
        == '<a href="new2-url">New Two</a>\nnew2 [img]'

    assert mock_get_rss.mock_calls == [call('url')]

    assert db.mock_calls == [
        call.cursor(),
        call.cursor().execute('SELECT guid FROM rss WHERE guid = ?',
                              ('old',)),
        call.cursor().fetchall(),
        call.cursor(),
        call.cursor().execute('SELECT guid FROM rss WHERE guid = ?',
                              ('new1-url',)),
        call.cursor().fetchall(),
        call.cursor(),
        call.cursor().execute('INSERT INTO rss (guid) VALUES (?)',
                              ('new1-url',)),
        call.cursor(),
        call.cursor().execute('SELECT guid FROM rss WHERE guid = ?',
                              ('new2',)),
        call.cursor().fetchall(),
        call.cursor(),
        call.cursor().execute('INSERT INTO rss (guid) VALUES (?)',
                              ('new2',))]


@patch('rss.client.get_conn')
@patch('rss.client._get_rss')
def test_rss_link(mock_get_rss, mock_get_conn):
    one_entry = Mock(spec_set=['link', 'title', 'description', 'summary'],
                     link='new1-url',
                     title='New One',
                     description=None,
                     summary='no image')
    mock_get_rss.return_value = ('title', 'site-url', [one_entry])

    db = Mock()
    mock_get_conn.return_value = mock_contextmanager(db)()
    db.cursor.return_value = cur = Mock()

    cur.fetchall.return_value = []

    replies = client.do_rss('url')

    assert len(replies.replies) == 2
    assert replies.replies[0].message == '<a href="site-url">title</a>'
    assert replies.replies[0].send_args == {'disable_notification': True,
                                            'disable_web_page_preview': True,
                                            'parse_mode': 'HTML'}
    assert replies.replies[1].message \
        == '<a href="new1-url">New One</a>\nno image'

    assert mock_get_rss.mock_calls == [call('url')]

    assert db.mock_calls == [
        call.cursor(),
        call.cursor().execute('SELECT guid FROM rss WHERE guid = ?',
                              ('new1-url',)),
        call.cursor().fetchall(),
        call.cursor(),
        call.cursor().execute('INSERT INTO rss (guid) VALUES (?)',
                              ('new1-url',))]


@patch('rss.client.get_conn')
@patch('rss.client._get_rss')
def test_rss_empty(mock_get_rss, mock_get_conn):
    mock_get_rss.return_value = ('title', None, [])

    db = Mock()
    mock_get_conn.return_value = mock_contextmanager(db)()

    replies = client.do_rss('url')

    assert len(replies.replies) == 0

    assert mock_get_rss.mock_calls == [call('url')]

    assert db.mock_calls == []


@patch('rss.client.sqlite3')
def test_get_conn(mock_sqlite3):
    mock_sqlite3.connect.return_value = conn = Mock()
    assert client.get_conn() == conn
    assert mock_sqlite3.mock_calls == [
        call.connect('/data/rss.db'),
        call.connect().execute('CREATE TABLE IF NOT EXISTS rss (guid text)')]


@patch('rss.client.requests')
def test_get_rss_404(mock_requests):
    client._ETAG.clear()
    client._LAST_MODIFIED.clear()

    mock_requests.get.return_value = res = Mock(status_code=404)

    assert client._get_rss('url') == ('url', None, [])

    assert client._ETAG == {}
    assert client._LAST_MODIFIED == {}

    assert mock_requests.mock_calls == [
        call.get('url',
                 headers={'User-Agent': 'tompaton.com/1.0'},
                 timeout=20)]


@patch('rss.client.requests')
@patch('rss.client.feedparser')
def test_get_rss_etag(mock_feedparser, mock_requests):
    client._ETAG.clear()
    client._LAST_MODIFIED.clear()

    client._ETAG['url'] = 'etag1'

    mock_requests.get.return_value \
        = res = Mock(status_code=200,
                     text='response',
                     headers={'ETag': 'etag2'})

    mock_feedparser.parse.return_value = feed = Mock(feed=Mock(title='title'),
                                                     entries=['entry'])
    feed.feed.has_key.return_value = False

    assert client._get_rss('url') == ('title', None, ['entry'])

    assert client._ETAG == {'url': 'etag2'}
    assert client._LAST_MODIFIED == {}

    assert mock_requests.mock_calls == [
        call.get('url',
                 headers={'User-Agent': 'tompaton.com/1.0',
                          'If-None-Match': 'etag1'},
                 timeout=20)]

    assert mock_feedparser.mock_calls == [call.parse('response'),
                                          call.parse().feed.has_key('link')]


@patch('rss.client.requests')
@patch('rss.client.feedparser')
def test_get_rss_last_modified(mock_feedparser, mock_requests):
    client._ETAG.clear()
    client._LAST_MODIFIED.clear()

    client._LAST_MODIFIED['url'] = '2001-01-01'

    mock_requests.get.return_value \
        = res = Mock(status_code=200,
                     text='response',
                     headers={'Last-Modified': '2002-02-02'})

    mock_feedparser.parse.return_value = feed = Mock(feed=Mock(title='title'),
                                                     entries=['entry'])
    feed.feed.has_key.return_value = False

    assert client._get_rss('url') == ('title', None, ['entry'])

    assert client._ETAG == {}
    assert client._LAST_MODIFIED == {'url': '2002-02-02'}

    assert mock_requests.mock_calls == [
        call.get('url',
                 headers={'User-Agent': 'tompaton.com/1.0',
                          'If-Modified-Since': '2001-01-01'},
                 timeout=20)]

    assert mock_feedparser.mock_calls == [call.parse('response'),
                                          call.parse().feed.has_key('link')]


@patch('rss.client.requests')
@patch('rss.client.feedparser')
def test_get_rss_link(mock_feedparser, mock_requests):
    client._ETAG.clear()
    client._LAST_MODIFIED.clear()

    client._LAST_MODIFIED['url'] = '2001-01-01'

    mock_requests.get.return_value \
        = res = Mock(status_code=200,
                     text='response',
                     headers={'Last-Modified': '2002-02-02'})

    mock_feedparser.parse.return_value = feed = Mock(feed=Mock(title='title',
                                                               link='site-url'),
                                                     entries=['entry'])
    feed.feed.has_key.return_value = True

    assert client._get_rss('url') == ('title', 'site-url', ['entry'])

    assert client._ETAG == {}
    assert client._LAST_MODIFIED == {'url': '2002-02-02'}

    assert mock_requests.mock_calls == [
        call.get('url',
                 headers={'User-Agent': 'tompaton.com/1.0',
                          'If-Modified-Since': '2001-01-01'},
                 timeout=20)]

    assert mock_feedparser.mock_calls == [call.parse('response'),
                                          call.parse().feed.has_key('link')]


def test_new_entry_true():
    db = Mock()
    db.cursor.return_value = cur = Mock()
    cur.fetchall.return_value = []

    assert client._new_entry(db, 'guid')

    assert db.mock_calls == [
        call.cursor(),
        call.cursor().execute('SELECT guid FROM rss WHERE guid = ?',
                              ('guid',)),
        call.cursor().fetchall(),
        call.cursor(),
        call.cursor().execute('INSERT INTO rss (guid) VALUES (?)',
                              ('guid',))]


def test_new_entry_False():
    db = Mock()
    db.cursor.return_value = cur = Mock()
    cur.fetchall.return_value = ['guid']

    assert not client._new_entry(db, 'guid')

    assert db.mock_calls == [
        call.cursor(),
        call.cursor().execute('SELECT guid FROM rss WHERE guid = ?',
                              ('guid',)),
        call.cursor().fetchall()]


def test_entry_replies():
    entry = Mock(link='entry-link',
                 title='Entry Title',
                 description='Entry description')
    replies = list(client._entry_replies(entry))
    assert len(replies) == 1
    assert replies[0].message \
        == '<a href="entry-link">Entry Title</a>\nEntry description'
    assert replies[0].send_args == {'parse_mode': 'HTML',
                                    'disable_notification': True,
                                    'disable_web_page_preview': True}


def test_entry_replies_no_summary():
    entry = Mock(link='entry-link',
                 title='Entry Title',
                 description='',
                 summary='',
                 content='')
    replies = list(client._entry_replies(entry))
    assert len(replies) == 1
    assert replies[0].message \
        == '<a href="entry-link">Entry Title</a>\n'
    assert replies[0].send_args == {'parse_mode': 'HTML',
                                    'disable_notification': True,
                                    'disable_web_page_preview': True}


def test_entry_replies_p():
    entry = Mock(link='entry-link',
                 title='Entry Title',
                 description='<p><a href="url">img</a> desc</p><br clear="all"/>')
    replies = list(client._entry_replies(entry))
    assert len(replies) == 1
    assert replies[0].message \
        == ('<a href="entry-link">Entry Title</a>\n'
            '<a href="url">img</a> desc\n')
    assert replies[0].send_args == {'parse_mode': 'HTML',
                                    'disable_notification': True,
                                    'disable_web_page_preview': True}


def test_entry_replies_div():
    entry = Mock(link='entry-link',
                 title='Entry Title',
                 description='<div><a href="url">img</a> desc</div><br clear="all"/>')
    replies = list(client._entry_replies(entry))
    assert len(replies) == 1
    assert replies[0].message \
        == ('<a href="entry-link">Entry Title</a>\n'
            '<a href="url">img</a> desc\n')
    assert replies[0].send_args == {'parse_mode': 'HTML',
                                    'disable_notification': True,
                                    'disable_web_page_preview': True}


def test_entry_replies_h2():
    entry = Mock(link='entry-link',
                 title='Entry Title',
                 description='<h2>title</h2> <a href="url">img</a> desc')
    replies = list(client._entry_replies(entry))
    assert len(replies) == 1
    assert replies[0].message \
        == ('<a href="entry-link">Entry Title</a>\n'
            '<b>title</b> '
            '<a href="url">img</a> desc')
    assert replies[0].send_args == {'parse_mode': 'HTML',
                                    'disable_notification': True,
                                    'disable_web_page_preview': True}


def test_entry_replies_img():
    entry = Mock(link='entry-link',
                 title='Entry Title',
                 description=None,
                 summary='',
                 content=('<p>Entry description '
                          '<img src="img-src" alt="image" /></p>'))
    replies = list(client._entry_replies(entry))
    assert len(replies) == 2
    assert replies[0].photos == ['img-src']
    assert replies[0].send_args == {'disable_notification': True}
    assert (replies[1].message
            == '<a href="entry-link">Entry Title</a>\n'
            'Entry description [image]')
    assert replies[1].send_args == {'parse_mode': 'HTML',
                                    'disable_notification': True,
                                    'disable_web_page_preview': True}


def test_entry_replies_img_no_alt():
    entry = Mock(link='entry-link',
                 title='Entry Title',
                 description=None,
                 summary=('<p>Entry description '
                          '<img src="img-src" /></p>'))
    replies = list(client._entry_replies(entry))
    assert len(replies) == 2
    assert replies[0].photos == ['img-src']
    assert replies[0].send_args == {'disable_notification': True}
    assert (replies[1].message
            == '<a href="entry-link">Entry Title</a>\n'
            'Entry description [img]')
    assert replies[1].send_args == {'parse_mode': 'HTML',
                                    'disable_notification': True,
                                    'disable_web_page_preview': True}
