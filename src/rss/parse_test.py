import pytest
from . import parse_msg


@pytest.mark.parametrize('msg,cmd,kwargs', [
    ('rss https://xkcd.com/atom.xml',
     'rss', {'url': 'https://xkcd.com/atom.xml'}),
])
def test_rss(msg, cmd, kwargs):
    assert list(parse_msg(msg)) == [(cmd, kwargs)]
