# Cyclesbot #

Personal telegram bot, interacts with https://cycles.tompaton.com/, https://rainfallrecord.info/ and other services.

### Notable features ###
* uses parser combinators for a powerful command syntax
* allows defining custom "macros", e.g. sending the message "#define 🏃 /tick run" will expand "🏃" to "/tick run" from that point on
* sends scheduled "push" notifications, e.g. "cron add 7:30 weekdays weather" will push the reply from the "weather" message each day at 7:30.
* supports `/undo` previous command
* supports `huh?` to echo previous command received, and "unparsed add" to add unparsed messages to a log for future debugging
