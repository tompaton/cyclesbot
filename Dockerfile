FROM python:3.7

# TODO: need to apt-get install anything? `sox` for podcasts...

RUN pip install --upgrade pip
RUN pip install better_exceptions
ENV BETTER_EXCEPTIONS=1

RUN mkdir -p /cyclesbot /data /blog/generate /blog/content /blog/cache \
             /podcasts/downloaded /podcasts/processed /podcasts/archived \
             /podcasts/deleted /podcasts/progress /podcasts/images
WORKDIR /cyclesbot

# install 3rd party deps before my modules to speed up container builds
COPY requirements_thirdparty.txt .
RUN pip install --no-cache-dir -r requirements_thirdparty.txt

COPY requirements.txt .
RUN pip install --no-cache-dir -r requirements.txt

COPY src ./

ENTRYPOINT ["python", "./bot.py"]
