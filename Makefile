.PHONY: bash clean up down start stop build rebuild test test-deps test-build check-types coverage deploy clean-docker test-and-deploy test-dep test-module


clean:
	sudo find . -name '*.pyc' -user root -delete


clean-docker:
	# remove stopped containers
	docker rm $$(docker ps -a -q) || echo "No stopped containers"
	docker rmi $$(docker images -f 'dangling=true' -q) || echo "No dangling images"


up:
	#docker-compose --verbose up -d
	docker run --rm -it \
	  --env-file ./.env \
	  -v ~/.ssh:/root/.ssh:ro \
	  -v `pwd`/src:/cyclesbot \
	  -v `pwd`/data:/data \
	  -v /home/tom/dev/www/blogs/blog2017/blog_lambda:/blog/generate \
	  -v /home/tom/dev/www/blogs/blog2017/blog_content:/blog/content \
	  -v /home/tom/dev/www/blogs/blog2017/blog_cache:/blog/cache \
	  registry.tompaton.com/tompaton/cyclesbot --disable=git --disable=bank


# down:
# 	docker-compose down


# start:
# 	docker-compose --verbose start


# stop:
# 	docker-compose stop


build: clean
	#docker-compose --verbose build
	docker build -t registry.tompaton.com/tompaton/cyclesbot .


rebuild: clean
	#docker-compose --verbose build
	docker build --no-cache -t registry.tompaton.com/tompaton/cyclesbot .


test:
	#docker-compose cyclesbot run python -m pytest .
	# -v /home/tom/dev/modules/yearlyreps_client/yearlyreps_client:/usr/local/lib/python3.7/site-packages/yearlyreps_client
	docker run --rm -it \
	  -v `pwd`/src:/cyclesbot \
	  --entrypoint "python" \
	  registry.tompaton.com/tompaton/cyclesbot \
	  -m pytest -vv -ra .


test-module:
	docker run --rm -it \
	  -v `pwd`/src:/cyclesbot \
	  -v /home/tom/dev/modules/weather_client/weather_client:/usr/local/lib/python3.7/site-packages/weather_client \
	  --entrypoint "python" \
	  registry.tompaton.com/tompaton/cyclesbot \
	  -m pytest -vv -ra ./chat


test-deps:
	#docker-compose cyclesbot run python -m pytest .
	# -v /home/tom/dev/modules/yearlyreps_client/yearlyreps_client:/usr/local/lib/python3.7/site-packages/yearlyreps_client
	docker run --rm -it \
	  --entrypoint "python" \
	  registry.tompaton.com/tompaton/cyclesbot \
	  -m pytest --pyargs bankcheck cycles_client heater_logs \
	  opentabs_client \
	  podcast_downloader rainfallrecord_client blog_generate \
	  unicode_charts weather_client weightlog_client \
	  yearlyreps_client


test-dep:
	docker run --rm -it \
	  --entrypoint "python" \
	  -v /home/tom/dev/modules/weather_client/weather_client:/usr/local/lib/python3.7/site-packages/weather_client \
	  registry.tompaton.com/tompaton/cyclesbot \
	  -m pytest --pyargs weather_client


test-build:
	#docker-compose cyclesbot run python -m pytest .
	docker run --rm -it \
	  --entrypoint "python" \
	  registry.tompaton.com/tompaton/cyclesbot \
	  -m pytest .


bash:
	#docker-compose cyclesbot run python -m pytest .
	docker run --rm -it \
	  --network host \
	  -v `pwd`/src:/cyclesbot \
	  -v /home/tom/dev/www/blogs/blog2017/blog_content:/blog/content \
	  --entrypoint "bash" \
	  registry.tompaton.com/tompaton/cyclesbot


# check-types:
# 	echo TODO
# 	# if [ -z "$1" ]; then
# 	#   modules=$(find . -maxdepth 1 -type d \
# 	#             -exec test -f {}/__init__.py \; \
# 	#             -printf "%f/ ")
# 	#   modules="bot.py $modules"
# 	# else
# 	#   modules="$@"
# 	# fi

# 	# mypy --config-file mypy.ini $modules


# coverage:
# 	echo TODO
# 	# coverage run --source=. --branch -m pytest $@
# 	# coverage report -m --skip-covered


deploy: build
	docker push registry.tompaton.com/tompaton/cyclesbot
	ssh phosphorus "docker pull registry.tompaton.com/tompaton/cyclesbot"
	scp docker-*.sh phosphorus:/var/data/tom/cyclesbot/


test-and-deploy: build test-deps test-build deploy
	@echo "Done!"
