#!/bin/bash

docker run --rm -it \
       --env-file ./production.env \
       -v ~/.ssh:/root/.ssh:ro \
       -v `pwd`/data:/data \
       -v `pwd`/heater_logs/output:/output \
       -v /var/data/tom/podcasts/data/:/podcasts \
       -v /var/data/tom/tompaton-com-blog-generate:/blog/generate \
       -v /var/data/tom/tompaton-com-blog-content:/blog/content \
       -v /var/data/tom/tompaton-com-blog-cache:/blog/cache \
       registry.tompaton.com/tompaton/cyclesbot $@
